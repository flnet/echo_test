<? include("head.php"); ?>
<style>
    .title-h2{color: ;}
    .title-h2 .fa {color: #abc10b;}
    button.btn-1,button.btn-2{background-color: #535353;color: #fff;border: 1px solid #535353;}/*button文字、背景、線條(CH)*/
    button.btn-1:hover,button.btn-2:hover{background-color: #abc10b;color: #fff;border: 1px solid #abc10b;}/*button文字、背景、線條(CH)*/
    
</style>
<body style="">


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>

    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / 忘記密碼</li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>
    <!-- InstanceBeginEditable name="main" -->
    <main class="cd-main-content page clearfix">
        <div class="row cont">
            <h1 class="col-lg-12 title-h2"><div class="fa fa-check-circle-o"></div>
                已將密碼寄至您註冊時的E-mail</h1>
            <div class="col-lg-4 col-md-4 col-xs-2"></div>
            <div class="col-lg-4 col-md-4 col-xs-8"><button type="" class="btn-1" onclick="location.href='index.php'">回首頁</button></div>
            <div class="col-lg-4 col-md-4 col-xs-2"></div>
        </div>


    </main>
    <!-- InstanceEndEditable -->
</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>



</body>

<!-- InstanceEnd --></html>

<? include("common_js.php"); ?>
