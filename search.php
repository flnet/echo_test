<? include("head.php"); ?>
<? include("por_shared.php"); ?>
<body style="">

<?
$keyWord = $_GET["keyWord"] ;
?>

<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">
    <? include("top_menu.php"); ?>
</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / 您搜尋的文字為"<?=$keyWord?>"</li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>
    <!-- InstanceBeginEditable name="main" -->
    <!--追蹤清單提示框-->
    <div class="ui-ios-overlay-cross">
        <div class="ui-ios-overlay-icon fa fa-check-circle-o"></div>
        <div class="ui-ios-overlay-txt">已加入追蹤</div>
    </div>
    <div class="ui-ios-overlay-checkMark">
        <div class="ui-ios-overlay-icon fa fa-times-circle-o"></div>
        <div class="ui-ios-overlay-txt">已取消追蹤</div>
    </div>
    <!--追蹤清單提示框 結束-->


    <main class="cd-main-content page clearfix">
        <ul id="productsShow" class="row">

            <?
                if( $web_style == SHOPPING_CART )
                {
                    $query_goods3  = "select * from goods3 where HIDE_ID =0 and UP_STATUS = 0 and NAME like '%" . $keyWord . "%' order by NAME ASC limit 40 " ;
                }
                else
                {
                    $query_goods3  = "select * from image_goods3 where HIDE_ID =0 and UP_STATUS = 0 and NAME like '%" . $keyWord . "%' order by NAME ASC limit 40 " ;
                }
                $result_goods3 = mysql_query($query_goods3)or die(mysql_error());
                while( $record_goods3 = mysql_fetch_array($result_goods3) )
                {
                    $goods3_id = $record_goods3["ID"] ;
                    $goods3_name = mb_substr($record_goods3["NAME"],0,30,'utf8')  ; //商品名稱
                    $goods3_image1 = $record_goods3["IMAGE1"] ; //圖片
                    $goods3_self_price = $record_goods3["SELL_PRICE"] ; //售價
                    $goods3_content = mb_substr(strip_tags($record_goods3["CONTENT"]) , 0 , 60 ,'utf8' )  ;
                    $goods3_views_count = $record_goods3["VIEWS_COUNT"] ;
                    $goods3_brief = $record_goods3["BRIEF"] ;

            ?>
                    <li class="col-md-3 col-sm-4 col-xs-6">
                        <div class="wap">
                            <?
                                if( $web_style == SHOPPING_CART ) {
                                ?>
                                    <h1 class="abgne-frame-20140107-1">
                                        <span></span>
                                        <a href="productsDetailed.php?request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>">
                                            <img class="lazy" src="<?=$loading_image?>" data-original="<?=FILE_PATH?>/goods3/<?=$goods3_image1?>" >
                                        </a>
                                    </h1>
                                <?
                                }
                                else
                                {
                                ?>
                                   <h1 class="abgne-frame-20140107-1">
                                        <span></span>
                                        <a href="productsDetailed-images.php?request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>">
                                            <img class="lazy" src="<?=$loading_image?>" data-original="<?=FILE_PATH?>/image_goods3/<?=$goods3_image1?>" >
                                        </a>
                                    </h1> 

                                <?
                                }
                            ?>




                            <h2>
                                <div class="h2B JQellipsis-2"><?=$goods3_name?></div>
                               <div class="h2S JQellipsis"><?=( $goods3_brief != "" ? $goods3_brief : $goods3_content )?></div>
                            </h2>

                            <?
                            if( $web_style == SHOPPING_CART ) {
                            ?>
                                <h3>
                                    <div class="h3A">瀏覽數：<?= $goods3_views_count ?></div>
                                    <div class="h3B"><span class="fa fa-heart checkMark"></span></div>
                                </h3>
                                <h4>NT<span><?= $goods3_self_price ?></span></h4>
                            <?
                            }
                            ?>

                            <h5>

                                <?
                                if( $web_style == SHOPPING_CART ) {
                                ?>
                                    <a href="productsDetailed.php?request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>">
                                        詳細資訊
                                    </a>
                                <?
                                }
                                else
                                {
                                ?>
                                    <a href="productsDetailed-images.php?request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>">
                                        詳細資訊
                                    </a>

                                <?
                                }
                                ?>


                            </h5>
                            <div class="clear"></div>
                        </div>
                    </li>
            <?
            }
            ?>

            <div class="clear"></div>
        </ul>
    </main>


    <div class="clear"></div>
    <!-- InstanceEndEditable -->
</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>


</body>

<!-- InstanceEnd --></html>

<? include("common_js.php"); ?>


<script>
    $( document ).ready(function() {

        //$(".sp-btn").trigger("click");

        $("img.lazy").lazyload({
            effect : "fadeIn"
        });


    });
</script>
