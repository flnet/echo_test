<? include("head.php"); ?>
<? include("king-color.php"); ?>
<style>
.qa-page button.btn01:hover{background-color: <?=$king_color?> <?$king_color_important?>;}/*4.button文字、背景、線條(CH) HOVER*/
</style>
<? include("table.php"); ?>

<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 26"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.背景(C)
$qa_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$qa_qa_page = $qa_style1->color ;

//2.整體文字(C)
$qa_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$qa_qa_page_span = $qa_style2->color ;

//3.input、文字、背景、線條(C)
$qa_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$qa_stylePar_name3_1 = 'color' ;
$qa_stylePar_name3_2 = 'background-color' ;
$qa_stylePar_name3_3 = 'border-top' ;
$qa_stylePar_name3_4 = 'border-right' ;
$qa_stylePar_name3_5 = 'border-bottom' ;
$qa_stylePar_name3_6 = 'border-left' ;
$qa_qa_page_input_input1_color = $qa_style3->$qa_stylePar_name3_1 ;
$qa_qa_page_input_input1_background_color = $qa_style3->$qa_stylePar_name3_2 ;
$qa_qa_page_input_input1_border_top = $qa_style3->$qa_stylePar_name3_3 ;
$qa_qa_page_input_input1_border_right = $qa_style3->$qa_stylePar_name3_4 ;
$qa_qa_page_input_input1_border_bottom = $qa_style3->$qa_stylePar_name3_5 ;
$qa_qa_page_input_input1_border_left = $qa_style3->$qa_stylePar_name3_6 ;

//4.button文字、背景、線條(CH)
$qa_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$qa_stylePar_name4_1 = 'color' ;
$qa_stylePar_name4_2 = 'background-color' ;
$qa_stylePar_name4_3 = 'border-top' ;
$qa_stylePar_name4_4 = 'border-right' ;
$qa_stylePar_name4_5 = 'border-bottom' ;
$qa_stylePar_name4_6 = 'border-left' ;
$qa_stylePar_name4_7 = 'color-hover' ;
$qa_stylePar_name4_8 = 'background-color-hover' ;
$qa_stylePar_name4_8_1 = 'background-color-hover-important' ;
$qa_stylePar_name4_9 = 'border-top-hover' ;
$qa_stylePar_name4_10 = 'border-right-hover' ;
$qa_stylePar_name4_11 = 'border-bottom-hover' ;
$qa_stylePar_name4_12 = 'border-left-hover' ;
$qa_qa_page_button_btn01_color = $qa_style4->$qa_stylePar_name4_1 ;
$qa_qa_page_button_btn01_background_color = $qa_style4->$qa_stylePar_name4_2 ;
$qa_qa_page_button_btn01_border_top = $qa_style4->$qa_stylePar_name4_3 ;
$qa_qa_page_button_btn01_border_right = $qa_style4->$qa_stylePar_name4_4 ;
$qa_qa_page_button_btn01_border_bottom = $qa_style4->$qa_stylePar_name4_5 ;
$qa_qa_page_button_btn01_border_left = $qa_style4->$qa_stylePar_name4_6 ;
$qa_qa_page_button_btn01_color_hover = $qa_style4->$qa_stylePar_name4_7 ;
$qa_qa_page_button_btn01_background_color_hover = $qa_style4->$qa_stylePar_name4_8 ;
$qa_qa_page_button_btn01_background_color_hover_important = $qa_style4->$qa_stylePar_name4_8_1 ;
$qa_qa_page_button_btn01_border_top_hover = $qa_style4->$qa_stylePar_name4_9 ;
$qa_qa_page_button_btn01_border_right_hover = $qa_style4->$qa_stylePar_name4_10 ;
$qa_qa_page_button_btn01_border_bottom_hover = $qa_style4->$qa_stylePar_name4_11 ;
$qa_qa_page_button_btn01_border_left_hover = $qa_style4->$qa_stylePar_name4_12 ;

//5.文字(C)
$qa_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$qa_stylePar_name5_1 = 'color' ;
$qa_stylePar_name5_2 = 'background-color' ;
$qa_stylePar_name5_3 = 'border-top' ;
$qa_stylePar_name5_4 = 'border-right' ;
$qa_stylePar_name5_5 = 'border-bottom' ;
$qa_stylePar_name5_6 = 'border-left' ;
$qa_stylePar_name5_7 = 'color-link' ;
$qa_stylePar_name5_8 = 'color-link-hover' ;
$qa_qa_page_reply_span = $qa_style5->$qa_stylePar_name5_1 ;
$qa_qa_page_reply_background_color = $qa_style5->$qa_stylePar_name5_2 ;
$qa_qa_page_reply_background_border_top = $qa_style5->$qa_stylePar_name5_3 ;
$qa_qa_page_reply_background_border_right = $qa_style5->$qa_stylePar_name5_4 ;
$qa_qa_page_reply_background_border_bottom = $qa_style5->$qa_stylePar_name5_5 ;
$qa_qa_page_reply_background_border_left = $qa_style5->$qa_stylePar_name5_6 ;
$qa_qa_page_reply_span_a = $qa_style5->$qa_stylePar_name5_7 ;
$qa_qa_page_reply_span_a_hover = $qa_style5->$qa_stylePar_name5_8 ;

//6.回覆區(C)
$qa_style6 = json_decode($record_design_style2["STYLE6"]) ; 
$qa_stylePar_name6_1 = 'color' ;
$qa_stylePar_name6_2 = 'background-color' ;
$qa_stylePar_name6_3 = 'border-top' ;
$qa_stylePar_name6_4 = 'border-right' ;
$qa_stylePar_name6_5 = 'border-bottom' ;
$qa_stylePar_name6_6 = 'border-left' ;
$qa_qa_page_reply_qa_reply_color = $qa_style6->$qa_stylePar_name6_1 ;
$qa_qa_page_reply_qa_reply_background_color = $qa_style6->$qa_stylePar_name6_2 ;
$qa_qa_page_reply_qa_reply_border_top = $qa_style6->$qa_stylePar_name6_3 ;
$qa_qa_page_reply_qa_reply_border_right = $qa_style6->$qa_stylePar_name6_4 ;
$qa_qa_page_reply_qa_reply_border_bottom = $qa_style6->$qa_stylePar_name6_5 ;
$qa_qa_page_reply_qa_reply_border_left = $qa_style6->$qa_stylePar_name6_6 ;

//7.圖示(C)
$qa_style7 = json_decode($record_design_style2["STYLE7"]) ; 
$qa_qa_page_reply_qa_reply_i = $qa_style7->color ;

?>

<style>
/*發問表格*/
    .qa-page{background-color: <?=$qa_qa_page?>;}/*1.背景(C)*/
    .qa-page span{color: <?=$qa_qa_page_span?>;}/*2.整體文字(C)*/
    .qa-page input,.input1{color: <?=$qa_qa_page_input_input1_color?>;background-color: <?=$qa_qa_page_input_input1_background_color?>;border-top: <?=$qa_qa_page_input_input1_border_top?>;border-right: <?=$qa_qa_page_input_input1_border_right?>;border-bottom: <?=$qa_qa_page_input_input1_border_bottom?>;border-left: <?=$qa_qa_page_input_input1_border_left?>;}/*3.input、文字、背景、線條(C)*/
    .qa-page button.btn01{color: <?=$qa_qa_page_button_btn01_color?>;background-color: <?=$qa_qa_page_button_btn01_background_color?>;border-top: <?=$qa_qa_page_button_btn01_border_top?>;border-right: <?=$qa_qa_page_button_btn01_border_right?>;border-bottom: <?=$qa_qa_page_button_btn01_border_bottom?>;border-left: <?=$qa_qa_page_button_btn01_border_left?>;}/*4.button文字、背景、線條(CH)*/
    .qa-page button.btn01:hover{color: <?=$qa_qa_page_button_btn01_color_hover?>;background-color: <?=$qa_qa_page_button_btn01_background_color_hover?> <?=$qa_qa_page_button_btn01_background_color_hover_important?>;border-top: <?=$qa_qa_page_button_btn01_border_top_hover?>;border-right: <?=$qa_qa_page_button_btn01_border_right_hover?>;border-bottom: <?=$qa_qa_page_button_btn01_border_bottom_hover?>;border-left: <?=$qa_qa_page_button_btn01_border_left_hover?>;}/*4.button文字、背景、線條(CH) HOVER*/
/*下方回覆區*/
    .qa-page-reply {background-color: <?=$qa_qa_page_reply_background_color?>;border-top: <?=$qa_qa_page_reply_background_border_top?>;border-right: <?=$qa_qa_page_reply_background_border_right?>;border-bottom: <?=$qa_qa_page_reply_background_border_bottom?>;border-left: <?=$qa_qa_page_reply_background_border_left?>;}/*5.背景、線條(C)*/
    .qa-page-reply span{color: <?=$qa_qa_page_reply_span?>;}/*5.文字(C)*/
    .qa-page-reply span a{color: <?=$qa_qa_page_reply_span_a?>;}/*5.連結文字(CH)*/
    .qa-page-reply span a:hover{color: <?=$qa_qa_page_reply_span_a_hover?>;}/*5.連結文字(CH) HOVER*/
    .qa-page-reply .qa-reply {color: <?=$qa_qa_page_reply_qa_reply_color?>;background-color: <?=$qa_qa_page_reply_qa_reply_background_color?>;border-top: <?=$qa_qa_page_reply_qa_reply_border_top?>;border-right: <?=$qa_qa_page_reply_qa_reply_border_right?>;border-bottom: <?=$qa_qa_page_reply_qa_reply_border_bottom?>;border-left: <?=$qa_qa_page_reply_qa_reply_border_left?>;}/*6.回覆區(C)*/
.qa-page-reply .qa-reply i{color: <?=$qa_qa_page_reply_qa_reply_i?>;}/*7.圖示(C)*/
</style>


<?
$query="select * from introduction where ID = 1 ";
$result = mysql_query($query) or die(mysql_error()) ;
while( $record=mysql_fetch_array($result) )
{
    $introduction_title = $record["TITLE"] ; //TITLE
}
?>


<?

if( $_POST['send'] == 1 )
{
    //新增提問 的插入
    $input_data = array() ;
    $input_data['CC_TIME'] = "now()" ;
    $input_data['LAYERS'] = "'<la>0'" ;
    $input_data['HIDE_ID'] = "0" ;
    $input_data['EDIT_TIME'] = "now()" ;
    $input_data['WHO_LOGIN'] = "0" ;
    $input_data['MEMBER_ID'] = "'".$_SESSION["member_id"]."'" ; //會員item
    $input_data['CONTENT'] = "'".$_POST['textarea']."'" ; //內容
    //$input_data['EMAIL'] = "'".$_POST['textfield4']."'"  ; //會員email
    $input_data['NAME'] = "'".$_SESSION["member_name"]."'"  ; //會員姓名

    //商品詢問時發問
    if( $_POST['goods3_ID'] != "" )
    {
        $input_data['GOODS3_ID'] = "'".$_POST['goods3_ID']."'" ; //詢問的商品
        $input_data['GOODS_ORIGIN'] = "'".$_POST['goods_origin']."'" ; //商品來源
    }

    if( $_POST["numerous"] == 1 )
    {
        $input_data['NUMBER'] = "'".$_POST['textfield8']."'" ; //大量詢問數量
    }

    $header = "" ;
    $footer = "" ;
    foreach( $input_data as $k => $v )
    {
        $header .= "," . $k ;
        $footer .= "," . $v ;
    }

    $query = "insert into qa1 ( " . substr($header,1) . ") values (" . substr($footer,1) . ")" ;
    mysql_query( $query ) or die( mysql_error() ) ;

    if ( defined('IS_EMAIL_SEND') ) //判斷是否有定義
    {
        /* 設定使用者資訊 */
        if( IS_EMAIL_SEND == "1" )
        {
            /*==== 寄給網站經營者 ====*/
            $query="select * from introduction where ID = 8 ";
            $result = mysql_query($query) or die(mysql_error()) ;
            $record=mysql_fetch_array($result) ;

            $send_mail = $record["EMAIL"] ; //收件者的eamil
            $subject = "網站會員提問通知！！" ; //電子郵件標題
            $convert = nl2br( $_POST["textarea"] );

            $msg = "<span style='font-size: 20px'>您好！";//信件內容
            $msg .= "您有一個新的提問，日期:".date("Y-m-d")."，詳細內容如下所示：<br/> " ;
            $msg .= "會員ID:".$_SESSION["member_id"]."<br/> " ;
            $msg .= "會員姓名:".$_SESSION["member_name"]."<br/> " ;
            $msg .= "聯絡電話:".$_SESSION['member_phone']."<br/> " ;
            $msg .= "提問內容如下:<br/>".$convert." </span>" ;
            $msg .= "<br/><p style='font-size: 20px;color:red'>您可以至後台 問與答 回覆此問題</p>" ;

            $smtp_EmailUsername = SMTP_EMAIL_USERNAME ;//驗證的使用者資訊
            $smtp_EmailPassword = SMTP_EMAIL_PASSWORD ;//驗證的使用者資訊

            $recipient_name = RECIPIENT_NAME ; //收件者的名稱

            // $from_mail = SMTP_EMAIL_USERNAME ;收件者顯示寄件者的電子郵件
            $from_mail = $record["EMAIL"] ;//收件者顯示寄件者的電子郵件
            $sender_name = SENDER_NAME ; //收件者顯示寄件者的名稱

            if( verificationSMTPinfo($send_mail,$recipient_name,$introduction_title,$from_mail,$subject,$msg,$smtp_EmailUsername,$smtp_EmailPassword) )
            {
                sendEmailMsg($send_mail,$recipient_name,$introduction_title,$from_mail,$subject,$msg,$smtp_EmailUsername,$smtp_EmailPassword);
            }


            /*==== 寄給留言者 ====*/
            $send_mail = $_SESSION['member_account'] ; //收件者的eamil

            $subject = "提問通知！！" ; //電子郵件標題

            $msg = '<a href="https://'.CUSTOMER_URL.'"><img src="https://'.CUSTOMER_URL.'/uploadfile/'.CKFINDER_FILE_PATH_NAME.'/ticker/'.$computer_logo_image.'"></a><br><br><hr>';            
            $msg = "您好！<br><br>";//信件內容
            $msg .= "我們於:".date("Y-m-d")."，有收到你的提問，會儘速與你聯絡，感謝您" ;

            $recipient_name = $_SESSION["member_name"] ; //收件者的名稱

            //$from_mail = SMTP_EMAIL_USERNAME ;收件者顯示寄件者的電子郵件
            $from_mail = $record["EMAIL"] ;//收件者顯示寄件者的電子郵件
            $sender_name = RECIPIENT_NAME ; //收件者顯示寄件者的名稱

            if( verificationSMTPinfo($send_mail,$recipient_name,$introduction_title,$from_mail,$subject,$msg,$smtp_EmailUsername,$smtp_EmailPassword) )
            {
                sendEmailMsg($send_mail,$recipient_name,$introduction_title,$from_mail,$subject,$msg,$smtp_EmailUsername,$smtp_EmailPassword);
            }

        }

    }

    header('LOCATION:qa.php'  ) ;
    exit() ;

}

?>


<body style="">


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->
<div class="alertbox-wa-2">
    <div class="alertbox-ok">
        <div class="alertbox-txt">
            <i class="fa fa-check"></i>
            <p>您的問題已送出，我們將盡快回復您。</p>
        </div>
        <a href="#" class="alertbox-s-2 check">確定</a>

        <!--<div class="alertbox-txt">
            <i class="fa fa-times"></i>
            <p>操作失敗，麻煩重新操作一次</p>
        </div>
        <a href="#" class="alertbox-s-2 times">確定</a>-->
    </div>
</div>
<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>

<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <?=$all_page_name_array["qa"]?></li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>
    <!-- InstanceBeginEditable name="main" -->
    <main class="cd-main-content page clearfix">


    <form id="form1" name="form1" method="post" action="qa.php">

        <div class="qa-page">
            <span>您的大名：</span><br>
            <input name="qa_name" type="text" class="input1" value="<?=$_SESSION['member_name']?>" disabled ><br>

            <?
            if( $_GET["goods3ID"] != "" )
            {
                $query_goods3  = "select * from goods3 where HIDE_ID =0 and ID = " . $_GET["goods3ID"] ;
                $result_goods3 = mysql_query($query_goods3)or die(mysql_error());
                $record_goods3 = mysql_fetch_array($result_goods3) ;

                $goods_origin = $_GET["goods_origin"] ;

            ?>
                <span>詢問商品：
                    <a href="productsDetailed.php?goods1_id=<?=$_GET["goods1"]?>&goods3_ID=<?=$_GET["goods3ID"]?>" target="new">
                        <?=$record_goods3["NAME"]?>
                    </a>
                </span><br>
                <input name="goods3_ID" type="hidden" id="goods3_ID" value="<?=$_GET["goods3ID"]?>" />
                <input name="goods_origin" type="hidden" id="goods_origin" value="<?=$goods_origin?>" />
            <?
            }
            ?>

            <span>您的問題：</span><br>
            <textarea name="textarea" id="textarea" class="input1" style="height:200px;"></textarea><br>
            <button class="td1 btn01" type="submit" name="button2" id="button2" value="送出">送出</button>
        </div>

    <input type="hidden" name="send" value="1" />
    </form>

        <?
            $itme_number = 0 ;
            $query_qa1 = "select * from qa1 where HIDE_ID = 0 AND MEMBER_ID = ".$_SESSION['member_id']." order by ID desc limit 20";
            $result_qa1 = mysql_query( $query_qa1 ) or die( mysql_error() ) ;
            while( $record_qa1 = mysql_fetch_array( $result_qa1 ) )
            {
                $itme_number ++ ;
                //0代表正常發問 -1代表下訂單時發問 其餘代表看手機型號時發問
                if ($record_qa1["GOODS3_ID"] != 0 || $record_qa1["GOODS3_ID"] != -1) {
                    $query_goods3 = "select * from goods3 where HIDE_ID =0 and ID = " . $record_qa1["GOODS3_ID"];
                    $result_goods3 = mysql_query($query_goods3) or die(mysql_error());
                    $record_goods3 = mysql_fetch_array($result_goods3);
                }

                //取得會員姓名
                $query_member = "select * from member where ID = " . $record_qa1["MEMBER_ID"];
                $result_member = mysql_query($query_member) or die(mysql_error());
                $record_member = mysql_fetch_array($result_member);



        ?>

                <div class="qa-page-reply">
                    <span>發 表 人：<?=$_SESSION['member_name']?></span><span class="nub">#<?=$itme_number?></span><br>
                    <span>發表時間：<?=$record_qa1["EDIT_TIME"]?></span><br>
                    
                    <? $goods3_id = $a_record["GOODS3_ID"];?>

                    <?
                    if( $record_qa1["GOODS3_ID"] != 0 && $record_qa1["GOODS3_ID"] != -1  )
                    {
                    ?>
                        <span>詢問商品：<a href="./productsDetailed.php?goods3_ID=<?=$goods3_id?>" target="new"><?=$record_goods3["NAME"]?></a></span><br>
                                                
                    <?
                    }
                    ?>

                    <span>詢問內容：<?=$record_qa1["CONTENT"]?></span><br>
                    
                    <?
                    $response_count = 0;
                    $response_name = "";
                    $response_time = "";
                    $query_qa2 = "SELECT * from qa2 WHERE HIDE_ID=0 and QA1_ID = " . $record_qa1['ID'];
                    $result_qa2 = mysql_query($query_qa2) or die(mysql_error);
                    while ($record_qa2 = mysql_fetch_array($result_qa2)) {
                        $response_count++;
                        $response_content = preg_replace("/\r\n|\r|\n/",'<br/>',$record_qa2["CONTENT"]);
                        $response_name = $admin_control_array[$record_qa2["WHO_LOGIN"]];
                        $response_time = $record_qa2["EDIT_TIME"];

                        echo '<div class="qa-reply"><i class="fa fa-reply"></i>' ;
                        //echo '<span style="color:royalblue">' ;
                        echo $response_content ;
                        echo '</span>' ;
                        echo '</div>' ;

                    }

                    if ($response_count == 0)
                    {
                        $response_content = '暫無回應';

                        echo '<div class="qa-reply"><i class="fa fa-reply"></i>' ;
                        //echo '<span style="color:royalblue">' ;
                        echo $response_content ;
                        echo '</span>' ;
                        echo '</div>' ;
                    }

                    ?>
                    


                </div>

        <?

            }
        ?>



    </main>


<!--    <div class="tab page">-->
<!--        <div class="tabw">-->
<!--            <ul class="tabNumber">-->
<!--                <li class="first btnShare"><a onclick=".c" href="#" class="fa fa-angle-double-left"></a></li>-->
<!--                <li class="pre btnShare"><a href="#" class="fa fa-caret-left"></a></li>-->
<!--                <li class="tb"><a href="#">1</a></li>-->
<!--                <li class="tb"><a href="#">2</a></li>-->
<!--                <li class="tb"><a href="#">3</a></li>-->
<!--                <li class="next btnShare"><a href="#" class="fa fa-caret-right"></a></li>-->
<!--                <li class="last btnShare"><a href="#" class="fa fa-angle-double-right"></a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--        <div class="tabGo">-->
<!--            <div class="mobile">頁次：2/8</div>-->
<!--            <button>Go</button>-->
<!--            <input type="text" name="text" id="" placeholder="">-->
<!--        </div>-->
<!---->
<!--    </div>-->


    <div class="clear"></div>
    <!-- InstanceEndEditable -->
</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>

</body>

<!-- InstanceEnd --></html>

<? include("common_js.php"); ?>


<script>

    $(document).ready(function() {

        $('#button2').click(function()
        {

            if( $("#textarea").val() == "" )
            {
                alert("請輸入內容");
                return ;
            }

            document.form1.submit() ;

        });

    });

</script>
