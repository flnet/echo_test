<? include("king-color.php"); ?>


<style>
    /*6.主MENU中英文選單(CH)*/
    .topNav-pc-ul > li a:hover{color: <?=$king_color?> <?=$king_color_important?>;}/*中文HOVER*/
    .topNav-pc-ul li a span:hover {color: <?=$king_color?> <?=$king_color_important?>;}/*英文 HOVER*/
    .dropdown-pc ul li a:hover {color: <?=$king_color?> <?=$king_color_important?>;}/*文字HOVER*/

    .topNav-mob-open ul > li > a:hover,.topNav-mob-open ul.topNav-mob-open2 li a {background-color: <?=$king_color?> <?=$king_color_important?>;}/*8.次選單展開的 文字(C)+線條(C)*/
</style>



<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 43"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.MENU背景(PC)
$top_menu_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$top_menu_stylePar_name1_1 = 'color' ;
$top_menu_stylePar_name1_2 = 'image' ;
$top_menu_header_background_color = $top_menu_style1->$top_menu_stylePar_name1_1 ;
$top_menu_header_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;

//2.滑鼠往下滑後的背景
$top_menu_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$top_menu_stylePar_name2_1 = 'color' ;
$top_menu_stylePar_name2_2 = 'image' ;
$top_menu_header_scroll_top_background_color = $top_menu_style2->$top_menu_stylePar_name2_1 ;
$top_menu_header_scroll_top_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;

 
//3.尋框(C)
$top_menu_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$top_menu_stylePar_name3_1 = 'color' ;
$top_menu_stylePar_name3_2 = 'background-color' ;
$top_menu_stylePar_name3_3 = 'border-top' ;
$top_menu_stylePar_name3_4 = 'border-right' ;
$top_menu_stylePar_name3_5 = 'border-bottom' ;
$top_menu_stylePar_name3_6 = 'border-left' ;
$top_menu_form_group_color = $top_menu_style3->$top_menu_stylePar_name3_1 ;
$top_menu_form_group_background_color = $top_menu_style3->$top_menu_stylePar_name3_2 ;
$top_menu_form_group_border_top = $top_menu_style3->$top_menu_stylePar_name3_3 ;
$top_menu_form_group_border_right = $top_menu_style3->$top_menu_stylePar_name3_4 ;
$top_menu_form_group_border_bottom = $top_menu_style3->$top_menu_stylePar_name3_5 ;
$top_menu_form_group_border_left = $top_menu_style3->$top_menu_stylePar_name3_6 ;
//4.搜尋按鈕(C)
$top_menu_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$top_menu_stylePar_name4_1 = 'color' ;
$top_menu_stylePar_name4_2 = 'background-color' ;
$top_menu_stylePar_name4_3 = 'border-top' ;
$top_menu_stylePar_name4_4 = 'border-right' ;
$top_menu_stylePar_name4_5 = 'border-bottom' ;
$top_menu_stylePar_name4_6 = 'border-left' ;
$top_menu_form_group_btn_color = $top_menu_style4->$top_menu_stylePar_name4_1 ;
$top_menu_form_group_btn_background_color = $top_menu_style4->$top_menu_stylePar_name4_2 ;
$top_menu_form_group_btn_border_top = $top_menu_style4->$top_menu_stylePar_name4_3 ;
$top_menu_form_group_btn_border_right = $top_menu_style4->$top_menu_stylePar_name4_4 ;
$top_menu_form_group_btn_border_bottom = $top_menu_style4->$top_menu_stylePar_name4_5 ;
$top_menu_form_group_btn_border_left = $top_menu_style4->$top_menu_stylePar_name4_6 ;
//5.登入登出(CH)
$top_menu_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$top_menu_stylePar_name5_1 = 'color' ;
$top_menu_stylePar_name5_2 = 'color-hover' ;
$top_menu_ul_nav01_li_a = $top_menu_style5->$top_menu_stylePar_name5_1 ;
$top_menu_ul_nav01_li_a_hover = $top_menu_style5->$top_menu_stylePar_name5_2 ;
//6.主MENU中英文選單(CH)
$top_menu_style6 = json_decode($record_design_style2["STYLE6"]) ; 
$top_menu_stylePar_name6_1 = 'color' ;
$top_menu_stylePar_name6_2 = 'color-hover' ;
$top_menu_stylePar_name6_2_1 = 'color-hover-important';
$top_menu_stylePar_name6_3 = 'color-en' ;
$top_menu_stylePar_name6_4 = 'color-en-hover' ;
$top_menu_stylePar_name6_4_1 = 'color-en-hover-important' ;
$top_menu_stylePar_name6_5 = 'border-bottom' ;
$top_menu_stylePar_name6_6 = 'border-bottom-hover' ;
$top_menu_topNav_pc_ul_li_a = $top_menu_style6->$top_menu_stylePar_name6_1 ;
$top_menu_topNav_pc_ul_li_a_hover = $top_menu_style6->$top_menu_stylePar_name6_2 ;
$top_menu_topNav_pc_ul_li_a_color_important = $top_menu_style6->$top_menu_stylePar_name6_2_1 ;
$top_menu_topNav_pc_ul_li_a_span = $top_menu_style6->$top_menu_stylePar_name6_3 ;
$top_menu_topNav_pc_ul_li_a_span_hover = $top_menu_style6->$top_menu_stylePar_name6_4 ;
$top_menu_topNav_pc_ul_li_a_span_hover_important = $top_menu_style6->$top_menu_stylePar_name6_4_1 ;
$top_menu_topNav_pc_ul_li = $top_menu_style6->$top_menu_stylePar_name6_5 ;
$top_menu_topNav_pc_ul_li_hover = $top_menu_style6->$top_menu_stylePar_name6_6 ;
//7.MENU下拉次選單(CH)
$top_menu_style7 = json_decode($record_design_style2["STYLE7"]) ; 
$top_menu_stylePar_name7_1 = 'color' ;
$top_menu_stylePar_name7_2 = 'color-hover' ;
$top_menu_stylePar_name7_2_1 = 'color-hover-important' ;
$top_menu_stylePar_name7_3 = 'background-color' ;
$top_menu_dropdown_pc_ul_li_a = $top_menu_style7->$top_menu_stylePar_name7_1 ;
$top_menu_dropdown_pc_ul_li_a_hover = $top_menu_style7->$top_menu_stylePar_name7_2 ;
$top_menu_dropdown_pc_ul_li_a_hover_important = $top_menu_style7->$top_menu_stylePar_name7_2_1 ;
$top_menu_dropdown_pc_ul_li_bg = $top_menu_style7->$top_menu_stylePar_name7_3 ;
?>

<style>
    /*1.MENU背景(PC)*/
    header{background-color: <?=$top_menu_header_background_color?>;background-image: url(<?=$top_menu_header_background_image?>);}/*背景*/
    header.scroll-top{background-color: <?=$top_menu_header_scroll_top_background_color?>;background-image: url(<?=$top_menu_header_scroll_top_background_image?>);}/*2.滑鼠往下滑後的背景，客戶若沒上傳預設為bg_header.png*/
    /*搜尋*/
    /*3.搜尋框(C)*/
    .form-group .form-control{
        color: <?=$top_menu_form_group_color?>;
        background-color: <?=$top_menu_form_group_background_color?>;
        border-top:<?=$top_menu_form_group_border_top?>; 
        border-right:<?=$top_menu_form_group_border_right?>; 
        border-bottom:<?=$top_menu_form_group_border_bottom?>; 
        border-left:<?=$top_menu_form_group_border_left?>;     
    }
    /*4.搜尋button(C)*/
    .form-group .btn {
        color: <?=$top_menu_form_group_btn_color?>;
        background-color: <?=$top_menu_form_group_btn_background_color?>;
        border-top: <?=$top_menu_form_group_btn_border_top?>;
        border-right: <?=$top_menu_form_group_btn_border_right?>;
        border-bottom: <?=$top_menu_form_group_btn_border_bottom?>;
        border-left: <?=$top_menu_form_group_btn_border_left?>;        
    }
    /*5.登入登出(CH)*/
    ul.nav01 li, ul.nav01 li a {color: <?=$top_menu_ul_nav01_li_a?>;}
    ul.nav01 li a:hover{color: <?=$top_menu_ul_nav01_li_a_hover?>;}
    /*6.主MENU中英文選單(CH)*/
    .topNav-pc-ul li a{color: <?=$top_menu_topNav_pc_ul_li_a?>;}/*中文*/
    .topNav-pc-ul > li a:hover{color: <?=$top_menu_topNav_pc_ul_li_a_hover?> <?=$top_menu_topNav_pc_ul_li_a_color_important?>;}/*中文HOVER*/
    .topNav-pc-ul li a span {color: <?=$top_menu_topNav_pc_ul_li_a_span?>;}/*英文*/
    .topNav-pc-ul li a span:hover {color: <?=$top_menu_topNav_pc_ul_li_a_span_hover?> <?=$top_menu_topNav_pc_ul_li_a_span_hover_important?>;}/*英文 HOVER*/
    .topNav-pc-ul > li {border-bottom: <?=$top_menu_topNav_pc_ul_li?>;}/*底線*/
    .topNav-pc-ul > li:hover{border-bottom: <?=$top_menu_topNav_pc_ul_li_hover?>;}/*底線HOVER*/
    /*7.MENU下拉次選單(CH)*/
    .dropdown-pc ul li a {color: <?=$top_menu_dropdown_pc_ul_li_a?>;}/*文字*/
    .dropdown-pc ul li a:hover {color: <?=$top_menu_dropdown_pc_ul_li_a_hover?> <?=$top_menu_dropdown_pc_ul_li_a_hover_important?>;}/*文字HOVER*/
    .dropdown-pc i {color: <?=$top_menu_dropdown_pc_ul_li_bg?>;}/*背景*/
    .dropdown-pc ul {background-color: <?=$top_menu_dropdown_pc_ul_li_bg?>;}/*背景*/
</style>

<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 44"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.MENU背景(PC)
$top_menu_mob_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$top_menu_mob_stylePar_name1_1 = 'color' ;
$top_menu_mob_stylePar_name1_2 = 'image' ;
$top_menu_mob_topNav_mob_background_color = $top_menu_mob_style1->$top_menu_mob_stylePar_name1_1 ;
$top_menu_mob_topNav_mob_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;

//2.滑鼠往下滑後的背景
$top_menu_mob_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$top_menu_mob_stylePar_name2_1 = 'color' ;
$top_menu_mob_stylePar_name2_2 = 'image' ;
$top_menu_mob_header_scroll_top_topNav_mob_background_color = $top_menu_mob_style2->$top_menu_mob_stylePar_name2_1 ;
$top_menu_mob_header_scroll_top_topNav_mob_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;

//3.搜尋、展開、收合ICON(C)
$top_menu_mob_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$top_menu_mob_topNav_mob_i = $top_menu_mob_style3->color ;

//4.搜尋展開-背景
$top_menu_mob_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$top_menu_mob_stylePar_name4_1 = 'background-color' ;
$top_menu_mob_search_dropdown_open_background_color = $top_menu_mob_style4->$top_menu_mob_stylePar_name4_1 ;

//5.搜尋展開-輸入欄位
$top_menu_mob_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$top_menu_mob_stylePar_name5_1 = 'color' ;
$top_menu_mob_stylePar_name5_2 = 'background-color' ;
$top_menu_mob_stylePar_name5_3 = 'border-top' ;
$top_menu_mob_stylePar_name5_4 = 'border-right' ;
$top_menu_mob_stylePar_name5_5 = 'border-bottom' ;
$top_menu_mob_stylePar_name5_6 = 'border-left' ;
$top_menu_mob_input_color = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_1 ;
$top_menu_mob_input_background_color = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_2 ;
$top_menu_mob_input_border_top = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_3 ;
$top_menu_mob_input_border_right = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_4 ;
$top_menu_mob_input_border_bottom = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_5 ;
$top_menu_mob_input_border_left = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_6 ;

//6.搜尋展開-按鈕
$top_menu_mob_style6 = json_decode($record_design_style2["STYLE6"]) ; 
$top_menu_mob_stylePar_name6_1 = 'color' ;
$top_menu_mob_stylePar_name6_2 = 'background-color' ;
$top_menu_mob_stylePar_name6_3 = 'border-left' ;
$top_menu_mob_search_dropdown_open_button_color = $top_menu_mob_style6->$top_menu_mob_stylePar_name6_1 ;
$top_menu_mob_search_dropdown_open_button_background_color = $top_menu_mob_style6->$top_menu_mob_stylePar_name6_2 ;
$top_menu_mob_search_dropdown_open_button_border_left = $top_menu_mob_style6->$top_menu_mob_stylePar_name6_3 ;

//7.MENU展開(CH)
$top_menu_mob_style7 = json_decode($record_design_style2["STYLE7"]) ; 
$top_menu_mob_stylePar_name7_1 = 'color' ;
$top_menu_mob_stylePar_name7_2 = 'background-color' ;
$top_menu_mob_stylePar_name7_3 = 'image' ;
$top_menu_mob_stylePar_name7_4 = 'border-top' ;
$top_menu_mob_stylePar_name7_5 = 'border-right' ;
$top_menu_mob_stylePar_name7_6 = 'border-bottom' ;
$top_menu_mob_stylePar_name7_7 = 'border-left' ;
$top_menu_mob_topNav_mob_open_ul_li_a_color = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_1 ;
$top_menu_mob_topNav_mob_open_ul_li_a_background_color = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_2 ;
$top_menu_mob_topNav_mob_open_ul_li_a_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE7"] ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_top = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_4 ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_right = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_5 ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_bottom = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_6 ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_left = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_7 ;

//8.次選單展開的 文字(C)+線條(C)
$top_menu_mob_style8 = json_decode($record_design_style2["STYLE8"]) ; 
$top_menu_mob_stylePar_name8_1 = 'color' ;
$top_menu_mob_stylePar_name8_2 = 'background-color' ;
$top_menu_mob_stylePar_name8_2_1 = 'background-color-important' ;
$top_menu_mob_stylePar_name8_3 = 'border-top' ;
$top_menu_mob_stylePar_name8_4 = 'border-right' ;
$top_menu_mob_stylePar_name8_5 = 'border-bottom' ;
$top_menu_mob_stylePar_name8_6 = 'border-left' ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_color = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_1 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_background_color = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_2 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_background_color_important = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_2_1 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_top = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_3 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_right = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_4 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_bottom = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_5 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_left = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_6 ;

?>
<style>
/**手機板**/
    /*1.MENU背景(PC)*/
    .topNav-mob{background-color: <?=$top_menu_mob_topNav_mob_background_color?>;background-image: url(<?=$top_menu_mob_topNav_mob_background_image?>) ;background-repeat: repeat;}
    header.scroll-top .topNav-mob{background-color: <?=$top_menu_mob_header_scroll_top_topNav_mob_background_color?>;background-image: url(<?=$top_menu_mob_header_scroll_top_topNav_mob_background_image?>) ;background-repeat: repeat;}/*2.滑鼠往下滑後的背景，客戶若沒上傳預設為bg_header-mob.png*/
    /*3.搜尋、展開、收合ICON(C)*/
    .topNav-mob i {color: <?=$top_menu_mob_topNav_mob_i?>;}
    /*MENU背景(P)*/
    /*搜尋展開*/
    @media only screen and (max-width: 1050px){
    .search-dropdown-open {
        background-color:<?=$top_menu_mob_search_dropdown_open_background_color?>;
        }/*4.背景*/
    input {
        color: <?=$top_menu_mob_input_color?>;
        background-color: <?=$top_menu_mob_input_background_color?>;
        border-top:<?=$top_menu_mob_input_border_top?>;
        border-right: <?=$top_menu_mob_input_border_right?>;
        border-bottom: <?=$top_menu_mob_input_border_bottom?>;
        border-left:<?=$top_menu_mob_input_border_left?>;
        }/*5.輸入欄位*/
    .search-dropdown-open button{
        color: <?=$top_menu_mob_search_dropdown_open_button_color?>;
        background-color: <?=$top_menu_mob_search_dropdown_open_button_background_color?>;
        border-left: <?=$top_menu_mob_search_dropdown_open_button_border_left?>;
        }/*6.按鈕*/
    }
    /*7.MENU展開(CH)*/
    .topNav-mob-open ul > li > a,.topNav-mob-open .mb,.topNav-mob-open .mb a,.topNav-mob i.fa-times{color: <?=$top_menu_mob_topNav_mob_open_ul_li_a_color?>;} /*文字(C)*/
    .topNav-mob-open {background: <?=$top_menu_mob_topNav_mob_open_ul_li_a_background_color?>;background-image: url(<?=$top_menu_mob_topNav_mob_open_ul_li_a_background_image?>);}/*背景(P)*/
    .topNav-mob-open ul > li > a,.topNav-mob-open .mb{border-top:<?=$top_menu_mob_topNav_mob_open_ul_li_a_border_top?>;border-right:<?=$top_menu_mob_topNav_mob_open_ul_li_a_border_right?>;border-bottom: <?=$top_menu_mob_topNav_mob_open_ul_li_a_border_bottom?>;border-left:<?=$top_menu_mob_topNav_mob_open_ul_li_a_border_left?>;} /*線條(C)*/
    .topNav-mob-open ul > li > a:hover,.topNav-mob-open ul.topNav-mob-open2 li a {color: <?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_color?>;background-color: <?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_background_color?> <?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_background_color_important?>;border-top:<?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_top?>;border-right:<?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_right?>;border-bottom: <?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_bottom?>;border-left:<?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_left?>;}/*8.次選單展開的 文字(C)+線條(C)*/

</style>

<?
/*-----------------------
//抓取menu的設定
-------------------------*/

    $page1_array = array() ;
    $together_page_name = array() ;

    $query_page1  = "select * from page1 where HIDE_ID = 0 and STATUS = 0 order by LEVEL ASC LIMIT 10" ;
    $result_page1 = mysql_query($query_page1)or die(mysql_error());
    while( $record_page1 = mysql_fetch_array($result_page1) )
    {
        $page1_id = $record_page1["ID"] ;
        $page1_name = $record_page1["NAME"] ;
        $page1_name_english = $record_page1["NAME_ENGLISH"] ;
        $page1_type = $record_page1["TYPE"] ;
        $page1_url = $record_page1["URL"] ;

        $page1_array[$page1_id][$page1_name] = $page1_type ;
        $page1_name_array[$page1_name] = $page1_name_english ;
        $together_page_name[$page1_id] = $page1_name ;
        
        $page1_url_array[$page1_id] = $page1_url ;
    }

    $all_page_name_array = array() ;



//    foreach( $page1_array as $page1_id => $page1_id_array )
//    {
//        echo "page1_id=".$page1_id ;echo "<br/>";
//        foreach( $page1_id_array as $page1_name => $page1_type )
//
//        echo "page1_name=".$page1_name ;echo "<br/>";
//        echo "page1_type=".$page1_type ;echo "<br/>";echo "<br/>";
//    }

?>




<!--<div class="divBackgroundImage-pc" style="background-image: url(images/bg_header.png);"></div>MENU背景圖-->
<!-- 電腦版 選單 -->
<nav class="topNav-pc">
    <div class="row">
        <div class="topNav-logo-pc col-lg-3 col-md-3 col-sm-3"><a href="index.php?type=click"><img src="<?=FILE_PATH?>/ticker/<?=$computer_logo_image?>" alt=""></a></div>
        <div class="col-lg-9 col-md-9 col-sm-9">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 nav-top">

                    <?
                    if( $_SESSION['member_id'] == "")
                    {
                    ?>
                        <ul class="nav01">
                            <li class="fa fa-user fa-user"></li>
                            <li><a id="login-link" href="signIn.php">登入</a> /
                                <a id="login-link" href="registered.php">註冊</a>
                            </li>
                        </ul>
                    <?
                    }
                    else
                    {
                    ?>
                        <ul class="nav01">
                            <li class="fa fa-user fa-user"></li>
                            <li><a id="login-link" href="profile.php"><?=$_SESSION['member_name']?></a> /
                                <a id="login-link" href="./signIn.php?logout=1">登出</a>
                            </li>
                        </ul>
                    <?
                    }
                    ?>

                    <div class="form-group">
                        <input type="text" id="keywords" class="form-control" placeholder="關鍵字搜尋" name="keyword">
                        <button type="button" value="搜尋" onclick="search_sent_check()" class="btn btn-default fa fa-search"></button>
                     </div>

                </div>
                <ul class="col-lg-12 col-md-12 col-sm-12 topNav-pc-ul">

                    <?
                    foreach( $page1_array as $page1_id => $page1_id_array )
                    {
                        foreach( $page1_id_array as $page1_name => $page1_type )
                        {
                    ?>
                            <?
                            if( $page1_id == 1 )
                            {
                                $all_page_name_array["index"] = $page1_name ;
                            ?>
                                <li> <!--首頁-->
                                    <a href="index.php?page1_ID=<?=$page1_id?>" class="color-topNav-1"><?=$page1_name?><span><?=$page1_name_array[$page1_name]?></span></a>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 2 )
                            {
                                $all_page_name_array["products"] = $page1_name ;
                            ?>
                                <li class="dropdown-btn-pc" style="color:#4d1a1a;"> <!--線上購物or產品展示 dropdown-btn-pc-down-->
                                    <?=($web_style==SHOPPING_CART?$page1_name:$page1_name)?>  
                                        <i class="fa fa-caret-down"></i>
                                        <span><?=($web_style==SHOPPING_CART?$page1_name_array[$page1_name]:$page1_name_array[$page1_name])?></span>
                                    </a>
                                    <div class="dropdown-pc">
                                        <i class="fa fa-sort-up"></i>
                                        <ul class="menuScrollbar" data-mcs-theme="minimal-dark">
                                            <?

                                            //特製清單
                                            $image_goods3_arr = array() ;
                                            $query_goods  = "select image_goods3.* from image_goods3  where HIDE_ID = 0 ORDER BY image_goods3.LEVEL ASC" ;
                                            $result_goods = mysql_query($query_goods)or die(mysql_error());
                                            while( $record_goods = mysql_fetch_array($result_goods) )
                                            {
                                                $tmp_ID = $record_goods['ID'] ;
                                                $tmp_NAME = $record_goods['NAME'] ;
                                                $tmp_LAYERS = $record_goods['LAYERS'] ;
                                                $tmp_LAYERS_arr = explode("<la>",$tmp_LAYERS) ;

                                                $image_goods3_arr[$tmp_ID]['ID'] = $tmp_ID ;
                                                $image_goods3_arr[$tmp_ID]['NAME'] = $tmp_NAME ;
                                                $image_goods3_arr[$tmp_ID]['goods1_id'] = $tmp_LAYERS_arr[2] ;
                                            }
                                            ?>


                                            <?
                                            foreach( $image_goods3_arr as $goods3_id => $image_goods3 )
                                            {
                                                ?>
                                                <li>
                                                    <a href="./productsDetailed-images.php?page1_ID=2&goods1_id=<?=$image_goods3["goods1_id"]?>&goods3_ID=<?=$goods3_id?>"><?=$image_goods3['NAME']?></a>
                                                </li>
                                                <?
                                            }
                                            ?>


                                        </ul>
                                    </div>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 3 )
                            {
                                $all_page_name_array["news"] = $page1_name ;
                            ?>
                                <li class=""><!--最新消息-->
                                    <a href="news.php?page1_ID=<?=$page1_id?>"><?=$page1_name?>
                                                 <span><?=$page1_name_array[$page1_name]?></span>
                                    </a>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 4 )
                            {
                                $all_page_name_array["connection"] = $page1_name ;
                            ?>
                                <li><!--聯絡我們-->
                                    <a href="connection.php?page1_ID=<?=$page1_id?>"><?=$page1_name?>
                                                       <span><?=$page1_name_array[$page1_name]?></span>
                                    </a>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 5 )
                            {
                                $all_page_name_array["photo"] = $page1_name ;
                            ?>
                                <li><!--相簿-->
                                    <a href="photo.php?page1_ID=<?=$page1_id?>"><?=$page1_name?>
                                                  <span><?=$page1_name_array[$page1_name]?></span>
                                    </a>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 6 )
                            {
                                $all_page_name_array["qa"] = $page1_name ;
                            ?>
                                <li><!--產品問與答-->
                                    <?
                                    if( $_SESSION['member_id'] == "") {
                                    ?>
                                        <a href="#" class="alertbox-btn-noLogin"><?=$page1_name?>
                                                                           <span><?=$page1_name_array[$page1_name]?></span>
                                        </a>
                                    <?
                                    }
                                    else
                                    {
                                    ?>
                                        <a href="qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?>
                                                   <span><?=$page1_name_array[$page1_name]?></span></a>
                                    <?
                                    }
                                    ?>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 12 )
                            {
                                $all_page_name_array["member_center"] = $page1_name ;
                            ?>
                                <!--<li>常見Q&A
                                    <a href="common-qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?>
                                                  <span><?=$page1_name_array[$page1_name]?></span>
                                    </a>
                                </li>-->
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 7 )
                            {
                                $all_page_name_array["member_center"] = $page1_name ;
                            ?>
                                <li class="dropdown-btn-pc scroll-1"><!--會員中心-->
                                    <a href="#"><?=$page1_name?><i class="fa fa-caret-down"></i>
                                          <span><?=$page1_name_array[$page1_name]?></span></a>
                                    <div class="dropdown-pc">
                                        <i class="fa fa-sort-up"></i>
                                        <ul>

                                            <?
                                            if( $_SESSION['member_id'] == "") {
                                            ?>
                                                <?
                                                if(  $web_style == SHOPPING_CART  )
                                                {
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >修改會員資料</a></li>' ;
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >訂單查詢</a></li>' ;
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >追蹤清單</a></li>' ;
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >購物車</a></li>' ;
                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                    echo '<li><a href="returns.php?page1_ID='.$page1_id.'">退換貨說明</a></li>' ;
                                                }
                                                else
                                                {
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >修改會員資料</a></li>' ;
                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                }
                                                ?>
                                            <?
                                            }
                                            else
                                            {
                                            ?>

                                                <?
                                                if(  $web_style == SHOPPING_CART  )
                                                {
                                                    echo '<li><a href="profile.php?page1_ID='.$page1_id.'">修改會員資料</a></li>' ;
                                                    echo '<li><a href="orderForm.php?page1_ID='.$page1_id.'">訂單查詢</a></li>' ;
                                                    echo '<li><a href="track.php?page1_ID='.$page1_id.'">追蹤清單</a></li>' ;

                                                    if( $order_commodity_number == 0 )
                                                    {
                                                        echo '<li><a href="#" class="alertbox-btn-noShopping" >購物車</a></li>' ;

                                                    }
                                                    else
                                                    {
                                                        echo '<li><a href="shoppingCart.php?page1_ID='.$page1_id.'&ID='.$order_item.'" >購物車</a></li>' ;

                                                    }

                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                    echo '<li><a href="returns.php?page1_ID='.$page1_id.'">退換貨說明</a></li>' ;
                                                }
                                                else
                                                {
                                                    echo '<li><a href="profile.php?page1_ID='.$page1_id.'" >修改會員資料</a></li>' ;
                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                }
                                                ?>
                                            <?
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 8 )
                            {
                                $all_page_name_array["about"] = $page1_name ;
                                ?>
                                <li><!--關於我們-->
                                    <a href="about.php?page1_ID=<?=$page1_id?>"><?=$page1_name?>
                                        <span><?=$page1_name_array[$page1_name]?></span>
                                    </a>
                                </li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id == 9 )
                            {
                                $all_page_name_array["common_qa"] = $page1_name ;
                                ?>
                                <li><!--常見Ｑ＆Ａ-->
                                    <a href="common-qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?>
                                        <span><?=$page1_name_array[$page1_name]?></span>
                                    </a>
                                </li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id > 9 && $page1_type == 2 )
                            {
                                $all_page_name_array["page_".$page1_id] = $page1_name ;

                                //特製清單
                                $image_goods3_arr = array() ;
                                $query_goods  = "select * from page2 where ON_LEVEL_ID = ".$page1_id." and HIDE_ID = 0 ORDER BY LEVEL ASC" ;
                                $result_goods = mysql_query($query_goods)or die(mysql_error());
                                while( $record_goods = mysql_fetch_array($result_goods) )
                                {
                                    $tmp_ID = $record_goods['ID'] ;
                                    $tmp_NAME = $record_goods['NAME'] ;
                                    //$tmp_LAYERS = $record_goods['LAYERS'] ;
                                    //$tmp_LAYERS_arr = explode("<la>",$tmp_LAYERS) ;

                                    $image_goods3_arr[$tmp_ID]['ID'] = $tmp_ID ;
                                    $image_goods3_arr[$tmp_ID]['NAME'] = $tmp_NAME ;
                                    //$image_goods3_arr[$tmp_ID]['goods1_id'] = $tmp_LAYERS_arr[2] ;
                                }



                            ?>


                                <li class="dropdown-btn-pc scroll-1" style="color:#4d1a1a;"><?=($web_style==SHOPPING_CART?$page1_name:$page1_name)?><i class="fa fa-caret-down"></i></a>
                                    <div class="dropdown-pc">
                                        <ul class="menuScrollbar" data-mcs-theme="minimal-dark">

                                            <?
                                            foreach( $image_goods3_arr as $goods3_id => $image_goods3 )
                                            {
                                                ?>
                                                <li>
                                                    <a href="./pageDetailed.php?page2_ID=<?=$image_goods3['ID']?>&page1_ID=<?=$page1_id?>&page1_type=2"><?=$image_goods3['NAME']?></a>
                                                </li>
                                                <?
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                </li>


                            <?
                            }
                            ?>

                            <?
                            if( $page1_id > 9 && $page1_type == 3 )
                            {
                                $all_page_name_array["pageDetailed_".$page1_id] = $page1_name ;
                            ?>
                                <li><!--自訂頁面 單頁內容-->
                                    <a href="pageDetailed.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?>
                                                         <span><?=$page1_name_array[$page1_name]?></span></a>
                                </li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id > 9 && $page1_type == 4 )
                            {
                                $all_page_name_array["pageDetailed_".$page1_id] = $page1_name ;
                            ?>
                                <li><!--自訂頁面 連結頁面-->
                                    <a href="<?=$page1_url_array[$page1_id]?>"><?=$page1_name?>
                                        <span><?=$page1_name_array[$page1_name]?></span>
                                    </a>
                                </li>
                            <?
                            }
                            ?>

                            
                            


                    <?
                        }
                    }
                    ?>

                </ul>
            </div>
        </div>
    </div>
</nav>



<!-- 手機版 選單 -->
<!--<div class="divBackgroundImage-mob" style="background-image: url(images/bg_header-mob.png);"></div>-->
<nav class="topNav-mob">

    <div class="topNav-logo-mob"><a href="index.php?type=click"><img src="<?=FILE_PATH?>/ticker/<?=$mobile_logo_image?>" alt=""></a></div>
    <i class="fa fa-bars"></i>
    <i class="search-btn fa fa-search"></i>
    <div class="search-dropdown-open" style="display: none;">
        <input type="text" id="mobile_keywords" placeholder="請輸入您要搜尋的關鍵字" name="keyword">
        <button type="button" value="搜尋" onclick="search_sent_mobile_check()" >搜尋</button>
    </div>
    <!--<div class="mobile-search" style="top: 10px;">
        <a href="#" class="dropdown-openSearch fa fa-search active" style="display: block;"></a>
        <a href="#" class="dropdown-openRemove fa fa-remove active" style="display: none;"></a>
        <div class="dropdown-open" style="display: none;">
            <input type="text" placeholder="請輸入您要搜尋的關鍵字" name="keyword">
            <button type="button" value="搜尋" onclick="location.href='search.php'">搜尋</button>
    </div>

    </div>-->

    <div class="topNav-mob-open">
        <i class="fa fa-times"></i>
        <div class="mb">

            <?
            if( $_SESSION['member_id'] == "") {
            ?>
                <!--<img src="index/images/profile.png" alt="" class="member">-->
                <a href="signIn.php">登入</a>/
                <a href="registered.php">註冊</a>
            <?
            }
            else
            {
            ?>
                <!--<img src="index/images/profile.png" alt="" class="member">-->
                <a href="profile.php"><?=$_SESSION['member_name']?></a>/
                <a href="./signIn.php?logout=1">登出</a>
            <?
            }
            ?>
            
        </div>
        <ul class="">


            <?
            foreach( $page1_array as $page1_id => $page1_id_array )
            {
                foreach( $page1_id_array as $page1_name => $page1_type )
                {
                ?>
                    <?
                    if( $page1_id == 1 )
                    {
                    ?>
                        <!--首頁-->
                        <li><a href="index.php?type=click&page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 2 )
                    {


                    ?>
                        <!--線上購物":"產品展示-->
                        <li><a href="#"><?=($web_style==SHOPPING_CART?$page1_name:$page1_name)?><span class="fa fa-angle-down"></span></a>
                            <ul class="topNav-mob-open2">
                                <?

                                //特製清單
                                $image_goods3_arr = array() ;
                                $query_goods  = "select image_goods3.* from image_goods3  where HIDE_ID = 0 ORDER BY image_goods3.LEVEL ASC" ;
                                $result_goods = mysql_query($query_goods)or die(mysql_error());
                                while( $record_goods = mysql_fetch_array($result_goods) )
                                {
                                    $tmp_ID = $record_goods['ID'] ;
                                    $tmp_NAME = $record_goods['NAME'] ;
                                    $tmp_LAYERS = $record_goods['LAYERS'] ;
                                    $tmp_LAYERS_arr = explode("<la>",$tmp_LAYERS) ;

                                    $image_goods3_arr[$tmp_ID]['ID'] = $tmp_ID ;
                                    $image_goods3_arr[$tmp_ID]['NAME'] = $tmp_NAME ;
                                    $image_goods3_arr[$tmp_ID]['goods1_id'] = $tmp_LAYERS_arr[2] ;
                                }
                                ?>


                                <?
                                foreach( $image_goods3_arr as $goods3_id => $image_goods3 )
                                {
                                    ?>
                                    <li>
                                        <a href="./productsDetailed-images.php?page1_ID=2&goods1_id=<?=$image_goods3["goods1_id"]?>&goods3_ID=<?=$goods3_id?>"><?=$image_goods3['NAME']?></a>
                                    </li>
                                    <?
                                }
                                ?>
                            </ul>
                        </li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 3 )
                    {
                    ?>
                        <!--最新消息-->
                        <li><a href="news.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 4 )
                    {
                    ?>
                        <!--聯絡我們-->
                        <li><a href="connection.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 5 )
                    {
                    ?>
                        <!--相簿-->
                        <li><a href="photo.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 6 )
                    {
                    ?>
                        <li><!--產品問與答-->
                            <?
                            if( $_SESSION['member_id'] == "") {
                            ?>
                                <a href="#" class="alertbox-btn-noLogin"><?=$page1_name?></a>
                            <?
                            }
                            else
                            {
                            ?>
                                <a href="qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a>
                            <?
                            }
                            ?>
                        </li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 7 )
                    {
                    ?>
                        <!--會員中心-->
                        <li><a href="#"><?=$page1_name?><span class="fa fa-angle-down"></span></a>
                            <ul class="topNav-mob-open2">

                                <?
                                if( $_SESSION['member_id'] == "") {
                                    ?>
                                    <li><a href="#" class="alertbox-btn-noLogin" >修改會員資料</a></li>
                                    <li><a href="#" class="alertbox-btn-noLogin" >訂單查詢</a></li>
                                    <li><a href="#" class="alertbox-btn-noLogin" >追蹤清單</a></li>
                                    <li><a href="#" class="alertbox-btn-noLogin" >購物車</a></li>
                                    <li><a href="servicerulePrivacy.php?page1_ID=<?=$page1_id?>">服務及隱私權條款</a></li>
                                    <!--<li><a href="common-qa.php?page1_ID=<?=$page1_id?>">常見Q & A</a></li>-->
                                    <li><a href="returns.php?page1_ID=<?=$page1_id?>">退換貨說明</a></li>
                                    <?
                                }
                                else
                                {
                                    ?>
                                    <li><a href="profile.php?page1_ID=<?=$page1_id?>">修改會員資料</a></li>
                                    <li><a href="orderForm.php?page1_ID=<?=$page1_id?>">訂單查詢</a></li>
                                    <li><a href="track.php?page1_ID=<?=$page1_id?>">追蹤清單</a></li>
                                    <li><a href="shoppingCart.php?page1_ID=<?=$page1_id?>&ID=<?=$order_item?>">購物車</a></li>
                                    <li><a href="servicerulePrivacy.php?page1_ID=<?=$page1_id?>">服務及隱私權條款</a></li>
                                    <!--<li><a href="common-qa.php?page1_ID=<?=$page1_id?>">常見Q & A</a></li>-->
                                    <li><a href="returns.php?page1_ID=<?=$page1_id?>">退換貨說明</a></li>
                                    <?
                                }
                                ?>

                            </ul>
                        </li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 8 )
                    {
                        ?>
                        <!--關於我們-->
                        <li><a href="about.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id == 9 )
                    {
                        ?>
                        <!--常見Ｑ＆Ａ-->
                        <li><a href="common-qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id > 9 && $page1_type == 2 )
                    {

                        //特製清單
                        $image_goods3_arr = array() ;
                        $query_goods  = "select * from page2 where ON_LEVEL_ID = ".$page1_id." and HIDE_ID = 0 ORDER BY LEVEL ASC" ;
                        $result_goods = mysql_query($query_goods)or die(mysql_error());
                        while( $record_goods = mysql_fetch_array($result_goods) )
                        {
                            $tmp_ID = $record_goods['ID'] ;
                            $tmp_NAME = $record_goods['NAME'] ;
                            //$tmp_LAYERS = $record_goods['LAYERS'] ;
                            //$tmp_LAYERS_arr = explode("<la>",$tmp_LAYERS) ;

                            $image_goods3_arr[$tmp_ID]['ID'] = $tmp_ID ;
                            $image_goods3_arr[$tmp_ID]['NAME'] = $tmp_NAME ;
                            //$image_goods3_arr[$tmp_ID]['goods1_id'] = $tmp_LAYERS_arr[2] ;
                        }

                    ?>

                        <li><a href="#"><?=$page1_name?><span class="fa fa-angle-down"></span></a>
                            <ul class="topNav-mob-open2">

                                <?
                                foreach( $image_goods3_arr as $goods3_id => $image_goods3 )
                                {
                                    ?>
                                    <li>
                                        <a href="./pageDetailed.php?page2_ID=<?=$image_goods3['ID']?>&page1_ID=<?=$page1_id?>&page1_type=2"><?=$image_goods3['NAME']?></a>
                                    </li>
                                    <?
                                }
                                ?>
                            </ul>
                        </li>


                    <?
                    }
                    ?>

                    <?
                    if( $page1_id > 9 && $page1_type == 3 )
                    {


                    ?>
                        <li><!--自訂頁面-->
                            <a href="pageDetailed.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?></a>
                        </li>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id > 9 && $page1_type == 4 )
                    {
                        $all_page_name_array["pageDetailed_".$page1_id] = $page1_name ;
                    ?>
                        <li><!--自訂頁面 連結頁面-->
                            <a href="<?=$page1_url_array[$page1_id]?>"><?=$page1_name?>
                                
                            </a>
                        </li>
                    <?
                    }
                    ?>

                <?
                }
            }
            ?>


        </ul>
    </div>
    <div class="topNav-mob-Wa">1</div>
</nav>



<script language="javascript">

    //電腦版搜尋用
    function search_sent_check()
    {

        if( $("#keywords").val() == '' )
        {
            alert("您尚未輸入關鍵字");
            return ;
        }

        window.location.href = 'search.php?keyWord=' + $("#keywords").val()  ;
    }

    //手機版搜尋用
    function search_sent_mobile_check()
    {

        if( $("#mobile_keywords").val() == '' )
        {
            alert("您尚未輸入關鍵字");
            return ;
        }

        window.location.href = 'search.php?keyWord=' + $("#mobile_keywords").val()  ;
    }



</script>

