<? include("head.php"); ?>
<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 49"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

$sr_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$sr_stylePar_name1 = 'background-color' ;
$sr_stylePar_name2 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;
$sr_stylePar_name3 = 'border-top' ;
$sr_stylePar_name4 = 'border-right' ;
$sr_stylePar_name5 = 'border-bottom' ;
$sr_stylePar_name6 = 'border-left' ;
$sr_stylePar_name7 = 'border-radius' ;
$sr_background_color = $sr_style1->$sr_stylePar_name1 ;
$sr_background_background_image = $sr_stylePar_name2 ;
$sr_background_border_top = $sr_style1->$sr_stylePar_name3 ;
$sr_background_border_right = $sr_style1->$sr_stylePar_name4 ;
$sr_background_border_bottom = $sr_style1->$sr_stylePar_name5 ;
$sr_background_border_left = $sr_style1->$sr_stylePar_name6 ;
$sr_background_border_radius = $sr_style1->$sr_stylePar_name7 ;

//2.
$sr_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$sr_title_h1 = $sr_style2->color ;
?>

<style>
    /*1.背景(CP)*/
    .servicerulePrivacy-page {
        background-color: <?=$sr_background_color?>;
        background-image: url(<?=$sr_background_background_image?>);background-repeat: repeat;
        border-top: <?=$sr_background_border_top?>;
        border-right: <?=$sr_background_border_right?>;
        border-bottom: <?=$sr_background_border_bottom?>;
        border-left: <?=$sr_background_border_left?>;
        border-radius:<?=$sr_background_border_radius?>;
        -moz-border-radius:<?=$sr_background_border_radius?>;
        -o-border-radius:<?=$sr_background_border_radius?>;
        -webkit- border-radius:<?=$sr_background_border_radius?>; 
    }
    .title-h1{color: <?=$sr_title_h1?>;}/*2.大標題(C)*/
</style>
<?
    $query="select * from introduction where ID in (4,5,6,7) ";
    $result = mysql_query($query) or die(mysql_error()) ;
    while( $record=mysql_fetch_array($result) )
    {
        if( $record["ID"] == 4 )
        {
            $returnExchangeSpecifications = $record["CONTENT"] ; //退/換貨規範
            $returnExchangeSpecificationsName = $record["NAME"] ; 
            $returnExchangeSpecifications_OffOn = $record["SUMMARY"] ; 
        }

        if( $record["ID"] == 5 )
        {
            $commodityReturns = $record["CONTENT"] ; //商品退貨需知
            $commodityReturnsName = $record["NAME"] ; 
            $commodityReturns_OffOn = $record["SUMMARY"] ; 
        }

        if( $record["ID"] == 6 )
        {
            $returnProcedureDescription = $record["CONTENT"] ; //退貨程序說明
            $returnProcedureDescriptionName = $record["NAME"] ; 
            $returnProcedureDescription_OffOn = $record["SUMMARY"] ; 
        }

        if( $record["ID"] == 7 )
        {
            $refundProcedureDescription = $record["CONTENT"] ; //退款程序說明
            $refundProcedureDescriptionName = $record["NAME"] ; 
            $refundProcedureDescription_OffOn = $record["SUMMARY"] ; 
        }
    }
?>

<body style="">

<!-- 產品獨立的css -->
<link rel="stylesheet" href="css/mainKingLeft.css"><!-- 左列菜單 -->
<link rel="stylesheet" href="css/bootstrap.min.css"><!-- 左列菜單 -->


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>


<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <a href="profile.html">會員中心</a> / 退換貨說明</li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>

    <main class="row page">

        <? include("member_menu.php"); ?>

        <main id="proList" class="col-md-9 col-sm-8 col-xs-12">
            <!-- InstanceBeginEditable name="mainKingRight" -->
            <main class="main-cont">

                <h1 class="title-h1" style="display:<?=$returnExchangeSpecifications_OffOn?>"><?=$returnExchangeSpecificationsName?></h1>
                <div class="row cont servicerulePrivacy-page" style="display:<?=$returnExchangeSpecifications_OffOn?>">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <?=$returnExchangeSpecifications?>
                    </div>
                </div>

                <h1 class="title-h1" style="display:<?=$commodityReturnsName_OffOn?>"><?=$commodityReturnsName?></h1>
                <div class="row cont servicerulePrivacy-page" style="display:<?=$commodityReturnsName_OffOn?>">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <?=$commodityReturns?>
                    </div>
                </div>
                <h1 class="title-h1" style="display:<?=$returnProcedureDescription_OffOn?>"><?=$returnProcedureDescriptionName?></h1>
                <div class="row cont servicerulePrivacy-page" style="display:<?=$returnProcedureDescription_OffOn?>">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <?=$returnProcedureDescription?>
                    </div>
                </div>
                <h1 class="title-h1" style="display:<?=$refundProcedureDescription_OffOn?>"><?=$refundProcedureDescriptionName?></h1>
                <div class="row cont servicerulePrivacy-page" style="display:<?=$refundProcedureDescription_OffOn?>">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <?=$refundProcedureDescription?>
                    </div>
                </div>

            </main>

            <!-- InstanceEndEditable -->
</div>
<div class="clear"></div>
</main>


</div><!--wrap結束-->
<!--內容結束-->


<? include("footer.php"); ?>


<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>

</body>

<!-- InstanceEnd --></html>


<? include("common_js.php"); ?>

<!--左列菜單-->
<? include("left_menu_js.php"); ?>

<!-- InstanceBeginEditable name="layout2 script" -->

<!-- InstanceEndEditable -->

<script>
    jQuery(document).ready(function($) {

        /*當左邊或右邊選單隱藏、上面選單出現時，商品內容滿版*/
        if($('.main-top-wa').css("display") == "block"){
            $("#proList").attr("class","col-md-12 col-sm-12 col-xs-12");
        }
    });
</script>