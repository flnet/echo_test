

<script src="js/jquery.min.js" type="text/javascript"></script><!-- jquery啟動 -->
<script src="js/jquery.script.js" type="text/javascript"></script><!-- 浮動icon -->
<script src="js/track.js" type="text/javascript"></script><!-- 追蹤清單 -->
<script src="js/totop.js" type="text/javascript"></script><!--totop-->
<script src="js/menu-new.js" type="text/javascript"></script><!-- menu主 js -->
<script src="js/jquery_lazyload/jquery.lazyload.js" type="text/javascript"></script>
<script src="js/jquery-cookie-master/src/jquery.cookie.js" type="text/javascript"></script>
<script src="js/promotion_specification.js" type="text/javascript"></script><!--商品規格(未輸入值則不顯示)-->
<script src="js/picturefill.js" type="text/javascript"></script><!-- 大圖輪播for IE -->

<!--淡入的動畫效果-->
    <script src="js/fadeIn-animation.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function(){
            $('.smoove').smoove({
               offset: "30%",
            })
        })
    </script>
<!--淡入的動畫效果 結束-->
 <!-- 字數限制 -->
    <script type="text/javascript">
    $(function(){
        var len = 39; // 超過39個字以"..."取代
        $(".JQellipsis").each(function(i){
            if($(this).text().length>len){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len-1)+"...";
                $(this).text(text);
            }
        });

        var len = 16; // 超過16個字以"..."取代
        $(".JQellipsis-2").each(function(i){
            if($(this).text().length>len){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len-1)+"...";
                $(this).text(text);
            }
        });
    });
    </script><!-- 字數限制end -->
<script type="text/javascript">
    //initialize parameters
    var addthis_config = {
        ui_508_compliant: true,
        pubid: "ra-520d1170720de4d0"
    };
    var current_selected = "lastest"; //目前使用者所選擇的項目 1.hot 2.lastest
    var start_index = {
        "hot": 5,
        "lastest": 5
    };
    //var category_id = 24;
    var current_subcategory_id = '';

    //console.log("url = " + req_url);
    //var btnStr = '<button class="load-more">更多文章</button>';
    var login = false;
</script>

<!-- logo輪播 -->
<script src="index/js/owl.carousel.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            autoPlay: 3000,
            items: 4,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3]
        });

    });
</script>

<!--焦點大圖輪播-->
<script src="index/js/owl.carousel_B.js" type="text/javascript"></script>
<script type="text/javascript">
    //Owl Carousel control
    $(document).ready(function() {
        $("#main-slide-photpshow").owlCarousel({
            autoPlay: true,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true
        });
        $("#sub-slide").owlCarousel({
            autoPlay: false,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: false,
            items: 3,
            itemsDesktop: [1024, 3],
            itemsDesktopSmall: [980, 5],
            itemsTablet: [768, 5],
            itemsMobile: [600, 3]
        });
    });
</script>
<!-- InstanceBeginEditable name="script" -->

<!--fancyBox 產品點小圖變大圖輪播-->
<script>
    var isMobile = false;
    var isTablet =false;
    var isAndroidOS =false;
    var isiOS =false;
</script>
<script type="text/javascript" src="fancybox/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="fancybox/js/jquery.fancybox-thumbs.js"></script>
<!-- <script type="text/javascript" src="http://www.genplus.com.tw/product/../Scripts/fancybox/helpers/jquery.fancybox-media.js"></script>-->
<!--photoSwipe-->
<script type="text/javascript">
    $(function() {
        if (isMobile) {
            if ($('.titan-lb').length > 0) {
                $('.titan-lb').photoSwipe();
            }
        } else {
            $('.titan-lb').fancybox({
                padding: 0,//原10
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    thumbs: {
                        width: 50,
                        height: 50
                    },
                    media: {}
                }
            });
        }

    });
</script>
<!--fancyBox 產品點小圖變大圖輪播 end-->

<!--彈出視窗-->
<script type="text/javascript">

    jQuery(document).ready(function($) {

        $('.alertbox-btn-noLogin').click(function(event) {
            $('.noLogin').fadeIn();
            $('.alertbox-wa-noLogin').fadeIn();
            $('.alertbox').hide();
            $('html, body').scrollTop(0);
        });

        $('.alertbox-s-noLogin').click(function(event) {
            $('.alertbox-wa-noLogin').fadeOut().hide;
        });

        $('.alertbox-btn-noShopping').click(function(event) {
            $("#noLogin_text").html("您的購物車沒有商品");
            $('.noLogin').fadeIn();
            $('.alertbox-wa-noLogin').fadeIn();
            $('.alertbox').hide();
            $('html, body').scrollTop(0);
        });

        $('.alertbox-s-noLogin').click(function(event) {
            $('.alertbox-wa-noLogin').fadeOut().hide;
        });

    });

</script>

<!-- InstanceEndEditable -->



<script type="text/javascript">

    // left: 37, up: 38, right: 39, down: 40,
    // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36

    var keys = [37, 38, 39, 40];


    //禁止頁面滾動 代碼
    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function keydown(e) {
        for (var i = keys.length; i--;) {
            if (e.keyCode === keys[i]) {
                preventDefault(e);
                return;
            }
        }
    }

    function wheel(e) {
        preventDefault(e);
    }

    function disable_scroll() {
        if (window.addEventListener) {
            window.addEventListener('DOMMouseScroll', wheel, false);
        }
        window.onmousewheel = document.onmousewheel = wheel;
        document.onkeydown = keydown;
    }

    function enable_scroll() {
        if (window.removeEventListener) {
            window.removeEventListener('DOMMouseScroll', wheel, false);
        }
        window.onmousewheel = document.onmousewheel = document.onkeydown = null;
    }
</script>

<script>
    var titleText_mainLeft = $('.main-left h4').text();
    var titleText_breadcrumb = $('li.breadcrumb').text();
    if($('#menuLeft').hasClass('sub')){
        $('title').append(' - '+titleText_mainLeft);
    }else{
        $('title').append(' - '+titleText_breadcrumb.substr(2));
    }
</script>





<script src="<?=FILE_PATH?>/customize.js" type="text/javascript"></script><!-- 自訂義JS -->

