<?
    /*-----------------------
    //抓取menu的設定
    -------------------------*/

    $page1_array = array() ;
    $together_page_name = array() ;

    $query_page1  = "select * from page1 where HIDE_ID = 0 and STATUS = 0 order by LEVEL ASC LIMIT 10" ;
    $result_page1 = mysql_query($query_page1)or die(mysql_error());
    while( $record_page1 = mysql_fetch_array($result_page1) )
    {
        $page1_id = $record_page1["ID"] ;
        $page1_name = $record_page1["NAME"] ;
        $page1_name_english = $record_page1["NAME_ENGLISH"] ;
        $page1_type = $record_page1["TYPE"] ;
        $page1_url = $record_page1["URL"] ;

        $page1_array[$page1_id][$page1_name] = $page1_type ;
        $page1_name_array[$page1_name] = $page1_name_english ;
        $together_page_name[$page1_id] = $page1_name ;

        $page1_url_array[$page1_id] = $page1_url ;
    }
?>
<? include("king-color.php"); ?>
<style>
#loading{background-color: <?=$king_color?> <?=$king_color_important?>;}
</style>

<?
$query="select * from introduction where ID = 1 ";
$result = mysql_query($query) or die(mysql_error()) ;
while( $record=mysql_fetch_array($result) )
{
    $introduction_summary = $record["SUMMARY"] ; //公司簡介
    $introduction_address = $record["ADDRESS"] ; //公司地址
    $introduction_cell = $record["CELL"] ; //公司電話
    $introduction_email = $record["EMAIL"] ; //公司信箱
    $introduction_fax = $record["FAX"] ; //公司傳真
    $introduction_data_facebookUrl = $record["FACEBOOK_URL"] ;
    $introduction_data_facebookName = $record["FACEBOOK_NAME"] ;
    $introduction_data_facebookOpen = $record["FACEBOOK_OPEN"] ;
    $introduction_translation = $record["GOOD_TRANSLATION"] ; //google翻譯網站原始語言
    $introduction_translation_off = $record["GOOD_OFF"] ; //google翻譯關閉
    $introduction_goodMap = $record["GOOD_MAP"] ; //公司位置
    $introduction_info = $record["INFO1"] ; //版權宣告
    $introduction_meta = $record["META"] ; //META
    $introduction_title = $record["TITLE"] ; //TITLE
}
?>

<?
    /*==== 取得網頁小工具-聯絡資訊小圖示樣式 Start====*/
    $query_web_tool2 = "select * from web_tool2 where HIDE_ID = 0 and ID = 1"  ;
    $result_web_tool2 = mysql_query( $query_web_tool2 ) or die( mysql_error() ) ;
    $record_web_tool2 = mysql_fetch_array( $result_web_tool2 ) ;

    //1.整體
    $contaction_style1 = json_decode($record_web_tool2["STYLE1"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'align' ;
    $contaction_display = $contaction_style1->$contaction_stylePar_name1 ;
    $contaction_align = $contaction_style1->$contaction_stylePar_name2 ;

    //2.第1個圖示 phone電話 固定
    $contaction_style2 = json_decode($record_web_tool2["STYLE2"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE2"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_stylePar_name5 = 'color' ;
    $contaction_stylePar_name6 = 'bg-color' ;
    $contaction_li_cont_icon_1_display = $contaction_style2->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_1_link = $contaction_style2->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_1_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_1_order = $contaction_style2->$contaction_stylePar_name4 ;
    $contaction_li_cont_icon_1_color = $contaction_style2->$contaction_stylePar_name5 ;
    $contaction_li_cont_icon_1_bg_color = $contaction_style2->$contaction_stylePar_name6 ;

    //3.第2個圖示
    $contaction_style3 = json_decode($record_web_tool2["STYLE3"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE3"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_2_display = $contaction_style3->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_2_link = $contaction_style3->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_2_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_2_order = $contaction_style3->$contaction_stylePar_name4 ;

    //4.第3個圖示
    $contaction_style4 = json_decode($record_web_tool2["STYLE4"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE4"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_3_display = $contaction_style4->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_3_link = $contaction_style4->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_3_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_3_order = $contaction_style4->$contaction_stylePar_name4 ;

    //5.第4個圖示
    $contaction_style5 = json_decode($record_web_tool2["STYLE5"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE5"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_4_display = $contaction_style5->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_4_link = $contaction_style5->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_4_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_4_order = $contaction_style5->$contaction_stylePar_name4 ;

    //6.第5個圖示
    $contaction_style6 = json_decode($record_web_tool2["STYLE6"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE6"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_5_display = $contaction_style6->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_5_link = $contaction_style6->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_5_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_5_order = $contaction_style6->$contaction_stylePar_name4 ;

    //7.第6個圖示
    $contaction_style7 = json_decode($record_web_tool2["STYLE7"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE7"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_6_display = $contaction_style7->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_6_link = $contaction_style7->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_6_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_6_order = $contaction_style7->$contaction_stylePar_name4 ;

?>
<style>
    /* 1.整體 */
    ul.contaction{display:<?=$contaction_display?>}
    /* 2.第1個圖示 phone電話 固定 */
    ul.contaction li.cont-icon-1{display:<?=$contaction_li_cont_icon_1_display?>;order:<?=$contaction_li_cont_icon_1_order?>}/*開關、排序*/
    ul.contaction li a#phone p{color:<?=$contaction_li_cont_icon_1_color?>;background-color:<?=$contaction_li_cont_icon_1_bg_color?>}/*滑入的電話顏色*/
    /* 3.第2個圖示*/
    ul.contaction li.cont-icon-2{display:<?=$contaction_li_cont_icon_2_display?>;order:<?=$contaction_li_cont_icon_2_order?>}/*開關、排序*/ 
    /* 4.第2個圖示*/
    ul.contaction li.cont-icon-3{display:<?=$contaction_li_cont_icon_3_display?>;order:<?=$contaction_li_cont_icon_3_order?>}/*開關、排序*/ 
    /* 5.第2個圖示*/
    ul.contaction li.cont-icon-4{display:<?=$contaction_li_cont_icon_4_display?>;order:<?=$contaction_li_cont_icon_4_order?>}/*開關、排序*/ 
    /* 6.第2個圖示*/
    ul.contaction li.cont-icon-5{display:<?=$contaction_li_cont_icon_5_display?>;order:<?=$contaction_li_cont_icon_5_order?>}/*開關、排序*/ 
    /* 7.第2個圖示*/
    ul.contaction li.cont-icon-6{display:<?=$contaction_li_cont_icon_6_display?>;order:<?=$contaction_li_cont_icon_6_order?>}/*開關、排序*/ 
</style>

<?
    /*==== 取得網頁小工具-是否禁用右鍵 Start====*/
    $query_web_tool2 = "select * from web_tool2 where HIDE_ID = 0 and ID = 2"  ;
    $result_web_tool2 = mysql_query( $query_web_tool2 ) or die( mysql_error() ) ;
    $record_web_tool2 = mysql_fetch_array( $result_web_tool2 ) ;

    //1.鎖右鍵
    $prohibited_style1 = json_decode($record_web_tool2["STYLE1"]) ; 
    $cprohibited_stylePar_name1 = 'prohibited' ;
    $prohibited = $prohibited_style1->$cprohibited_stylePar_name1 ;
?>

<?
    /*==== 取得網頁小工具-瀏覽人數計數器 Start====*/
    $query_web_tool2 = "select * from web_tool2 where HIDE_ID = 0 and ID = 3"  ;
    $result_web_tool2 = mysql_query( $query_web_tool2 ) or die( mysql_error() ) ;
    $record_web_tool2 = mysql_fetch_array( $result_web_tool2 ) ;

    //1.瀏覽人數計數器
    $visits_people_style1 = json_decode($record_web_tool2["STYLE1"]) ; 
    $visits_people_stylePar_name1 = 'display' ;
    $visits_people_stylePar_name2 = 'visits_people' ;
    $visits_people_stylePar_name3 = 'visits_name' ;
    $visits_people_display = $visits_people_style1->$visits_people_stylePar_name1 ;
    $visits_people = $visits_people_style1->$visits_people_stylePar_name2 ;
    $visits_name = $visits_people_style1->$visits_people_stylePar_name3 ;
?>

<!DOCTYPE html>
<html lang="en">

<?
        /*==== 取得商品樣式 Start====*/
        $query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 47"  ;
        $result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
        $record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

        //2.頁籤小圖
        $head_style1 = json_decode($record_design_style2["STYLE2"]) ; 
        $head_stylePar_name1_2 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;
        $head_titleLogo = $head_stylePar_name1_2 ;
        //3.網頁載入
        $head_style3 = json_decode($record_design_style2["STYLE3"]) ; 
        $head_stylePar_name3_1 = 'color' ;
        $head_stylePar_name3_1_1 = 'color-important' ;
        $head_stylePar_name3_2 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE3"] ;
        $loading_color = $head_style3->$head_stylePar_name3_1 ;
        $loading_color_important = $head_style3->$head_stylePar_name3_1_1 ;
        $loading_image = $head_stylePar_name3_2 ;
?>
<head>
    <meta charset="utf-8">
    <!-- 網頁所使用的語言種類 -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Language" content="zh-TW">
    <?=$introduction_meta?><!-- meta設定 -->
    <link rel="Shortcut icon" href="<?=$head_titleLogo?>" /><!-- title logo -->
    <!--<link href="index4/css/bootstrap.min-pic.css" rel="stylesheet"> 產品輪播
	<link rel="stylesheet" type="text/css" href="index4/css/flexible-bootstrap-carousel.css" />產品輪播 -->
    <link rel="stylesheet" type="text/css" href="index2/css/bootstrap_grid.css" /> <!-- 網格grid -->
    <link href="owl-carousel-product/css/owl.carousel-product.css" rel="stylesheet">
    <link href="owl-carousel-product/css/owl.theme-product.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="index4/css/menu-new.css" /><!-- menu -->
    <link rel="stylesheet" type="text/css" href="index/css/float.css"><!-- 浮動icon -->
    <link rel="stylesheet" type="text/css" href="index2/css/owl.carousel_B.css"><!--焦點大圖輪播-->
    <link rel="stylesheet" type="text/css" href="index/css/owl.carousel.css"><!-- 最下方logo輪播 -->
    <link rel="stylesheet" type="text/css" href="index2/css/zoom.css"><!-- 圖片滑入放大 -->
    <link rel="stylesheet" type="text/css" href="font-awesome-4.6.3/css/font-awesome.min.css"><!-- 網頁icon字型 -->
    <link rel="stylesheet" type="text/css" href="index2/css/track.css"> <!--追蹤清單 -->
    <link rel="stylesheet" type="text/css" href="index4/css/style.css"><!-- 主CSS -->
    <!-- <link rel="stylesheet" href="http://www.justinaguilar.com/animations/css/style.css">-->
    <link rel="stylesheet" href="index2/css/loading.css"><!-- loading -->
    <link rel="stylesheet" type="text/css" href="css/alert.css"><!-- 彈出視窗 -->
    <link href="<?=FILE_PATH?>/customize.css" rel="stylesheet" type="text/css" /><!--自訂義CSS-->

    
     <!--menu次選單的捲軸 開始-->
    <script src="js/jquery2.0.0.min.js"></script>
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
    <script src="js/jquery.mCustomScrollbar.js"></script>
    <script>
        (function ($) {
            $(window).load(function () {
                $(".menuScrollbar").mCustomScrollbar();
            });
        })(jQuery);
    </script>
    <style>.menuScrollbar{max-height: 300px;}</style>
    <!--menu次選單的捲軸 結束-->

    <?//形象頁面專用
    if( $web_style == COMPANY_IMAGE )
    {
        echo '<link rel="stylesheet" href="css/images.css">' ;
    }
    ?>

    <!--<title><?=WEB_TITLE?></title>-->
    <title><?=$introduction_title?></title>

<?
$query="select * from introduction where ID = 1 ";
$result = mysql_query($query) or die(mysql_error()) ;
while( $record=mysql_fetch_array($result) )
{
    $introduction_ga = $record["GOOD_GA"] ; //googleGA
}
?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?=$introduction_ga?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '<?=$introduction_ga?>');
</script>



<style>
    /*3.網頁載入*/
        #loading{background-color: <?=$loading_color?> <?=$loading_color_important?>;}
</style>


<?
/*==== 取得愛心樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 3"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

$products_style1 = json_decode($record_design_style2["STYLE1"]) ;
$products_style2 = json_decode($record_design_style2["STYLE2"]) ;

$products_checkMark_color = $products_style1->color ;
$products_cross_color = $products_style2->color ;

/*==== 取得愛心樣式 End====*/
?>


<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 44"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.MENU背景(PC)
$top_menu_mob_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$top_menu_mob_stylePar_name1_1 = 'color' ;
$top_menu_mob_stylePar_name1_2 = 'image' ;
$top_menu_mob_topNav_mob_background_color = $top_menu_mob_style1->$top_menu_mob_stylePar_name1_1 ;
$top_menu_mob_topNav_mob_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;

//2.滑鼠往下滑後的背景
$top_menu_mob_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$top_menu_mob_stylePar_name2_1 = 'color' ;
$top_menu_mob_stylePar_name2_2 = 'image' ;
$top_menu_mob_header_scroll_top_topNav_mob_background_color = $top_menu_mob_style2->$top_menu_mob_stylePar_name2_1 ;
$top_menu_mob_header_scroll_top_topNav_mob_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;

//3.搜尋、展開、收合ICON(C)
$top_menu_mob_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$top_menu_mob_topNav_mob_i = $top_menu_mob_style3->color ;

//4.搜尋展開-背景
$top_menu_mob_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$top_menu_mob_stylePar_name4_1 = 'background-color' ;
$top_menu_mob_search_dropdown_open_background_color = $top_menu_mob_style4->$top_menu_mob_stylePar_name4_1 ;

//5.搜尋展開-輸入欄位
$top_menu_mob_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$top_menu_mob_stylePar_name5_1 = 'color' ;
$top_menu_mob_stylePar_name5_2 = 'background-color' ;
$top_menu_mob_stylePar_name5_3 = 'border-top' ;
$top_menu_mob_stylePar_name5_4 = 'border-right' ;
$top_menu_mob_stylePar_name5_5 = 'border-bottom' ;
$top_menu_mob_stylePar_name5_6 = 'border-left' ;
$top_menu_mob_input_color = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_1 ;
$top_menu_mob_input_background_color = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_2 ;
$top_menu_mob_input_border_top = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_3 ;
$top_menu_mob_input_border_right = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_4 ;
$top_menu_mob_input_border_bottom = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_5 ;
$top_menu_mob_input_border_left = $top_menu_mob_style5->$top_menu_mob_stylePar_name5_6 ;

//6.搜尋展開-按鈕
$top_menu_mob_style6 = json_decode($record_design_style2["STYLE6"]) ; 
$top_menu_mob_stylePar_name6_1 = 'color' ;
$top_menu_mob_stylePar_name6_2 = 'background-color' ;
$top_menu_mob_stylePar_name6_3 = 'border-left' ;
$top_menu_mob_search_dropdown_open_button_color = $top_menu_mob_style6->$top_menu_mob_stylePar_name6_1 ;
$top_menu_mob_search_dropdown_open_button_background_color = $top_menu_mob_style6->$top_menu_mob_stylePar_name6_2 ;
$top_menu_mob_search_dropdown_open_button_border_left = $top_menu_mob_style6->$top_menu_mob_stylePar_name6_3 ;

//7.MENU展開(CH)
$top_menu_mob_style7 = json_decode($record_design_style2["STYLE7"]) ; 
$top_menu_mob_stylePar_name7_1 = 'color' ;
$top_menu_mob_stylePar_name7_2 = 'background-color' ;
$top_menu_mob_stylePar_name7_3 = 'image' ;
$top_menu_mob_stylePar_name7_4 = 'border-top' ;
$top_menu_mob_stylePar_name7_5 = 'border-right' ;
$top_menu_mob_stylePar_name7_6 = 'border-bottom' ;
$top_menu_mob_stylePar_name7_7 = 'border-left' ;
$top_menu_mob_topNav_mob_open_ul_li_a_color = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_1 ;
$top_menu_mob_topNav_mob_open_ul_li_a_background_color = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_2 ;
$top_menu_mob_topNav_mob_open_ul_li_a_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE7"] ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_top = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_4 ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_right = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_5 ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_bottom = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_6 ;
$top_menu_mob_topNav_mob_open_ul_li_a_border_left = $top_menu_mob_style7->$top_menu_mob_stylePar_name7_7 ;

//8.次選單展開的 文字(C)+線條(C)
$top_menu_mob_style8 = json_decode($record_design_style2["STYLE8"]) ; 
$top_menu_mob_stylePar_name8_1 = 'color' ;
$top_menu_mob_stylePar_name8_2 = 'background-color' ;
$top_menu_mob_stylePar_name8_3 = 'border-top' ;
$top_menu_mob_stylePar_name8_4 = 'border-right' ;
$top_menu_mob_stylePar_name8_5 = 'border-bottom' ;
$top_menu_mob_stylePar_name8_6 = 'border-left' ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_color = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_1 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_background_color = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_2 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_top = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_3 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_right = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_4 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_bottom = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_5 ;
$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_left = $top_menu_mob_style8->$top_menu_mob_stylePar_name8_6 ;

?>
<style>
/**手機板**/
    /*1.MENU背景(PC)*/
    .topNav-mob{background-color: <?=$top_menu_mob_topNav_mob_background_color?>;background-image: url(<?=$top_menu_mob_topNav_mob_background_image?>) ;background-repeat: repeat;}
    header.scroll-top .topNav-mob{background-color: <?=$top_menu_mob_header_scroll_top_topNav_mob_background_color?>;background-image: url(<?=$top_menu_mob_header_scroll_top_topNav_mob_background_image?>) ;background-repeat: repeat;}/*2.滑鼠往下滑後的背景，客戶若沒上傳預設為bg_header-mob.png*/
    /*3.搜尋、展開、收合ICON(C)*/
    .topNav-mob i {color: <?=$top_menu_mob_topNav_mob_i?>;}
    /*MENU背景(P)*/
    /*搜尋展開*/
    @media only screen and (max-width: 1050px){
    .search-dropdown-open {
        background-color:<?=$top_menu_mob_search_dropdown_open_background_color?>;
        }/*4.背景*/
    input {
        color: <?=$top_menu_mob_input_color?>;
        background-color: <?=$top_menu_mob_input_background_color?>;
        border-top:<?=$top_menu_mob_input_border_top?>;
        border-right: <?=$top_menu_mob_input_border_right?>;
        border-bottom: <?=$top_menu_mob_input_border_bottom?>;
        border-left:<?=$top_menu_mob_input_border_left?>;
        }/*5.輸入欄位*/
    .search-dropdown-open button{
        color: <?=$top_menu_mob_search_dropdown_open_button_color?>;
        background-color: <?=$top_menu_mob_search_dropdown_open_button_background_color?>;
        border-left: <?=$top_menu_mob_search_dropdown_open_button_border_left?>;
        }/*6.按鈕*/
    }
    /*7.MENU展開(CH)*/
    .topNav-mob-open ul > li > a,.topNav-mob-open .mb,.topNav-mob-open .mb a,.topNav-mob i.fa-times{color: <?=$top_menu_mob_topNav_mob_open_ul_li_a_color?>;} /*文字(C)*/
    .topNav-mob-open {background: <?=$top_menu_mob_topNav_mob_open_ul_li_a_background_color?>;background-image: url(<?=$top_menu_mob_topNav_mob_open_ul_li_a_background_image?>);}/*背景(P)*/
    .topNav-mob-open ul > li > a,.topNav-mob-open .mb{border-top:<?=$top_menu_mob_topNav_mob_open_ul_li_a_border_top?>;border-right:<?=$top_menu_mob_topNav_mob_open_ul_li_a_border_right?>;border-bottom: <?=$top_menu_mob_topNav_mob_open_ul_li_a_border_bottom?>;border-left:<?=$top_menu_mob_topNav_mob_open_ul_li_a_border_left?>;} /*線條(C)*/
    .topNav-mob-open ul > li > a:hover,.topNav-mob-open ul.topNav-mob-open2 li a {color: <?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_color?>;background-color: <?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_background_color?>;border-top:<?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_top?>;border-right:<?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_right?>;border-bottom: <?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_bottom?>;border-left:<?=$top_menu_mob_topNav_mob_open_ul_li_a_hover_border_left?>;}/*8.次選單展開的 文字(C)+線條(C)*/

</style>

<?
/*==== 取得排列順序 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 66"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.MENU背景(PC)
$index4_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_name1_1 = 'number1' ;
$index4_name1_2 = 'number2' ;
$index4_name1_3 = 'number3' ;
$index4_name1_4 = 'number4' ;
$index4_name1_5 = 'number5' ;
$index4_main_picAndLink = $index4_style1->$index4_name1_1 ;
$index4_main_p1 = $index4_style1->$index4_name1_2 ;
$index4_main_p2 = $index4_style1->$index4_name1_3 ;
$index4_main_p3 = $index4_style1->$index4_name1_4 ;
$index4_main_p4 = $index4_style1->$index4_name1_5 ;
/*==== 取得排列順序 End====*/
?>
<style>
/*66-排列順序*/
    main.mainkINwA {display:flex;flex-direction: column;}
    main#picAndLink {order: <?=$index4_main_picAndLink?>;}
    main#p1 {order: <?=$index4_main_p1?>;}
    main#p2 {order: <?=$index4_main_p2?>;}
    main#p3 {order: <?=$index4_main_p3?>;}
    main#demo {order: <?=$index4_main_p4?>;}
</style>

<?
/*==== 取得logo背景、選單 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 67"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.MENU背景(PC)
$index4_header_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_header_name1_1 = 'color' ;
$index4_header_topNav_logo_pc = $index4_header_style1->$index4_header_name1_1 ;

//2.選單
$index4_header_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$index4_header_name2_1 = 'color' ;
$index4_header_name2_2 = 'bgcolor' ;
$index4_header_name2_3 = 'border-top' ;
$index4_header_name2_4 = 'border-right' ;
$index4_header_name2_5 = 'border-bottom' ;
$index4_header_name2_6 = 'border-left' ;
$index4_header_name2_7 = 'color-hover' ;
$index4_header_name2_8 = 'bgcolor-hover' ;
$index4_header_topNav_pc_ul_li_a = $index4_header_style2->$index4_header_name2_1 ;
$index4_header_topNav_pc_ul = $index4_header_style2->$index4_header_name2_2 ;
$index4_header_topNav_pc_ul_bt = $index4_header_style2->$index4_header_name2_3 ;
$index4_header_topNav_pc_ul_br = $index4_header_style2->$index4_header_name2_4 ;
$index4_header_topNav_pc_ul_bb = $index4_header_style2->$index4_header_name2_5 ;
$index4_header_topNav_pc_ul_bl = $index4_header_style2->$index4_header_name2_6 ;
$index4_header_topNav_pc_ul_li_a_hover = $index4_header_style2->$index4_header_name2_7 ;
$index4_header_topNav_pc_ul_li_a_hover_bg = $index4_header_style2->$index4_header_name2_8 ;

//3.下單次選單
$index4_header_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$index4_header_name3_1 = 'color' ;
$index4_header_name3_2 = 'bgcolor' ;
$index4_header_name3_3 = 'color-hover' ;
$index4_header_name3_4 = 'bgcolor-hover' ;
$index4_dropdown_pc_ul_li_a_color = $index4_header_style3->$index4_header_name3_1 ;
$index4_dropdown_pc_ul_li_a_bgcolor = $index4_header_style3->$index4_header_name3_2 ;
$index4_dropdown_pc_ul_li_a_color2 = $index4_header_style3->$index4_header_name3_3 ;
$index4_dropdown_pc_ul_li_a_bgcolor2 = $index4_header_style3->$index4_header_name3_4 ;


//4.登入/註冊
$index4_header_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$index4_header_name4_1 = 'color' ;
$index4_header_name4_2 = 'color-2' ;
$index4_header_name4_3 = 'color-2-hover' ;
$index4_header_li_user = $index4_header_style4->$index4_header_name4_1 ;
$index4_header_li_user_a = $index4_header_style4->$index4_header_name4_2 ;
$index4_header_li_user_a_hover = $index4_header_style4->$index4_header_name4_3 ;
/*==== 取得logo背景、選單 End====*/
?>
<style>
/*67-logo背景、選單*/
    /*1.logo背景*/
    header .topNav-logo-pc {background-color: <?=$index4_header_topNav_logo_pc?>;}
    /*2.選單*/
    .topNav-pc-ul li a, .topNav-pc-ul li {color: <?=$index4_header_topNav_pc_ul_li_a?>;}/*2.選單文字顏色*/
    .topNav-pc-ul {background-color: <?=$index4_header_topNav_pc_ul?>;border-top: <?=$index4_header_topNav_pc_ul_bt?>;border-right: <?=$index4_header_topNav_pc_ul_br?>; border-bottom: <?=$index4_header_topNav_pc_ul_bb?>;border-left: <?=$index4_header_topNav_pc_ul_bl?>;}/*2.整體背景文字、邊框*/
    .topNav-pc-ul > li a:hover {color: <?=$index4_header_topNav_pc_ul_li_a_hover?>;}/*2.選單文字顏色 HOVER*/
    /*3.下單次選單*/
    .dropdown-pc ul li a{color:<?=$index4_dropdown_pc_ul_li_a_color?>;}/*下單次選單文字顏色*/
    .dropdown-pc ul {background-color: <?=$index4_dropdown_pc_ul_li_a_bgcolor?>;}/*下單次選單背景顏色*/
    .dropdown-pc ul li a:hover {color: <?=$index4_dropdown_pc_ul_li_a_color2?>;background: <?=$index4_dropdown_pc_ul_li_a_bgcolor2?>;}/*下單次選單文字、背景顏色 HOVVER*/
    .topNav-pc-ul > li > a:hover {background-color: <?=$index4_header_topNav_pc_ul_li_a_hover_bg?>;}/*2.選單背景顏色 HOVER*/
    /*4.登入/註冊*/
    li.user {color: <?=$index4_header_li_user?>;}/*圖示顏色*/
    li.user a {color: <?=$index4_header_li_user_a?>;}/*文字顏色*/
    li.user a:hover {color: <?=$index4_header_li_user_a_hover?>;}/*文字顏色 HOVER*/
</style>

<?
/*==== 取得四張大圖 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 68"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.開啟關閉區塊、背景顏色
$index4_main_picAndLink_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_main_picAndLink_name1_1 = 'display' ;
$index4_main_picAndLink_name1_2 = 'bgcolor' ;
$index4_main_picAndLink_display = $index4_main_picAndLink_style1->$index4_main_picAndLink_name1_1 ;
$index4_main_picAndLink_bgcolor = $index4_main_picAndLink_style1->$index4_main_picAndLink_name1_2 ;

//2.第一張圖
$index4_main_picAndLink_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$index4_main_picAndLink_name2_1 = 'texe-b' ;
$index4_main_picAndLink_name2_2 = 'texe-s' ;
$index4_main_picAndLink_name2_3 = 'href' ;
$index4_main_picAndLink_name2_4 = 'bgcolor' ;
$index4_main_picAndLink_name2_5 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;
$index4_main_picAndLink_name2_6 = 'layout' ;
$index4_main_picAndLink_name2_7 = 'align' ;
$index4_main_picAndLink_name2_8 = 'display' ;
$index4_main_picAndLink_p_1_texe_b = $index4_main_picAndLink_style2->$index4_main_picAndLink_name2_1 ;
$index4_main_picAndLink_p_1_texe_s = $index4_main_picAndLink_style2->$index4_main_picAndLink_name2_2 ;
$index4_main_picAndLink_p_1_href = $index4_main_picAndLink_style2->$index4_main_picAndLink_name2_3 ;
$index4_main_picAndLink_p_1_bgc = $index4_main_picAndLink_style2->$index4_main_picAndLink_name2_4 ;
$index4_main_picAndLink_p_1_bgi = $index4_main_picAndLink_name2_5 ;
$index4_main_picAndLink_p_1_layout = $index4_main_picAndLink_style2->$index4_main_picAndLink_name2_6 ;
$index4_main_picAndLink_p_1_align = $index4_main_picAndLink_style2->$index4_main_picAndLink_name2_7 ;
$index4_main_picAndLink_p_1_display = $index4_main_picAndLink_style2->$index4_main_picAndLink_name2_8 ;

//3.第二張圖
$index4_main_picAndLink_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$index4_main_picAndLink_name3_1 = 'texe-b' ;
$index4_main_picAndLink_name3_2 = 'texe-s' ;
$index4_main_picAndLink_name3_3 = 'href' ;
$index4_main_picAndLink_name3_4 = 'bgcolor' ;
$index4_main_picAndLink_name3_5 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE3"] ;
$index4_main_picAndLink_name3_6 = 'layout' ;
$index4_main_picAndLink_name3_7 = 'align' ;
$index4_main_picAndLink_name3_8 = 'display' ;
$index4_main_picAndLink_p_2_texe_b = $index4_main_picAndLink_style3->$index4_main_picAndLink_name3_1 ;
$index4_main_picAndLink_p_2_texe_s = $index4_main_picAndLink_style3->$index4_main_picAndLink_name3_2 ;
$index4_main_picAndLink_p_2_href = $index4_main_picAndLink_style3->$index4_main_picAndLink_name3_3 ;
$index4_main_picAndLink_p_2_bgc = $index4_main_picAndLink_style3->$index4_main_picAndLink_name3_4 ;
$index4_main_picAndLink_p_2_bgi = $index4_main_picAndLink_name3_5 ;
$index4_main_picAndLink_p_2_layout = $index4_main_picAndLink_style3->$index4_main_picAndLink_name3_6 ;
$index4_main_picAndLink_p_2_align = $index4_main_picAndLink_style3->$index4_main_picAndLink_name3_7 ;
$index4_main_picAndLink_p_2_display = $index4_main_picAndLink_style3->$index4_main_picAndLink_name3_8 ;

//3.第三張圖
$index4_main_picAndLink_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$index4_main_picAndLink_name4_1 = 'texe-b' ;
$index4_main_picAndLink_name4_2 = 'texe-s' ;
$index4_main_picAndLink_name4_3 = 'href' ;
$index4_main_picAndLink_name4_4 = 'bgcolor' ;
$index4_main_picAndLink_name4_5 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE4"] ;
$index4_main_picAndLink_name4_6 = 'layout' ;
$index4_main_picAndLink_name4_7 = 'align' ;
$index4_main_picAndLink_name4_8 = 'display' ;
$index4_main_picAndLink_p_3_texe_b = $index4_main_picAndLink_style4->$index4_main_picAndLink_name4_1 ;
$index4_main_picAndLink_p_3_texe_s = $index4_main_picAndLink_style4->$index4_main_picAndLink_name4_2 ;
$index4_main_picAndLink_p_3_href = $index4_main_picAndLink_style4->$index4_main_picAndLink_name4_3 ;
$index4_main_picAndLink_p_3_bgc = $index4_main_picAndLink_style4->$index4_main_picAndLink_name4_4 ;
$index4_main_picAndLink_p_3_bgi = $index4_main_picAndLink_name4_5 ;
$index4_main_picAndLink_p_3_layout = $index4_main_picAndLink_style4->$index4_main_picAndLink_name4_6 ;
$index4_main_picAndLink_p_3_align = $index4_main_picAndLink_style4->$index4_main_picAndLink_name4_7 ;
$index4_main_picAndLink_p_3_display = $index4_main_picAndLink_style4->$index4_main_picAndLink_name4_8 ;

//5.第四張圖
$index4_main_picAndLink_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$index4_main_picAndLink_name5_1 = 'texe-b' ;
$index4_main_picAndLink_name5_2 = 'texe-s' ;
$index4_main_picAndLink_name5_3 = 'href' ;
$index4_main_picAndLink_name5_4 = 'bgcolor' ;
$index4_main_picAndLink_name5_5 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE5"] ;
$index4_main_picAndLink_name5_6 = 'layout' ;
$index4_main_picAndLink_name5_7 = 'align' ;
$index4_main_picAndLink_name5_8 = 'display' ;
$index4_main_picAndLink_p_4_texe_b = $index4_main_picAndLink_style5->$index4_main_picAndLink_name5_1 ;
$index4_main_picAndLink_p_4_texe_s = $index4_main_picAndLink_style5->$index4_main_picAndLink_name5_2 ;
$index4_main_picAndLink_p_4_href = $index4_main_picAndLink_style5->$index4_main_picAndLink_name5_3 ;
$index4_main_picAndLink_p_4_bgc = $index4_main_picAndLink_style5->$index4_main_picAndLink_name5_4 ;
$index4_main_picAndLink_p_4_bgi = $index4_main_picAndLink_name5_5 ;
$index4_main_picAndLink_p_4_layout = $index4_main_picAndLink_style5->$index4_main_picAndLink_name5_6 ;
$index4_main_picAndLink_p_4_align = $index4_main_picAndLink_style5->$index4_main_picAndLink_name5_7 ;
$index4_main_picAndLink_p_4_display = $index4_main_picAndLink_style5->$index4_main_picAndLink_name5_8 ;

//6.文字
$index4_main_picAndLink_style6 = json_decode($record_design_style2["STYLE6"]) ; 
$index4_main_picAndLink_name6_1 = 'color' ;
$index4_main_picAndLink_name6_2 = 'color-hover' ;
$index4_main_picAndLink_a = $index4_main_picAndLink_style6->$index4_main_picAndLink_name6_1 ;
$index4_main_picAndLink_a_hover = $index4_main_picAndLink_style6->$index4_main_picAndLink_name6_2 ;

//7.按鈕
$index4_main_picAndLink_style7 = json_decode($record_design_style2["STYLE7"]) ; 
$index4_main_picAndLink_name7_1 = 'color' ;
$index4_main_picAndLink_name7_2 = 'bgcolor' ;
$index4_main_picAndLink_name7_3 = 'border-top' ;
$index4_main_picAndLink_name7_4 = 'border-right' ;
$index4_main_picAndLink_name7_5 = 'border-bottom' ;
$index4_main_picAndLink_name7_6 = 'border-left' ;
$index4_main_picAndLink_name7_7 = 'color-hover' ;
$index4_main_picAndLink_name7_8 = 'bgcolor-hover' ;
$index4_main_picAndLink_name7_9 = 'border-top-hover' ;
$index4_main_picAndLink_name7_10 = 'border-right-hover' ;
$index4_main_picAndLink_name7_11 = 'border-bottom-hover' ;
$index4_main_picAndLink_name7_12 = 'border-left-hover' ;
$index4_main_picAndLink_h3_color = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_1 ;
$index4_main_picAndLink_h3_bgcolor = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_2 ;
$index4_main_picAndLink_h3_border_top = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_3 ;
$index4_main_picAndLink_h3_border_right = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_4 ;
$index4_main_picAndLink_h3_border_bottom = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_5 ;
$index4_main_picAndLink_h3_border_left = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_6 ;
$index4_main_picAndLink_h3_color_hover = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_7 ;
$index4_main_picAndLink_h3_bgcolo_hoverr = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_8 ;
$index4_main_picAndLink_h3_border_top_hover = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_9 ;
$index4_main_picAndLink_h3_border_right_hover = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_10 ;
$index4_main_picAndLink_h3_border_bottom_hover = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_11 ;
$index4_main_picAndLink_h3_border_left_hover = $index4_main_picAndLink_style7->$index4_main_picAndLink_name7_12 ;
/*==== 取得四張大圖 End====*/
?>
<style>
/*68-四張大圖*/
    main#picAndLink{display:<?=$index4_main_picAndLink_display?>;background: <?=$index4_main_picAndLink_bgcolor?>;}/*1.整體區塊*/
    main#picAndLink .picAndLinkBtnBg_1 {background-color: <?=$index4_main_picAndLink_p_1_bgc?>;background-image: url(<?=$index4_main_picAndLink_p_1_bgi?>);text-align:<?=$index4_main_picAndLink_p_1_align?>;}
    .picAndLinkBtnBg_1_Wa{display:<?=$index4_main_picAndLink_p_1_display?>;}/*2.第一張圖(大標+小標+版面配置)共5個欄位*/

    main#picAndLink .picAndLinkBtnBg_2 {background-color: <?=$index4_main_picAndLink_p_2_bgc?>;background-image: url(<?=$index4_main_picAndLink_p_2_bgi?>);text-align: <?=$index4_main_picAndLink_p_2_align?>;}
    .picAndLinkBtnBg_2_Wa{display:<?=$index4_main_picAndLink_p_2_display?>;}/*3.第二張圖(大標+小標+版面配置)共5個欄位*/
    main#picAndLink .picAndLinkBtnBg_3 {background-color: <?=$index4_main_picAndLink_p_3_bgc?>;background-image: url(<?=$index4_main_picAndLink_p_3_bgi?>);text-align:<?=$index4_main_picAndLink_p_3_align?> ;}
    .picAndLinkBtnBg_3_Wa{display:<?=$index4_main_picAndLink_p_3_display?>;}/*4.第三張圖(大標+小標+版面配置)共5個欄位*/
    main#picAndLink .picAndLinkBtnBg_4 {background-color: <?=$index4_main_picAndLink_p_4_bgc?>;background-image: url(<?=$index4_main_picAndLink_p_4_bgi?>);text-align: <?=$index4_main_picAndLink_p_4_align?>;}
    .picAndLinkBtnBg_4_Wa{display:<?=$index4_main_picAndLink_p_4_display?>;}/*5.第四張圖(大標+小標+版面配置)共5個欄位*/
    main#picAndLink a {color: <?=$index4_main_picAndLink_a?>;}/*6.文字顏色*/
    main#picAndLink a:hover, main#picAndLink a:focus {color: <?=$index4_main_picAndLink_a_hover?>;}/*6.文字顏色 HOVER*/
    main#picAndLink h3 {color:<?=$index4_main_picAndLink_h3_color?>;background-color:<?=$index4_main_picAndLink_h3_bgcolor?>;border-top: <?=$index4_main_picAndLink_h3_border_top?>;border-right: <?=$index4_main_picAndLink_h3_border_right?>;border-bottom: <?=$index4_main_picAndLink_h3_border_bottom?>;border-left: <?=$index4_main_picAndLink_h3_border_left?>;}/*7.按鈕顏色、邊框*/    
    main#picAndLink a:hover>h3,main#picAndLink a:focus>h3{color: <?=$index4_main_picAndLink_h3_color_hover?>;background-color: <?=$index4_main_picAndLink_h3_bgcolo_hoverr?>;border-top: <?=$index4_main_picAndLink_h3_border_top_hover?>;border-right: <?=$index4_main_picAndLink_h3_border_right_hover?>;border-bottom: <?=$index4_main_picAndLink_h3_border_bottom_hover?>;border-left: <?=$index4_main_picAndLink_h3_border_left_hover?>;}/*7.按鈕顏色、邊框 HOVER*/
</style>

<?
/*==== 取得商品輪播 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 69"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.整體區塊
$index4_main_p1_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_main_p1_name1_1 = 'display' ;
$index4_main_p1_name1_2 = 'bgcolor' ;
$index4_main_p1_display = $index4_main_p1_style1->$index4_main_p1_name1_1 ;
$index4_main_p1_bgcolor = $index4_main_p1_style1->$index4_main_p1_name1_2 ;

//2.大標題
$index4_main_p1_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$index4_main_p1_name2_1 = 'color' ;
$index4_main_p1_name2_2 = 'border-color' ;
$index4_main_p1_name2_3 = 'color2' ;
$index4_main_p1_h1_color = $index4_main_p1_style2->$index4_main_p1_name2_1 ;
$index4_main_p1_h1_border_color = $index4_main_p1_style2->$index4_main_p1_name2_2 ;
$index4_main_p1_h1_color2 = $index4_main_p1_style2->$index4_main_p1_name2_3 ;

//3.商品
$index4_main_p1_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$index4_main_p1_name3_1 = 'color' ;
$index4_main_p1_name3_2 = 'color2' ;
$index4_main_p1_name3_3 = 'color-shadow' ;
$index4_main_p1_product_color = $index4_main_p1_style3->$index4_main_p1_name3_1 ;
$index4_main_p1_product_color2 = $index4_main_p1_style3->$index4_main_p1_name3_2 ;
$index4_main_p1_product_color_shadow = $index4_main_p1_style3->$index4_main_p1_name3_3 ;

//4.按鈕
$index4_main_p1_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$index4_main_p1_name4_1 = 'color' ;
$index4_main_p1_name4_2 = 'color-i' ;
$index4_main_p1_name4_3 = 'bgcolor' ;
$index4_main_p1_name4_4 = 'border-top' ;
$index4_main_p1_name4_5 = 'border-right' ;
$index4_main_p1_name4_6 = 'border-bottom' ;
$index4_main_p1_name4_7 = 'border-left' ;
$index4_main_p1_name4_8 = 'border-radius' ;
$index4_main_p1_name4_9 = 'color-hover' ;
$index4_main_p1_name4_10 = 'color-i-hover' ;
$index4_main_p1_name4_11 = 'bgcolor-hover' ;
$index4_main_p1_name4_12 = 'border-top-hover' ;
$index4_main_p1_name4_13 = 'border-right-hover' ;
$index4_main_p1_name4_14 = 'border-bottom-hover' ;
$index4_main_p1_name4_15 = 'border-left-hover' ;
$index4_main_p1_name4_16 = 'border-radius-hover' ;
$index4_main_p1_btn1_ind2_color = $index4_main_p1_style4->$index4_main_p1_name4_1 ;
$index4_main_p1_btn1_ind2_color_i = $index4_main_p1_style4->$index4_main_p1_name4_2 ;
$index4_main_p1_btn1_ind2_bgcolor = $index4_main_p1_style4->$index4_main_p1_name4_3 ;
$index4_main_p1_btn1_ind2_border_top = $index4_main_p1_style4->$index4_main_p1_name4_4 ;
$index4_main_p1_btn1_ind2_border_right = $index4_main_p1_style4->$index4_main_p1_name4_5 ;
$index4_main_p1_btn1_ind2_border_bottom = $index4_main_p1_style4->$index4_main_p1_name4_6 ;
$index4_main_p1_btn1_ind2_border_left = $index4_main_p1_style4->$index4_main_p1_name4_7 ;
$index4_main_p1_btn1_ind2_border_radius = $index4_main_p1_style4->$index4_main_p1_name4_8 ;
$index4_main_p1_btn1_ind2_color_hover = $index4_main_p1_style4->$index4_main_p1_name4_9 ;
$index4_main_p1_btn1_ind2_color_i_hover = $index4_main_p1_style4->$index4_main_p1_name4_10 ;
$index4_main_p1_btn1_ind2_bgcolor_hover = $index4_main_p1_style4->$index4_main_p1_name4_11 ;
$index4_main_p1_btn1_ind2_border_top_hover = $index4_main_p1_style4->$index4_main_p1_name4_12 ;
$index4_main_p1_btn1_ind2_border_right_hover = $index4_main_p1_style4->$index4_main_p1_name4_13 ;
$index4_main_p1_btn1_ind2_border_bottom_hover = $index4_main_p1_style4->$index4_main_p1_name4_14 ;
$index4_main_p1_btn1_ind2_border_left_hover = $index4_main_p1_style4->$index4_main_p1_name4_15 ;
$index4_main_p1_btn1_ind2_border_radius_hover = $index4_main_p1_style4->$index4_main_p1_name4_16 ;
/*==== 取得商品輪播 End====*/
?>
<style>
/*69-商品輪播*/
    main#p1{display:<?=$index4_main_p1_display?>;background-color: <?=$index4_main_p1_bgcolor?>;}/*1.整體區塊*/
    main#p1 h1.title-ind2 {color: <?=$index4_main_p1_h1_color?>;}/*2-1.大標題文字顏色*/
    main#p1 h1.title-ind2 span.titleLine {border-bottom: 4px solid <?=$index4_main_p1_h1_border_color?>;}/*2-2.標題線條顏色*/
    main#p1 h1.title-ind2 i, main#p1 h1.title2-ind2 i{color: <?=$index4_main_p1_h1_color2?>;}/*2-3.小標題文字顏色*/
    ul#owl-demo-product li h2 {color: <?=$index4_main_p1_product_color?>;}/*3-1.商品名稱文字顏色*/
    ul#owl-demo-product li h3 {color: <?=$index4_main_p1_product_color2?>;}/*3-2.預覽圖示文字顏色*/
    ul#owl-demo-product li:hover {box-shadow: 0px 0px 20px 2px <?=$index4_main_p1_product_color_shadow?>;}/*3-3.商品陰影顏色 HOVER*/
    main#p1 .btn1-ind2 {color: <?=$index4_main_p1_btn1_ind2_color?>;background-color: <?=$index4_main_p1_btn1_ind2_bgcolor?>;border-top: <?=$index4_main_p1_btn1_ind2_border_top?>;border-right: <?=$index4_main_p1_btn1_ind2_border_right?>;border-bottom: <?=$index4_main_p1_btn1_ind2_border_bottom?>;border-left: <?=$index4_main_p1_btn1_ind2_border_left?>;border-radius: <?=$index4_main_p1_btn1_ind2_border_radius?>;}/*4.按鈕文字背景線條顏色*/
    main#p1 .btn1-ind2 i {color: <?=$index4_main_p1_btn1_ind2_color_i?>;}/*4.按鈕箭頭顏色*/
    main#p1 .btn1-ind2:hover {color: <?=$index4_main_p1_btn1_ind2_color_hover?>;background-color: <?=$index4_main_p1_btn1_ind2_bgcolor_hover?>;border-top: <?=$index4_main_p1_btn1_ind2_border_top_hover?>;border-right: <?=$index4_main_p1_btn1_ind2_border_right_hover?>;border-bottom: <?=$index4_main_p1_btn1_ind2_border_bottom_hover?>;border-left: <?=$index4_main_p1_btn1_ind2_border_left_hover?>;border-radius: <?=$index4_main_p1_btn1_ind2_border_radius_hover?>;}/*4.按鈕文字背景線條顏色 HOVER*/
    main#p1 .btn1-ind2:hover i {color: <?=$index4_main_p1_btn1_ind2_color_i_hover?>;}/*4.按鈕箭頭顏色 HOVER*/
</style>

<?
/*==== 取得關於我們 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 70"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.整體區塊
$index4_main_p2_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_main_p2_name1_1 = 'display' ;
$index4_main_p2_name1_2 = 'bgcolor' ;
$index4_main_p2_name1_3 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;
$index4_main_p2_display = $index4_main_p2_style1->$index4_main_p2_name1_1 ;
$index4_main_p2_bgcolor = $index4_main_p2_style1->$index4_main_p2_name1_2 ;
$index4_main_p2_images = $index4_main_p2_name1_3 ;

//2.大標題
$index4_main_p2_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$index4_main_p2_name2_1 = 'color' ;
$index4_main_p2_name2_2 = 'border-color' ;
$index4_main_p2_name2_3 = 'color2' ;
$index4_main_p2_h1_color = $index4_main_p2_style2->$index4_main_p2_name2_1 ;
$index4_main_p2_h1_border_color = $index4_main_p2_style2->$index4_main_p2_name2_2 ;
$index4_main_p2_h1_color2 = $index4_main_p2_style2->$index4_main_p2_name2_3 ;

//3.主內容
$index4_main_p2_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$index4_main_p2_name3_1 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE3"] ;
$index4_main_p2_name3_2 = 'color' ;
$index4_main_p2_name3_3 = 'bgcolor' ;
$index4_main_p2_name3_4 = 'border-top' ;
$index4_main_p2_name3_5 = 'border-right' ;
$index4_main_p2_name3_6 = 'border-bottom' ;
$index4_main_p2_name3_7 = 'border-left' ;
$index4_main_p2_name3_8 = 'shadow' ;
$index4_main_p2_about_ind2_images = $index4_main_p2_name3_1 ;
$index4_main_p2_about_ind2_color = $index4_main_p2_style3->$index4_main_p2_name3_2 ;
$index4_main_p2_about_ind2_bgcolor = $index4_main_p2_style3->$index4_main_p2_name3_3 ;
$index4_main_p2_about_ind2_border_top = $index4_main_p2_style3->$index4_main_p2_name3_4 ;
$index4_main_p2_about_ind2_border_right = $index4_main_p2_style3->$index4_main_p2_name3_5 ;
$index4_main_p2_about_ind2_border_bottom = $index4_main_p2_style3->$index4_main_p2_name3_6 ;
$index4_main_p2_about_ind2_border_left = $index4_main_p2_style3->$index4_main_p2_name3_7 ;
$index4_main_p2_about_ind2_shadow = $index4_main_p2_style3->$index4_main_p2_name3_8 ;

//4.按鈕
$index4_main_p2_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$index4_main_p2_name4_1 = 'color' ;
$index4_main_p2_name4_2 = 'color-i' ;
$index4_main_p2_name4_3 = 'bgcolor' ;
$index4_main_p2_name4_4 = 'border-top' ;
$index4_main_p2_name4_5 = 'border-right' ;
$index4_main_p2_name4_6 = 'border-bottom' ;
$index4_main_p2_name4_7 = 'border-left' ;
$index4_main_p2_name4_8 = 'border-radius' ;
$index4_main_p2_name4_9 = 'color-hover' ;
$index4_main_p2_name4_10 = 'color-i-hover' ;
$index4_main_p2_name4_11 = 'bgcolor-hover' ;
$index4_main_p2_name4_12 = 'border-top-hover' ;
$index4_main_p2_name4_13 = 'border-right-hover' ;
$index4_main_p2_name4_14 = 'border-bottom-hover' ;
$index4_main_p2_name4_15 = 'border-left-hover' ;
$index4_main_p2_name4_16 = 'border-radius-hover' ;
$index4_main_p2_btn1_ind2_color = $index4_main_p2_style4->$index4_main_p2_name4_1 ;
$index4_main_p2_btn1_ind2_color_i = $index4_main_p2_style4->$index4_main_p2_name4_2 ;
$index4_main_p2_btn1_ind2_bgcolor = $index4_main_p2_style4->$index4_main_p2_name4_3 ;
$index4_main_p2_btn1_ind2_border_top = $index4_main_p2_style4->$index4_main_p2_name4_4 ;
$index4_main_p2_btn1_ind2_border_right = $index4_main_p2_style4->$index4_main_p2_name4_5 ;
$index4_main_p2_btn1_ind2_border_bottom = $index4_main_p2_style4->$index4_main_p2_name4_6 ;
$index4_main_p2_btn1_ind2_border_left = $index4_main_p2_style4->$index4_main_p2_name4_7 ;
$index4_main_p2_btn1_ind2_border_radius = $index4_main_p2_style4->$index4_main_p2_name4_8 ;
$index4_main_p2_btn1_ind2_color_hover = $index4_main_p2_style4->$index4_main_p2_name4_9 ;
$index4_main_p2_btn1_ind2_color_i_hover = $index4_main_p2_style4->$index4_main_p2_name4_10 ;
$index4_main_p2_btn1_ind2_bgcolor_hover = $index4_main_p2_style4->$index4_main_p2_name4_11 ;
$index4_main_p2_btn1_ind2_border_top_hover = $index4_main_p2_style4->$index4_main_p2_name4_12 ;
$index4_main_p2_btn1_ind2_border_right_hover = $index4_main_p2_style4->$index4_main_p2_name4_13 ;
$index4_main_p2_btn1_ind2_border_bottom_hover = $index4_main_p2_style4->$index4_main_p2_name4_14 ;
$index4_main_p2_btn1_ind2_border_left_hover = $index4_main_p2_style4->$index4_main_p2_name4_15 ;
$index4_main_p2_btn1_ind2_border_radius_hover = $index4_main_p2_style4->$index4_main_p2_name4_16 ;

/*==== 取得關於我們 End====*/
?>
<style>
/*70-關於我們*/
    main#p2{display:<?=$index4_main_p2_display?>;background-color: <?=$index4_main_p2_bgcolor?>;background-image: url(<?=$index4_main_p2_images?>);}/*1.整體區塊、背景顏色 圖片*/
    main#p2 h1.title-ind2 {color: <?=$index4_main_p2_h1_color?>;}/*2-1.大標題文字顏色*/
    main#p2 h1.title-ind2 span.titleLine {border-bottom: 4px solid <?=$index4_main_p2_h1_border_color?>;}/*2-2.標題線條顏色*/
    main#p2 h1.title-ind2 i, main#p1 h1.title2-ind2 i{color: <?=$index4_main_p2_h1_color2?>;}/*2-3.小標題文字顏色*/
    .about-ind2 .aboutWa .aboutWaImg {background-image: url(<?=$index4_main_p2_about_ind2_images?>);}/*3-1圖片*/
    .about-ind2 p.about-cont {color: <?=$index4_main_p2_about_ind2_color?>;}/*3-2文字顏色*/
    .about-ind2 .aboutWa {background-color: <?=$index4_main_p2_about_ind2_bgcolor?>; border-top: <?=$index4_main_p2_about_ind2_border_top?>; border-right: <?=$index4_main_p2_about_ind2_border_right?>;border-bottom: <?=$index4_main_p2_about_ind2_border_bottom?>;border-left: <?=$index4_main_p2_about_ind2_border_left?>;
    box-shadow: 0px 3px 20px <?=$index4_main_p2_about_ind2_shadow?>;}/*3-3背景顏色、邊框、陰影*/   
    main#p2 .btn1-ind2 {color: <?=$index4_main_p2_btn1_ind2_color?>;background-color: <?=$index4_main_p2_btn1_ind2_bgcolor?>;border-top: <?=$index4_main_p2_btn1_ind2_border_top?>;border-right: <?=$index4_main_p2_btn1_ind2_border_right?>;border-bottom: <?=$index4_main_p2_btn1_ind2_border_bottom?>;border-left: <?=$index4_main_p2_btn1_ind2_border_left?>;border-radius: <?=$index4_main_p2_btn1_ind2_border_radius?>;}/*4.按鈕文字背景線條顏色*/
    main#p2 .btn1-ind2 i {color: <?=$index4_main_p2_btn1_ind2_color_i?>;}/*4.按鈕箭頭顏色*/
    main#p2 .btn1-ind2:hover {color: <?=$index4_main_p2_btn1_ind2_color_hover?>;background-color: <?=$index4_main_p2_btn1_ind2_bgcolor_hover?>;border-top: <?=$index4_main_p2_btn1_ind2_border_top_hover?>;border-right: <?=$index4_main_p2_btn1_ind2_border_right_hover?>;border-bottom: <?=$index4_main_p2_btn1_ind2_border_bottom_hover?>;border-left: <?=$index4_main_p2_btn1_ind2_border_left_hover?>;border-radius: <?=$index4_main_p2_btn1_ind2_border_radius_hover?>;}/*4.按鈕文字背景線條顏色 HOVER*/
    main#p2 .btn1-ind2:hover i {color: <?=$index4_main_p2_btn1_ind2_color_i_hover?>;}/*4.按鈕箭頭顏色 HOVER*/
    
</style>

<?
/*==== 取得最新消息、聯絡我們 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 71"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.整體區塊
$index4_main_p3_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_main_p3_name1_1 = 'display' ;
$index4_main_p3_display = $index4_main_p3_style1->$index4_main_p3_name1_1 ;

//2.文字
$index4_main_p3_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$index4_main_p3_name2_1 = 'color' ;
$index4_main_p3_name2_2 = 'shadow' ;
$index4_main_p3_name2_3 = 'color2' ;
$index4_main_p3_h1_title_ind2_color = $index4_main_p3_style2->$index4_main_p3_name2_1 ;
$index4_main_p3_h1_title_ind2_shadow = $index4_main_p3_style2->$index4_main_p3_name2_2 ;
$index4_main_p3_h1_title_ind2_color2 = $index4_main_p3_style2->$index4_main_p3_name2_3 ;

//3.最新消息
$index4_main_p3_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$index4_main_p3_name3_1 = 'bgcolor' ;
$index4_main_p3_name3_2 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE3"] ;
$index4_main_p3_newsWaImg_bgcolor = $index4_main_p3_style3->$index4_main_p3_name3_1 ;
$index4_main_p3_newsWaImg_images = $index4_main_p3_name3_2 ;

//4.聯絡我們
$index4_main_p3_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$index4_main_p3_name4_1 = 'bgcolor' ;
$index4_main_p3_name4_2 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE4"] ;
$index4_main_p3_connectionWaImg_bgcolor = $index4_main_p3_style4->$index4_main_p3_name4_1 ;
$index4_main_p3_connectionWaImg_images = $index4_main_p3_name4_2 ;

/*==== 取得最新消息、聯絡我們 End====*/
?>
<style>
/*71-最新消息、聯絡我們*/
    main#p3{display:<?=$index4_main_p3_display?>;}/*1.整體區塊*/
    main#p3 h1.title-ind2 {color:<?=$index4_main_p3_h1_title_ind2_color?>; text-shadow: 0 0 10px <?=$index4_main_p3_h1_title_ind2_shadow?>;}/*2.大標文字、陰影顏色*/
    main h1.title-ind2 i, main h1.title2-ind2 i {color: <?=$index4_main_p3_h1_title_ind2_color2?>;}/*2.小標文字顏色*/
    .news-ind2 .newsWaImg {background-color: <?=$index4_main_p3_newsWaImg_bgcolor?>}/*3.最新消息背景顏色*/
    .news-ind2 .newsWaImg a {background-image: url(<?=$index4_main_p3_newsWaImg_images?>);}/*3.最新消息背景圖片*/
    .news-ind2 .connectionWaImg {background-color: <?=$index4_main_p3_connectionWaImg_bgcolor?>}/*4.聯絡我們背景顏色*/
    .news-ind2 .connectionWaImg a {background-image: url(<?=$index4_main_p3_connectionWaImg_images?>);}/*4.聯絡我們背景圖片*/
    
</style>


<?
/*==== 取得輪播 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 72"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.整體區塊
$index4_main_demo_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_main_demo_name1_1 = 'display' ;
$index4_main_demo_name1_2 = 'bgcolor' ;
$index4_main_demo_display = $index4_main_demo_style1->$index4_main_demo_name1_1 ;
$index4_main_demo_bgcolor = $index4_main_demo_style1->$index4_main_demo_name1_2 ;
/*==== 取得輪播 End====*/
?>
<style>
/*72-輪播*/
    main#demo {display:<?=$index4_main_demo_display?>;}/*1.整體區塊*/
    main#demo {background-color: <?=$index4_main_demo_bgcolor?>;}/*5.輪播背景顏色*/  
</style>

<?
/*==== 取得頁尾 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 73"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.整體區塊
$index4_main_footer_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$index4_main_footer_name1_1 = 'display' ;
$index4_main_footer_name1_2 = 'bgcolor' ;
$index4_main_footer_name1_3 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;
$index4_main_footer_display = $index4_main_footer_style1->$index4_main_footer_name1_1 ;
$index4_main_footer_bgcolor = $index4_main_footer_style1->$index4_main_footer_name1_2 ;
$index4_main_footer_images = $index4_main_footer_name1_3 ;

//2.至頂小圖示
$index4_main_footer_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$index4_main_footer_name2_1 = 'color' ;
$index4_main_footer_name2_2 = 'bgcolor' ;
$index4_main_footer_name2_3 = 'border' ;
$index4_main_footer_name2_4 = 'border-radius' ;
$index4_main_footer_name2_5 = 'color-hover' ;
$index4_main_footer_name2_6 = 'bgcolor-hover' ;
$index4_main_footer_name2_7 = 'border-hover' ;
$index4_main_footer_goTop2_color = $index4_main_footer_style2->$index4_main_footer_name2_1 ;
$index4_main_footer_goTop2_bgcolor = $index4_main_footer_style2->$index4_main_footer_name2_2 ;
$index4_main_footer_goTop2_border = $index4_main_footer_style2->$index4_main_footer_name2_3 ;
$index4_main_footer_goTop2_border_radius = $index4_main_footer_style2->$index4_main_footer_name2_4 ;
$index4_main_footer_goTop2_color_hover = $index4_main_footer_style2->$index4_main_footer_name2_5 ;
$index4_main_footer_goTop2_bgcolorhover = $index4_main_footer_style2->$index4_main_footer_name2_6 ;
$index4_main_footer_goTop2_borderhover = $index4_main_footer_style2->$index4_main_footer_name2_7 ;

//3.選單
$index4_main_footer_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$index4_main_footer_name3_1 = 'color' ;
$index4_main_footer_name3_2 = 'bgcolor' ;
$index4_main_footer_name3_3 = 'color-hover' ;
$index4_main_footer_mid_color = $index4_main_footer_style3->$index4_main_footer_name3_1 ;
$index4_main_footer_mid_bgcolor = $index4_main_footer_style3->$index4_main_footer_name3_2 ;
$index4_main_footer_mid_color_hover = $index4_main_footer_style3->$index4_main_footer_name3_3 ;

//4.搜尋文字框
$index4_main_footer_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$index4_main_footer_name4_1 = 'color-placeholder' ;
$index4_main_footer_name4_2 = 'color' ;
$index4_main_footer_name4_3 = 'bgcolor' ;
$index4_main_footer_name4_4 = 'border-top' ;
$index4_main_footer_name4_5 = 'border-right' ;
$index4_main_footer_name4_6 = 'border-bottom' ;
$index4_main_footer_name4_7 = 'border-left' ;
$index4_main_footer_search_pc_input_color_placeholder = $index4_main_footer_style4->$index4_main_footer_name4_1 ;
$index4_main_footer_search_pc_input_color = $index4_main_footer_style4->$index4_main_footer_name4_2 ;
$index4_main_footer_search_pc_input_bgcolor = $index4_main_footer_style4->$index4_main_footer_name4_3 ;
$index4_main_footer_search_pc_input_border_top = $index4_main_footer_style4->$index4_main_footer_name4_4 ;
$index4_main_footer_search_pc_input_border_right = $index4_main_footer_style4->$index4_main_footer_name4_5 ;
$index4_main_footer_search_pc_input_border_bottom = $index4_main_footer_style4->$index4_main_footer_name4_7 ;
$index4_main_footer_search_pc_input_border_left = $index4_main_footer_style4->$index4_main_footer_name4_7 ;

//5.搜尋按鈕
$index4_main_footer_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$index4_main_footer_name5_1 = 'color' ;
$index4_main_footer_name5_2 = 'bgcolor' ;
$index4_main_footer_name5_3 = 'border-top' ;
$index4_main_footer_name5_4 = 'border-right' ;
$index4_main_footer_name5_5 = 'border-bottom' ;
$index4_main_footer_name5_6 = 'border-left' ;
$index4_main_footer_name5_7 = 'color-hover' ;
$index4_main_footer_name5_8 = 'bgcolor-hover' ;
$index4_main_footer_name5_9 = 'border-top-hover' ;
$index4_main_footer_name5_10 = 'border-right-hover' ;
$index4_main_footer_name5_11 = 'border-bottom-hover' ;
$index4_main_footer_name5_12 = 'border-left-hover' ;
$index4_main_footer_search_pc_button_color = $index4_main_footer_style5->$index4_main_footer_name5_1 ;
$index4_main_footer_search_pc_button_bgcolor = $index4_main_footer_style5->$index4_main_footer_name5_2 ;
$index4_main_footer_search_pc_button_border_top = $index4_main_footer_style5->$index4_main_footer_name5_3 ;
$index4_main_footer_search_pc_button_border_right = $index4_main_footer_style5->$index4_main_footer_name5_4 ;
$index4_main_footer_search_pc_button_border_bottom = $index4_main_footer_style5->$index4_main_footer_name5_5 ;
$index4_main_footer_search_pc_buttont_border_left = $index4_main_footer_style5->$index4_main_footer_name5_6 ;
$index4_main_footer_search_pc_button_color_hover = $index4_main_footer_style5->$index4_main_footer_name5_7 ;
$index4_main_footer_search_pc_button_bgcolor_hover = $index4_main_footer_style5->$index4_main_footer_name5_8 ;
$index4_main_footer_search_pc_button_border_top_hover = $index4_main_footer_style5->$index4_main_footer_name5_9 ;
$index4_main_footer_search_pc_button_border_right_hover = $index4_main_footer_style5->$index4_main_footer_name5_10 ;
$index4_main_footer_search_pc_button_border_bottom_hover = $index4_main_footer_style5->$index4_main_footer_name5_11 ;
$index4_main_footer_search_pc_buttont_border_left_hover = $index4_main_footer_style5->$index4_main_footer_name5_12 ;

//6.底部文字
$index4_main_footer_style6 = json_decode($record_design_style2["STYLE6"]) ; 
$index4_main_footer_name6_1 = 'color' ;
$index4_main_footer_name6_2 = 'color2' ;
$index4_main_footer_footer_cont_color = $index4_main_footer_style6->$index4_main_footer_name6_1 ;
$index4_main_footer_footer_cont_color2 = $index4_main_footer_style6->$index4_main_footer_name6_2 ;

/*==== 取得頁尾 End====*/
?>
<style>
/*73-頁尾*/
    footer {display:<?=$index4_main_footer_display?>;background-color: <?=$index4_main_footer_bgcolor?>;background-image: url(<?=$index4_main_footer_images?>);}/*1.整體區塊、背景顏色、圖*/
    .footerTop #goTop2 {color: <?=$index4_main_footer_goTop2_color?>;background-color: <?=$index4_main_footer_goTop2_bgcolor?>;border: <?=$index4_main_footer_goTop2_border?>;border-radius: <?=$index4_main_footer_goTop2_border_radius?>;}/*2.至頂小圖示*/
    .footerTop #goTop2:hover {color: <?=$index4_main_footer_goTop2_color_hover?>;background-color: <?=$index4_main_footer_goTop2_bgcolorhover?>;border: <?=$index4_main_footer_goTop2_borderhover?>;}/*2.至頂小圖示 HOVER*/
    footer .mid ul li a {color: <?=$index4_main_footer_mid_color?>;}/*3.選單文字顏色*/
    footer .mid ul {background-color: <?=$index4_main_footer_mid_bgcolor?>;}/*3.選單背景顏色*/
    footer .mid ul li a:hover {color: <?=$index4_main_footer_mid_color_hover?>;}/*3.選單文字顏色 HOVER*/
    
    .footer-search-pc ::-webkit-input-placeholder {color: <?=$index4_main_footer_search_pc_input_color_placeholder?>;}/*4.搜尋文字框-預設文字顏色*/
    .footer-search-pc ::-moz-placeholder {color: <?=$index4_main_footer_search_pc_input_color_placeholder?>;}/*4.搜尋文字框-預設文字顏色*/
    .footer-search-pc :-ms-input-placeholder {color: <?=$index4_main_footer_search_pc_input_color_placeholder?>;}/*4.搜尋文字框-預設文字顏色*/
    .footer-search-pc :-moz-placeholder {color: <?=$index4_main_footer_search_pc_input_color_placeholder?>;}/*4.搜尋文字框-預設文字顏色*/
    .footer-search-pc input {color: <?=$index4_main_footer_search_pc_input_color?>;background-color: <?=$index4_main_footer_search_pc_input_bgcolor?>;border-top: <?=$index4_main_footer_search_pc_input_border_top?>;border-right: <?=$index4_main_footer_search_pc_input_border_right?>;border-bottom: <?=$index4_main_footer_search_pc_input_border_bottom?>;border-left: <?=$index4_main_footer_search_pc_input_border_left?>;}/*4.搜尋文字框*/

    .footer-search-pc button {color: <?=$index4_main_footer_search_pc_button_color?>;background-color: <?=$index4_main_footer_search_pc_button_bgcolor?>;border-top: <?=$index4_main_footer_search_pc_button_border_top?>;border-right: <?=$index4_main_footer_search_pc_button_border_right?>;border-bottom: <?=$index4_main_footer_search_pc_button_border_bottom?>;border-left: <?=$index4_main_footer_search_pc_buttont_border_left?>;}/*5搜尋按鈕*/
    .footer-search-pc button:hover {color: <?=$index4_main_footer_search_pc_button_color_hover?>;background-color: <?=$index4_main_footer_search_pc_button_bgcolor_hover?>;border-top: <?=$index4_main_footer_search_pc_button_border_top_hover?>;border-right: <?=$index4_main_footer_search_pc_button_border_right_hover?>;border-bottom: <?=$index4_main_footer_search_pc_button_border_bottom_hover?>;border-left: <?=$index4_main_footer_search_pc_buttont_border_left_hover?>;}/*5搜尋按鈕 HOVER*/

    footer p, footer p a, footer p a:hover, ul.footer-cont, ul.footer-cont li a,.visits_people {color: <?=$index4_main_footer_footer_cont_color?>;}/*6-1底部文字顏色*/
    footer p a:hover {opacity: 0.5;}
    ul.footer-cont li i {color: <?=$index4_main_footer_footer_cont_color2?>;}/*6-2底部圖示顏色*/
</style>

</head>






<body>
<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- <div id="gotop"></div> -->
<header class="index-nav">
    <!-- 電腦版 logo -->
    <nav class="topNav-pc">
        <div class="row">           
            
            <div class="topNav-logo-pc col-lg-12 col-md-12 col-sm-12"><a href="index.php?type=click"><img src="<?=FILE_PATH?>/ticker/<?=$computer_logo_image?>" alt=""></a></div>

        </div>
    </nav>



    <!-- 手機版 選單 -->
    <nav class="topNav-mob">

        <div class="topNav-logo-mob"><a href="index.php?type=click"><img src="<?=FILE_PATH?>/ticker/<?=$mobile_logo_image?>" alt=""></a></div>
        <i class="fa fa-bars"></i>
        <i class="search-btn fa fa-search"></i>
        <div class="search-dropdown-open" style="display: none;">
            <input type="text" placeholder="請輸入您要搜尋的關鍵字" id="mobile_keywords" name="keyword">
            <button type="button" value="搜尋" onclick="search_sent_mobile_check()" >搜尋</button>
        </div>
        <div class="topNav-mob-open">
            <i class="fa fa-times"></i>
            <div class="mb">

                <?
                if( $_SESSION['member_id'] == "") {
                    ?>
                    <!--<img src="index/images/profile.png" alt="" class="member">-->
                    <a href="signIn.php">登入</a>/
                    <a href="registered.php">註冊</a>
                    <?
                }
                else
                {
                    ?>
                    <!--<img src="index/images/profile.png" alt="" class="member">-->
                    <a href="profile.php"><?=$_SESSION['member_name']?></a>/
                    <a href="./signIn.php?logout=1">登出</a>
                    <?
                }
                ?>

            </div>
            <ul class="">

                <?
                foreach( $page1_array as $page1_id => $page1_id_array )
                {
                    foreach( $page1_id_array as $page1_name => $page1_type )
                    {
                ?>
                        <?
                        if( $page1_id == 1 ) //首頁
                        {
                        ?>
                            <li><a href="index.php?type=click&page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                        <?
                        }
                        ?>

                        <?
                        if( $page1_id == 2 ) //線上購物":"產品展示
                        {
                        ?>
                            <li><a href="#"><?=($web_style==SHOPPING_CART?$page1_name:$page1_name)?><span class="fa fa-angle-down"></span></a>
                                <ul class="topNav-mob-open2">
                                    <?
                                    foreach( $goods1_class as $goods1_id => $goods1_class_name )
                                    {
                                        ?>
                                        <li>
                                            <a href="<?=$products_url?>?page1_ID=<?=$page1_id?>&goods1_id=<?=$goods1_id?>"><?=$goods1_class_name?></a>
                                        </li>
                                        <?
                                    }
                                    ?>
                                </ul>
                            </li>
                        <?
                        }
                        ?>

                        <?
                        if( $page1_id == 3 ) //最新消息
                        {
                        ?>
                            <li><a href="news.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                        <?
                        }
                        ?>

                        <?
                        if( $page1_id == 4 ) //聯絡我們
                        {
                        ?>
                            <li><a href="connection.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                        <?
                        }
                        ?>

                        <?
                        if( $page1_id == 5 ) //相簿
                        {
                        ?>
                            <li><a href="photo.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                        <?
                        }
                        ?>

                        <?
                        if( $page1_id == 6 )
                        {
                        ?>
                        <li><!--產品問與答-->
                            <?
                            if( $_SESSION['member_id'] == "") {
                            ?>
                                <a href="#" class="alertbox-btn-noLogin"><?=$page1_name?></a>
                            <?
                            }
                            else
                            {
                            ?>
                                <a href="qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a>
                            <?
                            }
                            ?>
                        </li>
                        <?
                        }
                        ?>

                        <?
                        if( $page1_id == 7 )
                        {
                            ?>
                            <!--會員中心-->
                            <li><a href="#"><?=$page1_name?><span class="fa fa-angle-down"></span></a>
                                <ul class="topNav-mob-open2">

                                    <?
                                    if( $_SESSION['member_id'] == "") {
                                        ?>
                                        <li><a href="#" class="alertbox-btn-noLogin" >修改會員資料</a></li>
                                        <li><a href="#" class="alertbox-btn-noLogin" >訂單查詢</a></li>
                                        <li><a href="#" class="alertbox-btn-noLogin" >追蹤清單</a></li>
                                        <li><a href="#" class="alertbox-btn-noLogin" >購物車</a></li>
                                        <li><a href="servicerulePrivacy.php?page1_ID=<?=$page1_id?>">服務及隱私權條款</a></li>
                                        <!--<li><a href="common-qa.php?page1_ID=<?=$page1_id?>">常見Q & A</a></li>-->
                                        <li><a href="returns.php?page1_ID=<?=$page1_id?>">退換貨說明</a></li>
                                        <?
                                    }
                                    else
                                    {
                                        ?>
                                        <li><a href="profile.php?page1_ID=<?=$page1_id?>">修改會員資料</a></li>
                                        <li><a href="orderForm.php?page1_ID=<?=$page1_id?>">訂單查詢</a></li>
                                        <li><a href="track.php?page1_ID=<?=$page1_id?>">追蹤清單</a></li>
                                        <li><a href="shoppingCart.php?page1_ID=<?=$page1_id?>&ID=<?=$order_item?>">購物車</a></li>
                                        <li><a href="servicerulePrivacy.php?page1_ID=<?=$page1_id?>">服務及隱私權條款</a></li>
                                        <!--<li><a href="common-qa.php?page1_ID=<?=$page1_id?>">常見Q & A</a></li>-->
                                        <li><a href="returns.php?page1_ID=<?=$page1_id?>">退換貨說明</a></li>
                                        <?
                                    }
                                    ?>

                                </ul>
                            </li>
                            <?
                        }
                        ?>

                        <?
                        if( $page1_id == 8 ) //關於我們
                        {
                        ?>
                            <li><a href="about.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                        <?
                        }
                        ?>

                        <?
                        if( $page1_id == 9 ) //常見Ｑ＆Ａ
                        {
                            ?>
                            <li><a href="common-qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                            <?
                        }
                        ?>

                        <?
                        if( $page1_id > 9 && $page1_type == 2 )
                        {
                            ?>
                            <li><!--自訂頁面-->
                                <a href="page.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?></a>
                            </li>
                            <?
                        }
                        ?>

                        <?
                        if( $page1_id > 9 && $page1_type == 3 )
                        {
                            ?>
                            <li><!--自訂頁面-->
                                <a href="pageDetailed.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?></a>
                            </li>
                            <?
                        }
                        ?>

                        <?
                        if( $page1_id > 9 && $page1_type == 4 )
                        {
                            $all_page_name_array["pageDetailed_".$page1_id] = $page1_name ;
                        ?>
                            <li><!--自訂頁面 連結頁面-->
                                <a href="<?=$page1_url_array[$page1_id]?>"><?=$page1_name?>
                                    
                                </a>
                            </li>
                        <?
                        }
                        ?>

                <?
                    }
                }
                ?>
                
            </ul>
        </div>
        <div class="topNav-mob-Wa">1</div>
    </nav>
</header>

<? include("right_button.php"); ?>


<!--首頁內容-->


<!--輪播-->
<div class="slideWa">
    <ul class="slidedown-btn mainnav floating">
        <li class="scroll-1"><a href="#p2"><!--<i class="fa fa-angle-down"></i>--></a></li>
    </ul>

    <!--<div class="slideWa-txt animations-1">
        <h1 class="animations-1">響應式<br>讓您的網站像水一樣</h1>
        <h2 class="">一次解決各種螢幕解析相容問題<br>
            網站　做一個就好
        </h2>
        <h3 class="animations-2"><a href="#">了解更多</a></h3>
    </div>-->
    <picture id="main-slide-photpshow" class="owl-carousel_B">

        <? //上方圖片輪播
        $query_index_cycle  = "select * from index_cycle2 where HIDE_ID =0 and ON_LEVEL_ID = 4 order by LEVEL ASC" ;
        $result_index_cycle = mysql_query($query_index_cycle)or die(mysql_error());
        while( $record_index_cycle = mysql_fetch_array($result_index_cycle) )
        {
            $index_img_link[] = $record_index_cycle["LINK"] ;
            $index_img_image[] = $record_index_cycle["IMAGE"] ;
            $index_img_image_m[] = $record_index_cycle["IMAGE_M"] ;
            $index_img_image_s[] = $record_index_cycle["IMAGE_S"] ;

            if( $record_index_cycle["IMAGE_M"] == '' ) $index_img_image_m[] = $record_index_cycle["IMAGE"] ;
            if( $record_index_cycle["IMAGE_S"] == '' ) $index_img_image_s[] = $record_index_cycle["IMAGE"] ;

        }

        foreach( $index_img_image as $v1_no => $v2_image )
        {
        ?>
            <div class="item">
                <a href="<?=$index_img_link[$v1_no]?>" target="_blank" >
                    <picture>

                        <!--[if IE 9]><img srcset="<?=FILE_PATH?>/index_cycle2/<?=$v2_image?>" alt=""><![endif]-->

                        <source srcset="<?=FILE_PATH?>/index_cycle2/<?=$v2_image?>" media="(min-width: 768px)">
                        <source srcset="<?=FILE_PATH?>/index_cycle2/<?=$index_img_image_m[$v1_no]?>" media="(min-width: 568px)">
                        <img srcset="<?=FILE_PATH?>/index_cycle2/<?=$index_img_image_s[$v1_no]?>" alt="" class="ie9-no">

                    </picture>
                </a>
            </div>
        <?
        }
        ?>

    </picture>
</div>


<header class="index-nav">
    <!-- 電腦版 選單 -->
    <nav class="topNav-pc">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                

                    <ul class="topNav-pc-ul mainnav">
                        

                        <?
                        foreach( $page1_array as $page1_id => $page1_id_array )
                        {
                            foreach( $page1_id_array as $page1_name => $page1_type )
                            {
                        ?>
                                <?
                                if( $page1_id == 1 ) //首頁
                                {
                                ?>
                                    <li> <a href="index.php?type=click&page1_ID=<?=$page1_id?>"><?=$page1_name?><span>Index</span></a></li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id == 2 ) //線上購物or產品展示
                                {
                                    $product_list_page_name["tw"] = $page1_name ;
                                    $product_list_page_name["en"] = $page1_name_array[$page1_name] ;
                                ?>
                                    <li class="dropdown-btn-pc scroll-1"><a href="<?=$products_url?>?page1_ID=<?=$page1_id?>"><?=($web_style==SHOPPING_CART?$page1_name:$page1_name)?><i class="fa fa-caret-down"></i><span>Products</span></a>
                                        <div class="dropdown-pc">
                                            <ul class="menuScrollbar" data-mcs-theme="minimal-dark">
                                                <?
                                                foreach( $goods1_class as $goods1_id => $goods1_class_name )
                                                {
                                                    ?>
                                                    <li>
                                                        <a href="<?=$products_url?>?page1_ID=<?=$page1_id?>&goods1_id=<?=$goods1_id?>"><?=$goods1_class_name?></a>
                                                    </li>
                                                    <?
                                                }
                                                ?>
                                            </ul>
                                        </div>
                                    </li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id == 3 ) //最新消息
                                {
                                    $news_list_page_name["tw"] = $page1_name ;
                                    $news_list_page_name["en"] = $page1_name_array[$page1_name] ;
                                ?>
                                    <li class="scroll-3"><a href="news.php?page1_ID=<?=$page1_id?>"><?=$page1_name?><span>News</span></a></li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id == 4 ) //聯絡我們
                                {
                                    $connection_list_page_name["tw"] = $page1_name ;
                                    $connection_list_page_name["en"] = $page1_name_array[$page1_name] ;
                                ?>
                                    <li> <a href="connection.php?page1_ID=<?=$page1_id?>"><?=$page1_name?><span>Connection</span></a></li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id == 5 ) //相簿
                                {
                                ?>
                                    <li> <a href="photo.php?page1_ID=<?=$page1_id?>"><?=$page1_name?><span>Photo</span></a></li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id == 6 ) //產品問與答
                                {
                                ?>
                                    <li>
                                        <?
                                        if( $_SESSION['member_id'] == "") {
                                            ?>
                                            <a href="#" class="alertbox-btn-noLogin"><?=$page1_name?><span>Q&A</span></a>
                                            <?
                                        }
                                        else
                                        {
                                            ?>
                                            <a href="qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?><span>Q&A</span></a>
                                            <?
                                        }
                                        ?>
                                    </li>
                                <?
                                }
                                ?>

                                <?
                            if( $page1_id == 7 )
                            {
                                $all_page_name_array["member_center"] = $page1_name ;
                            ?>
                                <li class="dropdown-btn-pc scroll-1"><!--會員中心-->
                                    <a href="#"><?=$page1_name?><i class="fa fa-caret-down"></i>
                                          <span><?=$page1_name_array[$page1_name]?></span></a>
                                    <div class="dropdown-pc">
                                        <ul>

                                            <?
                                            if( $_SESSION['member_id'] == "") {
                                            ?>
                                                <?
                                                if(  $web_style == SHOPPING_CART  )
                                                {
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >修改會員資料</a></li>' ;
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >訂單查詢</a></li>' ;
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >追蹤清單</a></li>' ;
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >購物車</a></li>' ;
                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                    echo '<li><a href="returns.php?page1_ID='.$page1_id.'">退換貨說明</a></li>' ;
                                                }
                                                else
                                                {
                                                    echo '<li><a href="#" class="alertbox-btn-noLogin" >修改會員資料</a></li>' ;
                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                }
                                                ?>
                                            <?
                                            }
                                            else
                                            {
                                            ?>

                                                <?
                                                if(  $web_style == SHOPPING_CART  )
                                                {
                                                    echo '<li><a href="profile.php?page1_ID='.$page1_id.'">修改會員資料</a></li>' ;
                                                    echo '<li><a href="orderForm.php?page1_ID='.$page1_id.'">訂單查詢</a></li>' ;
                                                    echo '<li><a href="track.php?page1_ID='.$page1_id.'">追蹤清單</a></li>' ;

                                                    if( $order_commodity_number == 0 )
                                                    {
                                                        echo '<li><a href="#" class="alertbox-btn-noShopping" >購物車</a></li>' ;

                                                    }
                                                    else
                                                    {
                                                        echo '<li><a href="shoppingCart.php?page1_ID='.$page1_id.'&ID='.$order_item.'" >購物車</a></li>' ;

                                                    }

                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                    echo '<li><a href="returns.php?page1_ID='.$page1_id.'">退換貨說明</a></li>' ;
                                                }
                                                else
                                                {
                                                    echo '<li><a href="profile.php?page1_ID='.$page1_id.'" >修改會員資料</a></li>' ;
                                                    echo '<li><a href="servicerulePrivacy.php?page1_ID='.$page1_id.'">服務及隱私權條款</a></li>' ;
                                                    //echo '<li><a href="common-qa.php?page1_ID='.$page1_id.'">常見Q & A</a></li>' ;
                                                }
                                                ?>
                                            <?
                                            }
                                            ?>

                                        </ul>
                                    </div>
                                </li>
                            <?
                            }
                            ?>

                                <?
                                if( $page1_id == 8 ) //關於我們
                                {
                                    $about_list_page_name["tw"] = $page1_name ;
                                    $about_list_page_name["en"] = $page1_name_array[$page1_name] ;
                                ?>
                                    <li class="scroll-2"><a href="about.php?page1_ID=<?=$page1_id?>" class="color-topNav-1"><?=$page1_name?><span><?=$page1_name?></span></a></li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id == 9 ) //常見Ｑ＆Ａ
                                {
                                    ?>
                                    <li> <a href="common-qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?><span>ＱＡ</span></a></li>
                                    <?
                                }
                                ?>

                                <?
                                if( $page1_id > 9 && $page1_type == 2 ) //自訂頁面(雙頁)
                                {
                                ?>
                                    <li> <a href="page.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?><span>Page</span></a></li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id > 9 && $page1_type == 3 ) //自訂頁面(單頁)
                                {
                                ?>
                                    <li> <a href="pageDetailed.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?><span>Page</span></a></li>
                                <?
                                }
                                ?>

                                <?
                                if( $page1_id > 9 && $page1_type == 4 )
                                {
                                    $all_page_name_array["pageDetailed_".$page1_id] = $page1_name ;
                                ?>
                                    <li><!--自訂頁面 連結頁面-->
                                        <a href="<?=$page1_url_array[$page1_id]?>"><?=$page1_name?>
                                            
                                        </a>
                                    </li>
                                <?
                                }
                                ?>

                        <?
                            }
                        }
                        ?>


                        <?
                        if( $_SESSION['member_id'] == "") {
                        ?>
                            <li class="user">
                                
                                    <p>
                                        <i class="fa fa-user fa-user"></i>
                                        <a id="login-link" href="signIn.php">登入</a> /
                                        <a id="login-link" href="registered.php">註冊</a>
                                    </p>
                                
                            </li>
                        <?
                        }
                        else
                        {
                        ?>
                            <li  class="user">
                                
                                    <p>
                                        <i class="fa fa-user fa-user"></i>
                                        <a id="login-link" href="profile.php"><?=$_SESSION['member_name']?></a> /
                                        <a id="login-link" href="./signIn.php?logout=1">登出</a>
                                    </p>
                                
                            </li>
                        <?
                        }
                        ?>

                    </ul>
                
            </div>
        </div>
    </nav>
</header>

<main class="mainkINwA">
<main class="" id="picAndLink">
    <div class="row container_grid">
        <div class="col-lg-<?=$index4_main_picAndLink_p_1_layout?> col-md-<?=$index4_main_picAndLink_p_1_layout?> col-xs-12 picAndLinkBtnBg_1_Wa">
            <a href="<?=$index4_main_picAndLink_p_1_href?>" class="picAndLinkBtnBg_1">
                <h1><?=$index4_main_picAndLink_p_1_texe_b?></h1>
                <h2><?=$index4_main_picAndLink_p_1_texe_s?></h2>
                <h3 class="picAndLinkBtn_1">MORE</h3>
                <span></span>
            </a>
        </div>
        <div class="col-lg-<?=$index4_main_picAndLink_p_2_layout?> col-md-<?=$index4_main_picAndLink_p_2_layout?> col-xs-12 picAndLinkBtnBg_2_Wa">
            <a href="<?=$index4_main_picAndLink_p_2_href?>" class="picAndLinkBtnBg_2">
                <h1><?=$index4_main_picAndLink_p_2_texe_b?></h1>
                <h2><?=$index4_main_picAndLink_p_2_texe_s?></h2>
                <h3 class="picAndLinkBtn_2">MORE</h3>
                <span></span>
            </a>
        </div>
        <div class="col-lg-<?=$index4_main_picAndLink_p_3_layout?> col-md-<?=$index4_main_picAndLink_p_3_layout?> col-xs-12 picAndLinkBtnBg_3_Wa">
            <a href="<?=$index4_main_picAndLink_p_3_href?>" class="picAndLinkBtnBg_3">
                <h1><?=$index4_main_picAndLink_p_3_texe_b?></h1>
                <h2><?=$index4_main_picAndLink_p_3_texe_s?></h2>
                <h3 class="picAndLinkBtn_3">MORE</h3>
                <span></span>
            </a>
        </div>
        <div class="col-lg-<?=$index4_main_picAndLink_p_4_layout?> col-md-<?=$index4_main_picAndLink_p_4_layout?> col-xs-12 picAndLinkBtnBg_4_Wa">
            <a href="<?=$index4_main_picAndLink_p_4_href?>" class="picAndLinkBtnBg_4">
                <h1><?=$index4_main_picAndLink_p_4_texe_b?></h1>
                <h2><?=$index4_main_picAndLink_p_4_texe_s?></h2>
                <h3 class="picAndLinkBtn_4">MORE</h3>
                <span></span>
            </a>
        </div>

    </div>

</main>



<main class="" id="p1">
    <!--追蹤清單提示框-->
    <div class="ui-ios-overlay-cross">
        <div class="ui-ios-overlay-icon fa fa-check-circle-o"></div>
        <div class="ui-ios-overlay-txt">已加入追蹤</div>
    </div>
    <div class="ui-ios-overlay-checkMark">
        <div class="ui-ios-overlay-icon fa fa-times-circle-o"></div>
        <div class="ui-ios-overlay-txt">已取消追蹤</div>
    </div>
    <!--追蹤清單提示框 結束-->
    <!--<section id="example-1">

        <div class="example-container">
            <img src="http://www.justinaguilar.com/animations/images/phone.png" alt="Image of a phone" id="phone" class="animate" />

            <div class="copy">
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                <p>
                    Vestibulum euismod odio quis pretium hendrerit. Pellentesque fringilla suscipit ipsum ut euismod. Phasellus quis porttitor eros, vitae luctus est. Vivamus ut porta massa. Suspendisse id erat placerat, pulvinar nunc at, fringilla risus. Aliquam erat volutpat.
                </p>
                <h3>
                    .slideUp
                </h3>
            </div>

        </div>-->


    <div class="products-ind2 container_grid">
        <h1 class="title-ind2 row smoove" data-move-y="20%">
            <span class="titleLine col-lg"></span>
            <span><?=($web_style==COMPANY_IMAGE?$product_list_page_name["tw"]:$product_list_page_name["tw"])?></span>
            <span class="titleLine"></span>
            <i><?=$product_list_page_name["en"]?></i>
        </h1>

        <!--<ul id="productsShow" class="row ">-->

        <ul id="owl-demo-product" class="row owl-carousel-product smoove" data-move-y="20%">


            <?
            if( $web_style == COMPANY_IMAGE )
            {
            ?>
                <? //產品資訊

                $goods3_track_status = '' ;
                $sql_member_ser = '' ;
                if( $_SESSION['member_id'] == "" ) $goods3_track_status = "alertbox-btn-noLogin" ;
                else $sql_member_ser = ' and MEMBER_ID = ' . $_SESSION['member_id'] ;

                $query_goods3  = "select * from image_goods3 where HIDE_ID =0 and UP_STATUS = 0 and NEWS = 1 order by EDIT_TIME DESC limit 3333 " ;
                $result_goods3 = mysql_query($query_goods3)or die(mysql_error());
                while( $record_goods3 = mysql_fetch_array($result_goods3) )
                {
                    $goods3_id = $record_goods3["ID"] ;
                    $goods3_name = mb_substr($record_goods3["NAME"],0,30,'utf8')  ; //商品名稱
                    $goods3_image1 = $record_goods3["IMAGE1"] ; //圖片
                    $goods3_self_price = $record_goods3["SELL_PRICE"] ; //售價
                    $goods3_content = mb_substr(strip_tags($record_goods3["CONTENT"]) , 0 , 60 ,'utf8' )  ;
                    $goods3_views_count = $record_goods3["VIEWS_COUNT"] ;
                    $goods3_brief = $record_goods3["BRIEF"] ;
                ?>

                    <li class="col-lg-3 col-md-4 col-xs-6 item-product">
                        <h1 class="abgne-frame-20140107-1"><span></span>
                            <a href="productsDetailed-images.php?page1_ID=2&request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>"></a>
                            <img class="lazy lazyOwl" data-src="<?=FILE_PATH?>/image_goods3/<?=$goods3_image1?>" src="<?=FILE_PATH?>/image_goods3/<?=$goods3_image1?>" >
                        </h1>
                        <h2><div class="h2B"><?=$goods3_name?></div>
                        </h2>
                    </li>

                <?
                }
                ?>

                </ul>
                <div class="btnWap smoove" data-move-y="20%">
                    <button type="" class="btn1-ind2 " onclick="location.href='products-images.php?page1_ID=2'">Read more<i class="fa fa-long-arrow-right"></i></button>
                </div>

            <?
            }
            else
            {
            ?>

                <? //產品資訊-購物

                $goods3_track_status = '' ;
                $sql_member_ser = '' ;
                if( $_SESSION['member_id'] == "" ) $goods3_track_status = "alertbox-btn-noLogin" ;
                else $sql_member_ser = ' and MEMBER_ID = ' . $_SESSION['member_id'] ;

                $query_goods3  = "select * , (SELECT COUNT(*) from goods_tracking where GOODS3_ID = goods3.ID ".$sql_member_ser." ) AS track_count
                                    from goods3 where HIDE_ID =0 and UP_STATUS = 0 and NEWS = 1 order by VIEWS_COUNT DESC limit 3333 " ;
                $result_goods3 = mysql_query($query_goods3)or die(mysql_error());
                while( $record_goods3 = mysql_fetch_array($result_goods3) )
                {
                    $track_count = $record_goods3["track_count"] ;

                    if( $_SESSION['member_id'] != "" )
                    {  //判斷商品是否有被追蹤了嗎？ 如果是追蹤中 為cross 如果是未追蹤 checkMark

                        if( $track_count > 0 ) $goods3_track_status = 'cross' ;
                        else $goods3_track_status = 'checkMark' ;
                    }

                    $goods3_id = $record_goods3["ID"] ;
                    $goods3_name = mb_substr($record_goods3["NAME"],0,30,'utf8')  ; //商品名稱
                    $goods3_image1 = $record_goods3["IMAGE1"] ; //圖片
                    $goods3_self_price = $record_goods3["SELL_PRICE"] ; //售價
                    $goods3_content = mb_substr(strip_tags($record_goods3["CONTENT"]) , 0 , 60 ,'utf8' )  ;
                    $goods3_views_count = $record_goods3["VIEWS_COUNT"] ;
                    $goods3_brief = $record_goods3["BRIEF"] ;
                    ?>

                    <li class="col-lg-3 col-md-4 col-xs-6 item-product">
                        <h1 class="abgne-frame-20140107-1"><span></span>
                            <a href="productsDetailed.php?page1_ID=2&request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>"></a>
                            <img class="lazy lazyOwl" data-src="<?=FILE_PATH?>/goods3/<?=$goods3_image1?>" data-original="<?=FILE_PATH?>/goods3/<?=$goods3_image1?>" >
                        </h1>
                        <h2><div class="h2B"><?=$goods3_name?></div>
                        </h2>
                        <h3>
                            <div class="h3A"><i class="fa fa-eye"></i>&nbsp;<?=$goods3_views_count?></div>

                            <!-- 追蹤區塊 -->
                            <div class="h3B">
                                <span track_status="<?=$goods3_track_status?>" goods3_id="<?=$goods3_id?>" class="fa fa-heart <?=$goods3_track_status?>"></span>
                            </div>

                        </h3>
                    </li>

                <?
                }
                ?>

                </ul>
                <div class="btnWap smoove" data-move-y="20%">
                    <button type="" class="btn1-ind2 " onclick="location.href='products.php?page1_ID=2'">Read more<i class="fa fa-long-arrow-right"></i></button>
                </div>

            <?
            }
            ?>

    </div>
</main>

<!--<div id="owl-demo-product" class="owl-carousel-product">
                <div class="item-product">
                <img class="lazyOwl" data-src="owl-carousel-product/assets/owl1.jpg" alt="Lazy Owl Image">
                </div>
                <div class="item-product">
                <img class="lazyOwl" data-src="owl-carousel-product/assets/owl2.jpg" alt="Lazy Owl Image">
                </div>
                <div class="item-product">
                <img class="lazyOwl" data-src="owl-carousel-product/assets/owl3.jpg" alt="Lazy Owl Image">
                </div>
                <div class="item-product">
                <img class="lazyOwl" data-src="owl-carousel-product/assets/owl2.jpg" alt="Lazy Owl Image">
                </div>
                <div class="item-product">
                <img class="lazyOwl" data-src="owl-carousel-product/assets/owl3.jpg" alt="Lazy Owl Image">
                </div>
                
        </div>-->


<main class="" id="p2">
    <div class="about-ind2" id="second">
        <div class="container_grid">
            <h1 class="title-ind2 row smoove" data-move-y="30%">
                <span class="titleLine col-lg"></span>
                <span><?=$about_list_page_name["tw"]?></span>
                <span class="titleLine"></span>
                <i><?=$about_list_page_name["en"]?></i>
            </h1>

            <div class="row aboutWa smoove" data-move-y="30%">
                <div class="col-lg-5 col-md-4 col-xs-12 abgne-frame-20140107-1 aboutWaImg"><span></span> </div>
                <div class="col-lg-7 col-md-8 col-xs-12"><p class="about-cont JQellipsis"><?=$introduction_summary?></p></div>
            </div>
            <div class="btnWap smoove" data-move-y="20%">
                <button type="" class="btn1-ind2" onclick="location.href='about.php?page1_ID=8'">Read more<i class="fa fa-long-arrow-right"></i></button>
            </div>
        </div>

    </div>
</main>

<main class="" id="p3">
    <div class="news-ind2 pullUp">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 newsWaImg">
                <a href="news.php?page1_ID=11">
                    <h1 class="title-ind2 ">
                        <span><?=$news_list_page_name["en"]?></span>
                        <i><?=$news_list_page_name["tw"]?></i>
                    </h1>
                </a>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 connectionWaImg">
                <a href="connection.php?page1_ID=4">
                    <h1 class="title-ind2 ">
                        <span><?=$connection_list_page_name["en"]?></span>
                        <i><?=$connection_list_page_name["tw"]?></i>
                    </h1>
                </a>
            </div>
        </div>

    </div>

</main>

<main id="demo">
    <div class="container">
        <div class="row">
            <div class="span12">
                <div id="owl-demo" class="owl-carousel">

                    <?//廣告連結區
                    $query_related_coverage = "select * from related_coverage where HIDE_ID =0 order by LEVEL ASC" ;
                    $result_related_coverage = mysql_query( $query_related_coverage ) or die( mysql_error() ) ;
                    while( $record_related_coverage = mysql_fetch_array( $result_related_coverage ) )
                    {
                        $ID = $record_related_coverage["ID"];
                        $IMAGE = $record_related_coverage["IMAGE"] ; //圖片
                        $LINK = $record_related_coverage["LINK"] ; //連結
                        $TITLE = $record_related_coverage["TITLE"] ; //標題
                        ?>
                        <div class="item">
                            <a href="<?=$LINK?>" target="_blank">
                                <img src="<?=FILE_PATH?>/related_coverage/<?=$IMAGE?>" alt="<?=$TITLE?>">
                            </a>
                        </div>
                        <?
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</main>

</main>



<footer class="container_grid-fluid">
    <!--totop-->
    <div class="footerTop">
        <a href="#" id="goTop2"><span class="fa fa-chevron-up"></span></a>
    </div>
    <div class="" id="second">
        <div class="container_grid">
            <div class="mid mainnav">
                <ul class="mainnav">
                    <?
                    foreach( $page1_array as $page1_id => $page1_id_array )
                    {
                        foreach( $page1_id_array as $page1_name => $page1_type )
                        {
                    ?>
                            <?
                            if( $page1_id == 1 ) //首頁
                            {
                            ?>
                                <li><a href="index.php?type=click&page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 2 ) //線上購物
                            {
                                $product_list_page_name["tw"] = $page1_name ;
                                $product_list_page_name["en"] = $page1_name_array[$page1_name] ;
                            ?>
                                <li class="dropdown-btn-pc scroll-1"><a href="#p1"><?=($web_style==SHOPPING_CART?$page1_name:$page1_name)?></a></li>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 3 )
                            {
                                $news_list_page_name["tw"] = $page1_name ;
                                $news_list_page_name["en"] = $page1_name_array[$page1_name] ;
                                ?>
                                <!--最新消息-->
                                <li class="scroll-3"><a href="news.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id == 4 )
                            {
                                ?>
                                <!--聯絡我們-->
                                <li> <a href="connection.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id == 5 )
                            {
                                ?>
                                <!--相簿-->
                                <li><a href="photo.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id == 6 )
                            {
                            ?>
                            <?
                            if( $_SESSION['member_id'] == "") {
                                ?>
                                <!--產品問與答-->
                                <li><a href="#" class="alertbox-btn-noLogin"><?=$page1_name?></a></li>
                                <?
                            }
                            else
                            {
                                ?>
                                <li><a href="qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>
                            <?
                            }
                            ?>

                            <?
                            if( $page1_id == 7 )
                            {
                            ?>
                            <?
                            if( $_SESSION['member_id'] == "") {
                                ?>
                                <!--會員中心-->
                                <li><a href="#" class="alertbox-btn-noLogin"><?=$page1_name?></a></li>
                                <?
                            }
                            else
                            {
                                ?>
                               <li><a href="profile.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>
                            <?
                            }
                            ?>


                            <?
                            if( $page1_id == 8 )
                            {
                                ?>
                                <!--關於我們-->
                                <li class="scroll-2"><a href="#p2" class="color-topNav-1"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id == 9 ) //常見Ｑ＆Ａ
                            {
                                ?>
                                <li><a href="common-qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id > 9 && $page1_type == 2 )
                            {
                                ?>
                                <!--共用頁面-->
                                <li><a href="page.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>

                            <?
                            if( $page1_id > 9 && $page1_type == 3 )
                            {
                                ?>
                                <li><a href="pageDetailed.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?></a></li>
                                <?
                            }
                            ?>


                            <?
                        }
                    }
                    ?>


                    <?
                    if( $_SESSION['member_id'] == "") {
                    ?>
                    <li>
                        <i class="fa fa-user fa-user"></i>
                        <a id="login-link" href="signIn.php">登入</a> /
                        <a id="login-link" href="registered.php">註冊</a>
                    </li>
                    <?
                    }
                    else
                    {
                    ?>
                        <li>
                            <i class="fa fa-user fa-user"></i>
                            <a id="login-link" href="profile.php"><?=$_SESSION['member_name']?></a> /
                            <a id="login-link" href="./signIn.php?logout=1">登出</a>
                        </li>
                    <?
                    }
                    ?>

                </ul>
            </div>
            <div class="row">

                <div class="col-lg-12">
                    <div class="footer-search-pc">
                        <input type="text" class="form-control" placeholder="關鍵字搜尋" id="keywords" name="keyword">
                        <button type="button" value="搜尋" onclick="search_sent_check()" class="btn btn-default">搜尋</button>
                    </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-xs-12 ">
                    <!-- <p><?=$introduction_info?>系統提供 by <a href="https://www.qu106.com.tw/">皇后架站</a></p> -->
                    <ul class="footer-cont">                        
                        <li><i class="fa fa-map-marker"></i><?=$introduction_address?><span>｜</span></li>
                        <li><i class="fa fa-phone"></i>服務電話：<?=$introduction_cell?><span>｜</span></li>
                        <li><i class="fa fa-print"></i>傳　　真：<?=$introduction_fax?><span>｜</span></li>
                        <li><i class="fa fa-envelope-o"></i>Email:<?=$introduction_email?></li>
                    </ul>
                    <!--<ul class="footer-icon">
                        <li class="fi-1"><a href="#"></a></li>
                        <li class="fi-2"><a href="#"></a></li>
                        <li class="fi-3"><a href="#"></a></li>
                        <li class="fi-4"><a href="#"></a></li>
                        <li class="fi-5"><a href="#"></a></li>
                        <li class="fi-6"><a href="#"></a></li>
                    </ul>-->                    
                </div>
                

            </div>
        </div>
    </div>

    <!--googe翻譯-->
    <div id="google_translate_element" style="display:<?=$introduction_translation_off?>">
    </div>
        <script type="text/javascript">
        function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: '<?=$introduction_translation?>'}, 'google_translate_element');
        }
        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"> </script>    

    <!--嵌入facebook fb-->
    <div class="embedded-fb" style="display:<?=$introduction_data_facebookOpen?>">
        <div class="fb-page" data-href="<?=$introduction_data_facebookUrl?>" data-tabs="timeline" width="500" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
            <blockquote cite="<?=$introduction_data_facebookUrl?>" class="fb-xfbml-parse-ignore">
                <a href="<?=$introduction_data_facebookUrl?>"><?=$introduction_data_facebookName?></a>
            </blockquote>
        </div>
        <div id="fb-root"></div>
    </div>

    <ul class="contaction">
        <li class="cont-icon-6"><a href="<?=$contaction_li_cont_icon_6_link?>" id=""><img src="<?=$contaction_li_cont_icon_6_image?>"></a></li>
        <li class="cont-icon-5"><a href="<?=$contaction_li_cont_icon_5_link?>" id=""><img src="<?=$contaction_li_cont_icon_5_image?>"></a></li>
        <li class="cont-icon-4"><a href="<?=$contaction_li_cont_icon_4_link?>" id="map"><img src="<?=$contaction_li_cont_icon_4_image?>"></a></li>
        <li class="cont-icon-3"><a href="<?=$contaction_li_cont_icon_3_link?>" id="fb"><img src="<?=$contaction_li_cont_icon_3_image?>"></a></li>
        <li class="cont-icon-2"><a href="<?=$contaction_li_cont_icon_2_link?>" id="line"><img src="<?=$contaction_li_cont_icon_2_image?>"></a></li>
        <li class="cont-icon-1"><a href="" id="phone"><img src="<?=$contaction_li_cont_icon_1_image?>"><p><?=$contaction_li_cont_icon_1_link?></p></a></li>
    </ul >
    <div class="align" style="display:none"><?=$contaction_align?></div>
    <div class="visits_people" style="display:<?=$visits_people_display?>;padding:10px 0">您是第&nbsp;&nbsp;
        <span>
            <a href="http://www.cutercounter.com/" target="_blank"><img src="<?=$visits_people?>" border="0" alt="<?=$visits_name?>"></a>
        </span>&nbsp;&nbsp;位造訪本站用戶！
    </div>
</footer>

<!--totop-->
<div class="top">
        <a href="#" id="goTop"><span></span></a>
</div>


<!--首頁內容 結束-->



<script src="index2/js/jquery.min.js"></script><!-- jquery啟動 -->
<script src="index2/js/picturefill.min.js"></script><!-- 依解析度不同載入不同圖片 -->
<script src="index/js/jquery.script.js"></script><!-- 浮動icon -->
<script type="text/javascript" src="index2/js/totop.js"></script><!--totop-->
<script src="index4/js/menu-new.js"></script><!-- menu主 js -->
<script src="js/jquery_lazyload/jquery.lazyload.js"></script>
<!--<script src="index4/js/bootstrap.min.js"></script> 產品輪播 -->
<!--<script src="index4/js/flexible-bootstrap-carousel.js"></script> 產品輪播 -->
<script>
    //網頁小工具-聯絡資訊圖示
    var tel_str ="<?=$contaction_li_cont_icon_1_link?>";
    var tel_num = tel_str.replace(/[^0-9]/ig,"");
    var tel_phone = "tel:"+tel_num;
    $('#phone').attr('href',tel_phone);

    if($('.align').text() == "left"){
        $('ul.contaction').css({'left': '15px','bottom': '15px'})
    }
    if($('.align').text() == "right"){
        $('ul.contaction').css({'left': 'unset','right': '25px','bottom': '150px'});
        $('ul.contaction li a#phone p').css({'left': 'unset','right': '50px'});
    }

     //禁用右鍵
    var prohibited = <?=$prohibited?>;
    /*** 禁用右键菜单*/
    document.oncontextmenu = function () {event.returnValue = prohibited;};
    /***禁用选中功能*/
    document.onselectstart = function () {event.returnValue = prohibited;};
    /*** 禁用复制功能*/
    document.oncopy = function () {event.returnValue = prohibited;};
    /*** 禁用鼠标的左右键* @param {Object} e */
    document.onmousedown = function () {
        // if (event.which == 1) {//鼠标左键
        //     return prohibited;
        // }
        if (event.which == 3) {//鼠标右键
            return prohibited;
        }
    };
    /*** 获取键盘上的输入值*/
    document.onkeydown = function () {
        console.info(event.which);
        if (event.which == 13) {
            console.info("回车键");
        }
    };
</script>
<!-- 字數限制 -->
<script>
    $(function(){
        var len = 660; // 超過50個字以"..."取代
        $(".JQellipsis").each(function(i){
            if($(this).text().length>len){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len-1)+"...";
                $(this).text(text);
            }
        });
   });
</script><!-- 字數限制end -->

<!--焦點大圖輪播-->
<script src="index2/js/owl.carousel_B.js"></script>
<script>
    //Owl Carousel control
    $(document).ready(function() {
        $("#main-slide-photpshow").owlCarousel({
            autoPlay: true,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true
        });
        $("#sub-slide").owlCarousel({
            autoPlay: false,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: false,
            items: 3,
            itemsDesktop: [1024, 3],
            itemsDesktopSmall: [980, 5],
            itemsTablet: [768, 5],
            itemsMobile: [600, 3]
        });
    });
</script>

<script src="index/js/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            autoPlay: 3000,
            items: 4,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3]
        });

    });
</script>

<!--商品輪播-->
<!--<script src="owl-carousel-product/js/jquery-1.9.1.min.js"></script>-->
<script src="owl-carousel-product/js/owl.carousel-product.js"></script>
<script>
    $(document).ready(function () {

      $("#owl-demo-product").owlCarousel({
        autoPlay: 6000,
        items: 4,
        lazyLoad: true,
        navigation: true
      });

    });
</script>
<!-- logo輪播 -->



<!--捲軸平移-->
<script src="index2/js/jquery.smooth-scroll.js"></script>
<script>
    $(document).ready(function() {

        $('ul.mainnav a').smoothScroll();

        $('p.subnav a').click(function(event) {
            event.preventDefault();
            var link = this;
            $.smoothScroll({
                scrollTarget: link.hash
            });
        });

        $('button.scrollsomething').click(function() {
            $.smoothScroll({
                scrollElement: $('div.scrollme'),
                scrollTarget: '#findme'
            });
            return false;
        });
        $('button.scrollhorz').click(function() {
            $.smoothScroll({
                direction: 'left',
                scrollElement: $('div.scrollme'),
                scrollTarget: '.horiz'
            });
            return false;
        });

    });

</script>




<!--動畫(animations.css)-->
<script>

    $(window).scroll(function() {
        $('.animations-1').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+980) {
                $(this).addClass("slideShow");
            }
        });

        $('.animations-2').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+980) {
                $(this).addClass("slideLeft");
            }
        });

        $('.animations-3').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+980) {
                $(this).addClass("slideLeft-2");
            }
        });

        $('.animations-4').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+980) {
                $(this).addClass("slideRight-2");
            }
        });

        $('.animations-5').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+980) {
                $('.device-arrow').addClass("stretchRight");
            }
        });

        $('.animations-6').each(function(){
            var imagePos = $(this).offset().top;

            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow+980) {
                $('.graph-bar').addClass("pullUp");
            }
        });


    });

</script>

<!--彈出視窗-->
<script>

    jQuery(document).ready(function($) {

        $('.alertbox-btn-noLogin').click(function(event) {
            $('.noLogin').fadeIn();
            $('.alertbox-wa-noLogin').fadeIn();
            $('.alertbox').hide();
            //$('html, body').scrollTop(0);
        });

        $('.alertbox-s-noLogin').click(function(event) {
            $('.alertbox-wa-noLogin').fadeOut().hide;
        });

        $('.alertbox-btn-noShopping').click(function(event) {
            $("#noLogin_text").html("您的購物車沒有商品");
            $('.noLogin').fadeIn();
            $('.alertbox-wa-noLogin').fadeIn();
            $('.alertbox').hide();
            //$('html, body').scrollTop(0);
        });

        $('.alertbox-s-noLogin').click(function(event) {
            $('.alertbox-wa-noLogin').fadeOut().hide;
        });

    });

</script>

<script language="javascript">

    //電腦版搜尋用
    function search_sent_check()
    {

        if( $("#keywords").val() == '' )
        {
            alert("您尚未輸入關鍵字");
            return ;
        }

        window.location.href = 'search.php?keyWord=' + $("#keywords").val()  ;
    }

    //手機版搜尋用
    function search_sent_mobile_check()
    {

        if( $("#mobile_keywords").val() == '' )
        {
            alert("您尚未輸入關鍵字");
            return ;
        }

        window.location.href = 'search.php?keyWord=' + $("#mobile_keywords").val()  ;
    }



</script>


<script>

    var check_h3B_button = true ; //控制追蹤按鈕是否可以點擊
    var sendCondition = new Object(); //傳遞search參數
    var member_id =  <?=($_SESSION['member_id']!=''?$_SESSION['member_id']:0)?> ;

    $( document ).ready(function() {

        //$(".sp-btn").trigger("click");

        $("img.lazy").lazyload({
            effect : "fadeIn",
            threshold : 5000
        });

        $('.h3B').click(function(e){

            if( member_id == 0 ) check_h3B_button = false ;

            if( !check_h3B_button ) return ;

            check_h3B_button = false ;

            e.preventDefault();

            sendCondition.goods3_id = $(this).find('span').attr('goods3_id');
            sendCondition.member_id = member_id ;
            sendCondition.track_status = $(this).find('span').attr('track_status');

            //alert('sendCondition.track_status='+sendCondition.track_status) ;

            $.ajax(
                {
                    url: './api/api_goods_track.php',
                    data: sendCondition,
                    type: 'get',
                    context: this,
                    error: function(xhr)
                    {
                        alert('Ajax request 發生錯誤');
                    },
                    success: function(responseObject)
                    {

                        if( responseObject["status"] == 1 )
                        {
                            console.log(responseObject["goods3_id"]) ;
                            console.log(responseObject["member_id"]) ;
                            console.log(responseObject["track_status"]) ;

                            //                        for (var content_key in responseObject["contents"]) {
                            //
                            //                            var object = responseObject["contents"][content_key] ;
                            //
                            //                            console.log(object["productID"]) ;
                            //                            console.log(object["productName"]) ;
                            //                        }

                            //                        console.log("共："+ responseObject["contents"].length + "筆") ;

                        }
                        else
                        {
                            console.log(responseObject["status"]) ;
                            console.log(responseObject["reason"]) ;
                        }

                        //商品追蹤處理
                        goods_track($(this)) ;

                    }
                }) ;

        });

    });


    function goods_track(this_goods){

        if (this_goods.find('span').hasClass('checkMark')) {

            this_goods.find('span').removeClass('checkMark').addClass('cross');
            this_goods.find('span').attr('track_status','cross');

            $('.h3B-text').text('已加入追蹤').css({color:'red',});
            $('.take-btn').find('.fa').css({color:'red',});
            $('.ui-ios-overlay-cross').fadeIn(1000);
            $('.ui-ios-overlay-cross').fadeOut(500,dsomething_fun);

        } else if (this_goods.find('span').hasClass('cross')) {

            this_goods.find('span').removeClass('cross').addClass('checkMark');
            this_goods.find('span').attr('track_status','checkMark');

            $('.h3B-text').text('已取消追蹤').css({color:'#A1A1A1',});
            $('.take-btn').find('.fa').css({color:'#A1A1A1',});
            $('.ui-ios-overlay-checkMark').fadeIn(1000);
            $('.ui-ios-overlay-checkMark').fadeOut(500,dsomething_fun);
        }
    }


    function dsomething_fun(){

        check_h3B_button = true ;
    }


</script>
<!--淡入的動畫效果-->
<script src="index3/js/fadeIn-animation.js"></script>
<script>
    $(function(){
        $('.smoove').smoove({
           offset: "40%",
        })
    })
</script>
<!--淡入的動畫效果 結束-->

<!--嵌入facebook fb-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.12&appId=982254525233061&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script>
    $('title').append(' - 首頁');
</script>
<script src="<?=FILE_PATH?>/customize.js" type="text/javascript"></script><!-- 自訂義JS -->
</body>
</html>