<? include("head.php"); ?>
<? include("por_shared.php"); ?>

<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 29"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.商品大標(C)
$page_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$page_ul_productsShow_li_h2_h2B = $page_style1->color ;

//2.詳細資訊按鈕(CH)
$page_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$page_stylePar_name2_1 = 'color' ;
$page_stylePar_name2_2 = 'background' ;
$page_stylePar_name2_3 = 'border-top' ;
$page_stylePar_name2_4 = 'border-right' ;
$page_stylePar_name2_5 = 'border-bottom' ;
$page_stylePar_name2_6 = 'border-left' ;
$page_stylePar_name2_7 = 'border-radius' ;
$page_ul_productsShow_li_h5_a_color = $page_style2->$page_stylePar_name2_1 ;
$page_ul_productsShow_li_h5_a_background = $page_style2->$page_stylePar_name2_2 ;
$page_ul_productsShow_li_h5_a_border_top = $page_style2->$page_stylePar_name2_3 ; 
$page_ul_productsShow_li_h5_a_border_right = $page_style2->$page_stylePar_name2_4 ; 
$page_ul_productsShow_li_h5_a_border_bottom = $page_style2->$page_stylePar_name2_5 ; 
$page_ul_productsShow_li_h5_a_border_left = $page_style2->$page_stylePar_name2_6 ; 
$page_ul_productsShow_li_h5_a_border_radius = $page_style2->$page_stylePar_name2_7 ;
$page_stylePar_name2_8 = 'color-hover' ;
$page_stylePar_name2_9 = 'background-hover' ;
$page_stylePar_name2_10 = 'border-top-hover' ;
$page_stylePar_name2_11 = 'border-right-hover' ;
$page_stylePar_name2_12 = 'border-bottom-hover' ;
$page_stylePar_name2_13 = 'border-left-hover' ;
$page_stylePar_name2_14 = 'border-radius-hover' ;
$page_ul_productsShow_li_h5_a_hover_color = $page_style2->$page_stylePar_name2_8 ;
$page_ul_productsShow_li_h5_a_hover_background = $page_style2->$page_stylePar_name2_9 ;
$page_ul_productsShow_li_h5_a_hover_border_top = $page_style2->$page_stylePar_name2_10 ; 
$page_ul_productsShow_li_h5_a_hover_border_right = $page_style2->$page_stylePar_name2_11 ; 
$page_ul_productsShow_li_h5_a_hover_border_bottom = $page_style2->$page_stylePar_name2_12 ; 
$page_ul_productsShow_li_h5_a_hover_border_left = $page_style2->$page_stylePar_name2_13 ; 
$page_ul_productsShow_li_h5_a_hover_border_radius = $page_style2->$page_stylePar_name2_14 ;

//3.商品圖片背景(C)
$page_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$page_ul_productsShow_li_h1 = $page_style3->color ;

//4.整體線條(C)、背景(CP)
$page_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$page_stylePar_name4_1 = 'background-color' ;
$page_stylePar_name4_2 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE4"] ;
$page_stylePar_name4_3 = 'border-top' ;
$page_stylePar_name4_4 = 'border-right' ;
$page_stylePar_name4_5 = 'border-bottom' ;
$page_stylePar_name4_6 = 'border-left' ;
$page_stylePar_name4_7 = 'border-radius' ;
$page_ul_productsShow_li_wap_background_color = $page_style4->$page_stylePar_name4_1 ;
$page_ul_productsShow_li_wap_background_image = $page_stylePar_name4_2 ;
$page_ul_productsShow_li_wap_border_top = $page_style4->$page_stylePar_name4_3 ; ;
$page_ul_productsShow_li_wap_border_right = $page_style4->$page_stylePar_name4_4 ;
$page_ul_productsShow_li_wap_border_bottom = $page_style4->$page_stylePar_name4_5 ;
$page_ul_productsShow_li_wap_border_left = $page_style4->$page_stylePar_name4_6 ;
$page_ul_productsShow_li_wap_border_radius = $page_style4->$page_stylePar_name4_7 ;

//5.每一列顯示的數量(雙頁內容專用)
$page_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$page_stylePar_name5_1 = 'quantity-lg' ;
$page_stylePar_name5_2 = 'quantity-md' ;
$page_stylePar_name5_3 = 'quantity-xs' ;
$page_ul_productsShow_li_wap_quantity_lg = $page_style5->$page_stylePar_name5_1 ;$page_ul_productsShow_li_wap_quantity_md = $page_style5->$page_stylePar_name5_2 ;
$page_ul_productsShow_li_wap_quantity_xs = $page_style5->$page_stylePar_name5_3 ;

?>

<style>
    /*這裡是自訂頁面(獨立樣式)，如果這裡沒有設定預設就繼承上面的css*/
    ul#productsShow li h2 .h2B {color: <?=$page_ul_productsShow_li_h2_h2B?>;}/*1.大標(C)*/
    ul#productsShow li h5 a {color: <?=$page_ul_productsShow_li_h5_a_color?>;background: <?=$page_ul_productsShow_li_h5_a_background?>;border-top: <?=$page_ul_productsShow_li_h5_a_border_top?>;border-right: <?=$page_ul_productsShow_li_h5_a_border_right?>;border-bottom: <?=$page_ul_productsShow_li_h5_a_border_bottom?>;border-left: <?=$page_ul_productsShow_li_h5_a_border_left?>;border-radius: <?=$page_ul_productsShow_li_h5_a_border_radius?>;}/*2.詳細資訊按鈕(CH)*/
    ul#productsShow li h5 a:hover,ul#productsShow li h5 a.active{color: <?=$page_ul_productsShow_li_h5_a_hover_color?>;background: <?=$page_ul_productsShow_li_h5_a_hover_background?>;border-top: <?=$page_ul_productsShow_li_h5_a_hover_border_top?>;border-right: <?=$page_ul_productsShow_li_h5_a_hover_border_right?>;border-bottom: <?=$page_ul_productsShow_li_h5_a_hover_border_bottom?>;border-left: <?=$page_ul_productsShow_li_h5_a_hover_border_left?>;border-radius: <?=$page_ul_productsShow_li_h5_a_hover_border_radius?>;}/*2.詳細資訊BTN(CH) 滑入*/
    ul#productsShow li h1 {background: <?=$page_ul_productsShow_li_h1?>;}/*3.商品圖片背景(C)*/
    ul#productsShow li .wap {
        background-color: <?=$page_ul_productsShow_li_wap_background_color?>;
        background-image: url(<?=$page_ul_productsShow_li_wap_background_image?>);/*建議300x500以上*/
        border-top: <?=$page_ul_productsShow_li_wap_border_top?>;
        border-right: <?=$page_ul_productsShow_li_wap_border_right?>;
        border-bottom: <?=$page_ul_productsShow_li_wap_border_bottom?>;
        border-left: <?=$page_ul_productsShow_li_wap_border_left?>;
        border-radius:<?=$page_ul_productsShow_li_wap_border_radius?>;}/*4.整體線條(C)、背景(CP)*/
</style>
<body style="">


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
<!--輪播-->
<div class="slideWa">
    <ul class="slidedown-btn mainnav floating">
        <li class="scroll-1"><a href="#p1"><!--<i class="fa fa-angle-down"></i>--></a></li>
    </ul>

    <!--<div class="slideWa-txt animations-1">
        <h1 class="animations-1">響應式<br>讓您的網站像水一樣</h1>
        <h2 class="">一次解決各種螢幕解析相容問題<br>
            網站　做一個就好
        </h2>
        <h3 class="animations-2"><a href="#">了解更多</a></h3>
    </div>-->
    <picture id="main-slide-photpshow" class="owl-carousel_B">

        <? //上方圖片輪播
        $query_index_cycle  = "select * from index_cycle2 where HIDE_ID =0 and ON_LEVEL_ID = 2 order by LEVEL ASC" ;
        $result_index_cycle = mysql_query($query_index_cycle)or die(mysql_error());
        while( $record_index_cycle = mysql_fetch_array($result_index_cycle) )
        {
            $index_img_link[] = $record_index_cycle["LINK"] ;
            $index_img_image[] = $record_index_cycle["IMAGE"] ;
            $index_img_image_m[] = $record_index_cycle["IMAGE_M"] ;
            $index_img_image_s[] = $record_index_cycle["IMAGE_S"] ;

            if( $record_index_cycle["IMAGE_M"] == '' ) $index_img_image_m[] = $record_index_cycle["IMAGE"] ;
            if( $record_index_cycle["IMAGE_S"] == '' ) $index_img_image_s[] = $record_index_cycle["IMAGE"] ;

        }

        foreach( $index_img_image as $v1_no => $v2_image )
        {
        ?>
            <div class="item">
                <a href="<?=$index_img_link[$v1_no]?>" target="_blank" >
                    <picture>

                        <!--[if IE 9]><img srcset="<?=FILE_PATH?>/index_cycle2/<?=$v2_image?>" alt=""><![endif]-->

                        <source srcset="<?=FILE_PATH?>/index_cycle2/<?=$v2_image?>" media="(min-width: 768px)">
                        <source srcset="<?=FILE_PATH?>/index_cycle2/<?=$index_img_image_m[$v1_no]?>" media="(min-width: 568px)">
                        <img srcset="<?=FILE_PATH?>/index_cycle2/<?=$index_img_image_s[$v1_no]?>" alt="" class="ie9-no">

                    </picture>
                </a>
            </div>
        <?
        }
        ?>

    </picture>
</div>
    

    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <?=$together_page_name[$_GET["page1_ID"]]?></li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>




    <!-- InstanceBeginEditable name="main" -->
    <main class="cd-main-content page clearfix page-page">
        <ul id="productsShow" class="row">


            <? //產品資訊

            $page_par_page1_ID = $_GET["page1_ID"] ;
            $page_par_page1_type = $_GET["page1_type"] ;

            $query_page2  = "select * from page2 where HIDE_ID = 0 AND ON_LEVEL_ID = ".$page_par_page1_ID." order by LEVEL ASC limit 100 " ;
            $result_page2 = mysql_query($query_page2)or die(mysql_error());
            while( $record_page2 = mysql_fetch_array($result_page2) )
            {
                $page2_id = $record_page2["ID"] ;
                $page2_name = mb_substr($record_page2["NAME"],0,30,'utf8')  ; //商品名稱
                $page2_image = $record_page2["IMAGE"] ; //圖片
            ?>

                <li class="col-lg-<?=$page_ul_productsShow_li_wap_quantity_lg?> col-md-<?=$page_ul_productsShow_li_wap_quantity_md?> col-xs-<?=$page_ul_productsShow_li_wap_quantity_xs?>">
                    <div class="wap">
                        <h1 class="abgne-frame-20140107-1">
                            <span></span>
                            <a href="pageDetailed.php?page2_ID=<?=$page2_id?>&page1_ID=<?=$page_par_page1_ID?>&page1_type=<?=$page_par_page1_type?>">
                                <img src="<?=FILE_PATH?>/page2/<?=$page2_image?>" alt="<?=$page2_name?>">
                            </a>
                        </h1>
                        <h2>
                            <div class="h2B JQellipsis-2"><?=$page2_name?></div>
                        </h2>
                        <h5><a href="pageDetailed.php?page2_ID=<?=$page2_id?>&page1_ID=<?=$page_par_page1_ID?>&page1_type=<?=$page_par_page1_type?>">詳細資訊</a></h5>
                        <div class="clear"></div>
                    </div>
                </li>

            <?
            }
            ?>

            <div class="clear"></div>

        </ul>
    </main>
<!--    <div class="tab page">-->
<!--        <div class="tabw">-->
<!--            <ul class="tabNumber">-->
<!--                <li class="first btnShare"><a onclick=".c" href="#" class="fa fa-angle-double-left"></a></li>-->
<!--                <li class="pre btnShare"><a href="#" class="fa fa-caret-left"></a></li>-->
<!--                <li class="tb"><a href="#">1</a></li>-->
<!--                <li class="tb"><a href="#">2</a></li>-->
<!--                <li class="tb"><a href="#">3</a></li>-->
<!--                <li class="next btnShare"><a href="#" class="fa fa-caret-right"></a></li>-->
<!--                <li class="last btnShare"><a href="#" class="fa fa-angle-double-right"></a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--        <div class="tabGo">-->
<!--            <div class="mobile">頁次：2/8</div>-->
<!--            <button>Go</button>-->
<!--            <input type="text" name="text" id="" placeholder="">-->
<!--        </div>-->
<!---->
<!--    </div>-->
    <div class="clear"></div>
    <!-- InstanceEndEditable -->
</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>

<!-- InstanceEndEditable -->


</body>

<!-- InstanceEnd --></html>


<? include("common_js.php"); ?>
 <!-- 字數限制 -->
 <script>
    $(function(){
        var len = 39; // 超過39個字以"..."取代
        $(".JQellipsis").each(function(i){
            if($(this).text().length>len){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len-1)+"...";
                $(this).text(text);
            }
        });

        var len = 16; // 超過16個字以"..."取代
        $(".JQellipsis-2").each(function(i){
            if($(this).text().length>len){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len-1)+"...";
                $(this).text(text);
            }
        });
    });
    </script><!-- 字數限制end -->


