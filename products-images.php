<? include("head.php"); ?>
<? include("por_shared.php"); ?>

<!-- 產品獨立的css -->
<link rel="stylesheet" href="css/mainKingLeft.css"><!-- 左列菜單 -->
<link rel="stylesheet" href="css/bootstrap.min.css"><!-- 左列菜單 -->
<link rel="stylesheet" href="css/images.css"><!-- 形象頁面專用 -->

<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>

<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? /*include("right_button.php");  形象頁不需要*/?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->

    <!--輪播-->
<div class="slideWa">
    <ul class="slidedown-btn mainnav floating">
        <li class="scroll-1"><a href="#p1"><!--<i class="fa fa-angle-down"></i>--></a></li>
    </ul>

    <!--<div class="slideWa-txt animations-1">
        <h1 class="animations-1">響應式<br>讓您的網站像水一樣</h1>
        <h2 class="">一次解決各種螢幕解析相容問題<br>
            網站　做一個就好
        </h2>
        <h3 class="animations-2"><a href="#">了解更多</a></h3>
    </div>-->
    <picture id="main-slide-photpshow" class="owl-carousel_B">

        <? //上方圖片輪播
        $query_index_cycle  = "select * from index_cycle2 where HIDE_ID =0 and ON_LEVEL_ID = 2 order by LEVEL ASC" ;
        $result_index_cycle = mysql_query($query_index_cycle)or die(mysql_error());
        while( $record_index_cycle = mysql_fetch_array($result_index_cycle) )
        {
            $index_img_link[] = $record_index_cycle["LINK"] ;
            $index_img_image[] = $record_index_cycle["IMAGE"] ;
            $index_img_image_m[] = $record_index_cycle["IMAGE_M"] ;
            $index_img_image_s[] = $record_index_cycle["IMAGE_S"] ;

            if( $record_index_cycle["IMAGE_M"] == '' ) $index_img_image_m[] = $record_index_cycle["IMAGE"] ;
            if( $record_index_cycle["IMAGE_S"] == '' ) $index_img_image_s[] = $record_index_cycle["IMAGE"] ;

        }

        foreach( $index_img_image as $v1_no => $v2_image )
        {
        ?>
            <div class="item">
                <a href="<?=$index_img_link[$v1_no]?>" target="_blank" >
                    <picture>

                        <!--[if IE 9]><img srcset="<?=FILE_PATH?>/index_cycle2/<?=$v2_image?>" alt=""><![endif]-->

                        <source srcset="<?=FILE_PATH?>/index_cycle2/<?=$v2_image?>" media="(min-width: 768px)">
                        <source srcset="<?=FILE_PATH?>/index_cycle2/<?=$index_img_image_m[$v1_no]?>" media="(min-width: 568px)">
                        <img srcset="<?=FILE_PATH?>/index_cycle2/<?=$index_img_image_s[$v1_no]?>" alt="" class="ie9-no">

                    </picture>
                </a>
            </div>
        <?
        }
        ?>

    </picture>
</div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb">
                <a href="index.php" class="fa fa-home"></a> /
                <a href="products-images.php"><?=$all_page_name_array["products"]?></a> /
                <a href="products-images.php?goods1_id=<?=$_GET["goods1_id"]?>"><?=$goods1_class[$_GET["goods1_id"]]?></a></li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>

    <main class="row page">


        <? include("products_left_menu-images.php"); ?>


        <main id="proList" class="col-md-12 col-sm-12 col-xs-12">
            <!-- InstanceBeginEditable name="mainKingRight" -->

            <!--追蹤清單提示框-->
            <div class="ui-ios-overlay-cross">
                <div class="ui-ios-overlay-icon fa fa-check-circle-o"></div>
                <div class="ui-ios-overlay-txt">已加入追蹤</div>
            </div>
            <div class="ui-ios-overlay-checkMark">
                <div class="ui-ios-overlay-icon fa fa-times-circle-o"></div>
                <div class="ui-ios-overlay-txt">已取消追蹤</div>
            </div>
            <!--追蹤清單提示框 結束-->

            <main class="main-cont page-page">
            <!-- <div class="sizeWa">
                <ul class="size">
                    <li class="proTxt">呈現方式</li>
                    <li class="s"><a href="javascript:void(0);">小</a></li>
                    <li class="m"><a href="javascript:void(0);">中</a></li>
                    <li class="l"><a href="javascript:void(0);">大</a></li>
                </ul>
            </div> -->
                <ul id="productsShow" class="row">

                    <span class="productsContent">

                    <? //產品資訊

                    if( $_GET["goods2_ID"] == "" && $_GET["goods1_id"] == "" )
                    {
                        $goods3_track_status = '' ;
                        $sql_member_ser = '' ;
                        if( $_SESSION['member_id'] == "" ) $goods3_track_status = "alertbox-btn-noLogin" ;
                        else $sql_member_ser = ' and MEMBER_ID = ' . $_SESSION['member_id'] ;

                        //按照瀏覽數
                        //$query_goods3  = "select * from image_goods3 where HIDE_ID =0 and UP_STATUS = 0 order by VIEWS_COUNT DESC limit 6 " ;

                        //按照 goods1 LEVEL , goods2 LEVEL , goods3 LEVEL 順序排序
                        $new_table = "SELECT image_goods1.ID AS image_goods1_ID , image_goods1.LEVEL AS image_goods1_LEVEL ,
                                             image_goods2.ID AS image_goods2_ID , image_goods2.LEVEL AS image_goods2_LEVEL
                                        FROM image_goods2 LEFT JOIN image_goods1 ON image_goods2.GOODS1_ID = image_goods1.ID WHERE image_goods2.HIDE_ID = 0
                                         AND image_goods1.HIDE_ID = 0 ORDER BY image_goods1.LEVEL ASC , image_goods2.LEVEL ASC" ;

                        $query_goods3 = "select  new_table.image_goods1_ID , new_table.image_goods1_LEVEL ,
                                                 new_table.image_goods2_ID , new_table.image_goods2_LEVEL  ,
                                                 image_goods3.*
                                                 from image_goods3 LEFT JOIN (".$new_table.") AS new_table
                                                   ON image_goods3.GOODS2_ID = new_table.image_goods2_ID
                                                where image_goods3.HIDE_ID =0 and image_goods3.UP_STATUS = 0
                                             order by new_table.image_goods1_LEVEL ASC , new_table.image_goods2_LEVEL ASC , image_goods3.LEVEL ASC limit 6 " ;
                    }
                    else
                    {
                        //如果是從小分類點擊進來的
                        $sql_par["goods2_ID"] = $_GET["goods2_ID"] ;

                        //如果是從大分類點擊進來的
                        if( $_GET["goods2_ID"] == "" )
                        {
                            $sql_par["goods2_ID"] = '' ;
                            foreach( $goods2_class[$_GET["goods1_id"]] as $goods2_id => $goods2_class_name )
                            {
                                if( $goods3_count[$goods2_id] == 0 ) continue ;
                                $sql_par["goods2_ID"] .= ',' . $goods2_id ;
                            }
                            $sql_par["goods2_ID"] = substr($sql_par["goods2_ID"],1);
                        }

                        $goods3_track_status = '' ;
                        $sql_member_ser = '' ;

                        if( $_SESSION['member_id'] == "" ) $goods3_track_status = "alertbox-btn-noLogin" ;
                        else $sql_member_ser = ' and MEMBER_ID = ' . $_SESSION['member_id'] ;

                        $query_goods3  = "select * from image_goods3 where HIDE_ID =0 and UP_STATUS = 0 and GOODS2_ID in (".$sql_par["goods2_ID"].") order by LEVEL ASC limit 6 " ;
                    }

                    $result_goods3 = mysql_query($query_goods3)or die(mysql_error());
                    while( $record_goods3 = mysql_fetch_array($result_goods3) )
                    {
                        $goods3_id = $record_goods3["ID"] ;
                        $goods3_name = mb_substr($record_goods3["NAME"],0,30,'utf8')  ; //商品名稱
                        $goods3_image1 = $record_goods3["IMAGE1"] ; //圖片
                        $goods3_self_price = $record_goods3["SELL_PRICE"] ; //售價
                        $goods3_content = mb_substr(strip_tags($record_goods3["CONTENT"]) , 0 , 40 ,'utf8' )  ;
                        $goods3_views_count = $record_goods3["VIEWS_COUNT"] ;
                        $goods3_brief = mb_substr(strip_tags($record_goods3["BRIEF"]) , 0 , 40 ,'utf8' ) ;
                    ?>

                        <li class="col-lg-<?=$products_ul_productsShow_li_wap_quantity_lg?> col-md-<?=$products_ul_productsShow_li_wap_quantity_md?> col-xs-<?=$products_ul_productsShow_li_wap_quantity_xs?>">
                            <div class="wap">
                                <h1 class="abgne-frame-20140107-1">
                                    <span></span>
                                    <a href="productsDetailed-images.php?request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>">
                                        <!--<img class="lazy"  data-original="<?=FILE_PATH?>/image_goods3/<?=$goods3_image1?>" >-->
                                        <img src="<?=FILE_PATH?>/image_goods3/<?=$goods3_image1?>" >
                                    </a>
                                </h1>
                                <h2>
                                    <div class="h2B JQellipsis-2"><?=$goods3_name?></div>
                                    <div class="h2S JQellipsis"><?=( $goods3_brief != "" ? $goods3_brief : $goods3_content )?></div>
                                </h2>
                                <h5><a href="productsDetailed-images.php?request_url=<?=$request_url?>&goods3_ID=<?=$goods3_id?>">詳細資訊</a></h5>
                                <div class="clear"></div>
                            </div>
                        </li>

                    <?
                    }
                    ?>

                    </span>

                    <div class="clear"></div>
                </ul>
            </main>

            <div class="clear"></div>

            <!-- InstanceEndEditable -->
        </main>
        <div class="clear"></div>
    </main>


</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>


<!-- InstanceBeginEditable name="layout2 script" -->

<!-- InstanceEndEditable -->



</body>

<!-- InstanceEnd --></html>

<? include("common_js.php"); ?>

<!--左列菜單-->
<? include("left_menu_js.php"); ?>


<script>

    var check_h3B_button = true ; //控制追蹤按鈕是否可以點擊
    var sendCondition = new Object(); //傳遞search參數
    var member_id =  <?=($_SESSION['member_id']!=''?$_SESSION['member_id']:0)?> ;

    $( document ).ready(function() {

        //$(".sp-btn").trigger("click");

        $("img.lazy").lazyload({
            effect : "fadeIn"
        });


        $(document).on("click", ".h3B", function(e) {

            if( member_id == 0 ) check_h3B_button = false ;

            if( !check_h3B_button ) return ;

            check_h3B_button = false ;

            e.preventDefault();

            sendCondition.goods3_id = $(this).find('span').attr('goods3_id');
            sendCondition.member_id = member_id ;
            sendCondition.track_status = $(this).find('span').attr('track_status');

            //alert('sendCondition.track_status='+sendCondition.track_status) ;

            $.ajax(
                {
                    url: './api/api_goods_track.php',
                    data: sendCondition,
                    type: 'get',
                    context: this,
                    error: function(xhr)
                    {
                        alert('Ajax request 發生錯誤');
                    },
                    success: function(responseObject)
                    {

                        if( responseObject["status"] == 1 )
                        {
                            console.log(responseObject["goods3_id"]) ;
                            console.log(responseObject["member_id"]) ;
                            console.log(responseObject["track_status"]) ;

//                        for (var content_key in responseObject["contents"]) {
//
//                            var object = responseObject["contents"][content_key] ;
//
//                            console.log(object["productID"]) ;
//                            console.log(object["productName"]) ;
//                        }

//                        console.log("共："+ responseObject["contents"].length + "筆") ;

                        }
                        else
                        {
                            console.log(responseObject["status"]) ;
                            console.log(responseObject["reason"]) ;
                        }

                        //商品追蹤處理
                        goods_track($(this)) ;

                    }
                }) ;

        });

    });


    function goods_track(this_goods){

        if (this_goods.find('span').hasClass('checkMark')) {

            this_goods.find('span').removeClass('checkMark').addClass('cross');
            this_goods.find('span').attr('track_status','cross');

            $('.h3B-text').text('已加入追蹤').css({color:'red',});
            $('.take-btn').find('.fa').css({color:'red',});
            $('.ui-ios-overlay-cross').fadeIn(1000);
            $('.ui-ios-overlay-cross').fadeOut(500,dsomething_fun);

        } else if (this_goods.find('span').hasClass('cross')) {

            this_goods.find('span').removeClass('cross').addClass('checkMark');
            this_goods.find('span').attr('track_status','checkMark');

            $('.h3B-text').text('已取消追蹤').css({color:'#A1A1A1',});
            $('.take-btn').find('.fa').css({color:'#A1A1A1',});
            $('.ui-ios-overlay-checkMark').fadeIn(1000);
            $('.ui-ios-overlay-checkMark').fadeOut(500,dsomething_fun);
        }
    }


    function dsomething_fun(){

        check_h3B_button = true ;
    }

    var is_check_response = true ; //確認抓取最新消息的資料是否回應
    var start_index = 6; //第幾筆開始
    var each_count = 3 ; //每次抓取的數量
    var is_end = false ; //是否已經最後一筆了
    var sendProductsCondition = new Object(); //傳遞Products參數

    function getMessageInfo()
    {
        is_check_response = false ;

        sendProductsCondition.start_index = start_index;
        sendProductsCondition.count = each_count;
        sendProductsCondition.member_id = member_id;
        sendProductsCondition.goods1_id = <?=($_GET["goods1_id"]!=''?$_GET["goods1_id"]:0)?>;
        sendProductsCondition.goods2_ID = <?=($_GET["goods2_ID"]!=''?$_GET["goods2_ID"]:0)?>;


        $.ajax({
            url: "./api/api_image_products_list.php",
            data: sendProductsCondition,
            dataType: "json",
            error: function(xhr)
            {
                alert('Ajax request 發生錯誤');
            },
            success: function(responseObject)
            {
                //console.log("response="+response.message_list) ;

                for (var content_key in responseObject["contents"]) {

                    var object = responseObject["contents"][content_key] ;

                    console.log("goods3_id="+object["goods3_id"]+" goods3_name="+object["goods3_name"]+" goods3_content="+object["goods3_content"]) ;

                    products_template(object) ;

                }

                $("img.lazy").lazyload({
                    effect : "fadeIn",
                    threshold : 5000
                });

                $(window).trigger('scroll');


                start_index = responseObject['count'] + start_index  ;

                if( responseObject['count'] < each_count  ) is_end = true ; //如果取得的數量小於要娶的數量代表已經最後一次了

                console.log("共："+ responseObject["contents"].length + "筆") ;
                is_check_response = true ;

            }

        }) ;
    }

    $(".s").click(function() {
    $("#productsShow li").attr('class', 'col-lg-3 col-md-4 col-sm-6 col-xs-6');
  
  
    });
    $(".m").click(function() {
    $("#productsShow li").attr('class', 'col-lg-4 col-md-4 col-sm-6 col-xs-6');
    
    });
    $(".l").click(function() {
    $("#productsShow li").attr('class', 'col-lg-6 col-md-4 col-sm-6 col-xs-6');

    });

    function products_template(products_object){

        var products_template_html =
        '<li class="col-lg-<?=$products_ul_productsShow_li_wap_quantity_lg?> col-md-<?=$products_ul_productsShow_li_wap_quantity_md?> col-xs-<?=$products_ul_productsShow_li_wap_quantity_xs?> new-product" style="display:none">'+
            '<div class="wap">'+
                '<h1 class="abgne-frame-20140107-1">'+
                    '<span></span>'+
                    '<a href="productsDetailed-images.php?request_url=<?=$request_url?>&goods3_ID='+products_object['goods3_id']+'">'+
                        '<img src="<?=FILE_PATH?>/image_goods3/'+products_object['goods3_image1']+'" >'+
                    '</a>'+
                '</h1>'+
            '<h2>'+
                '<div class="h2B JQellipsis-2">'+products_object['goods3_name']+'</div>'+
                '<div class="h2S JQellipsis">'+( products_object['goods3_brief'] != "" ? products_object['goods3_brief'] : products_object['goods3_content'] )+'</div>'+
            '</h2>'+
            '<h5><a href="productsDetailed-images.php?request_url=<?=$request_url?>&goods3_ID='+products_object['goods3_id']+'">詳細資訊</a></h5>'+
            '<div class="clear"></div>'+
            '</div>'+
        '</li>';

        $('.productsContent').append(products_template_html) ;

        $('.new-product').fadeIn(1000);

        var len = 39; // 超過39個字以"..."取代
        $(".JQellipsis").each(function(i){
            if($(this).text().length>len){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len-1)+"...";
                $(this).text(text);
            }
        });

        var len = 16; // 超過16個字以"..."取代
        $(".JQellipsis-2").each(function(i){
            if($(this).text().length>len){
                $(this).attr("title",$(this).text());
                var text=$(this).text().substring(0,len-1)+"...";
                $(this).text(text);
            }
        });


    }


    $( document ).ready(function() {

        //當網頁滑動到底下時 要再去抓資料
        $(window).scroll(function () {

            console.log('window.scrollTop='+$(window).scrollTop()+' document.height='+$(document).height()+' window.height='+$(window).height() );

            if ($(window).scrollTop() >= ( $(document).height() - $(window).height() - 100 ) || $(window).scrollTop() >= ( $(window).height() -50 ))
            //if ($(window).scrollTop() >= ( $(window).height() -50 ) )
            {
                //console.log('滑到最下面囉');

                if( !is_check_response ) return ;

                if( !is_end )
                {
                    getMessageInfo() ;
                }
            }

        });

    });



    jQuery(document).ready(function($) {

    /*當左邊或右邊選單隱藏、上面選單出現時，商品內容滿版*/
    if($('.main-top-wa').css("display") == "block"){
        $("#proList").attr("class","col-md-12 col-sm-12 col-xs-12");
    }
});




</script>


<!--<script src="index2/js/jquery.min.js"></script> jquery啟動 -->
<!-- <script src="index2/js/picturefill.min.js"></script>依解析度不同載入不同圖片 -->
<!-- <script src="index/js/jquery.script.js"></script>浮動icon -->
<!--<script type="text/javascript" src="index2/js/totop.js"></script>totop-->
<!--<script src="index2/js/menu-new.js"></script> menu主 js -->
<!-- <script src="js/jquery_lazyload/jquery.lazyload.js"></script> -->
<!--焦點大圖輪播-->
<!-- <script src="index2/js/owl.carousel_B.js"></script> -->
<!-- <script> 
    //Owl Carousel control
    $(document).ready(function() {
        $("#main-slide-photpshow").owlCarousel({
            autoPlay: true,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true
        });
        $("#sub-slide").owlCarousel({
            autoPlay: false,
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: false,
            items: 3,
            itemsDesktop: [1024, 3],
            itemsDesktopSmall: [980, 5],
            itemsTablet: [768, 5],
            itemsMobile: [600, 3]
        });
    });
</script>-->
