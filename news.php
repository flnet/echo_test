<? include("head.php"); ?>
<? include("king-color.php"); ?>
<style>
ul.news li a:hover {color: <?=$king_color?> <?=$king_color_important?>;}/*1.內容文字(CH) 滑入*/
</style>

<?
/*==== 取得樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 1"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

$news_style1 = json_decode($record_design_style2["STYLE1"]) ;
$news_stylePar_name1_1 = 'color' ;
$news_ul_news_li_a_color = $news_style1->$news_stylePar_name1_1 ;

$news_style2 = json_decode($record_design_style2["STYLE2"]) ;
$news_stylePar_name2_1 = 'color' ;
$news_stylePar_name2_1_1 = 'color-important' ;
$news_ul_news_li_a_hover_color = $news_style2->$news_stylePar_name2_1 ;
$news_ul_news_li_a_hover_color_important = $news_style2->$news_stylePar_name2_1_1 ;

$news_style3 = json_decode($record_design_style2["STYLE3"]) ;
$news_stylePar_name3_1 = 'color' ;
$news_stylePar_name3_2 = 'border-bottom' ;
$news_stylePar_name3_3 = 'background-color' ;
$news_ul_news_li_color = $news_style3->$news_stylePar_name3_1 ;
$news_ul_news_li_border_bottom = $news_style3->$news_stylePar_name3_2 ;
$news_ul_news_li_background_color = $news_style3->$news_stylePar_name3_3 ;
/*==== 取得樣式 End====*/
?>

<style>
    ul.news li {color: <?=$news_ul_news_li_color?>;
                border-bottom:<?=$news_ul_news_li_border_bottom?>;
                background-color:<?=$news_ul_news_li_background_color?> ;}/*2.背景、文字、線條(C)*/

    ul.news li a {color: <?=$news_ul_news_li_a_color?>;}/*1.內容文字(CH)*/

    ul.news li a:hover {color: <?=$news_ul_news_li_a_hover_color?> <?=$news_ul_news_li_a_hover_color_important?>;}/*1.內容文字(CH) 滑入*/
</style>

    <body style="">

        <div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
        <!-- InstanceBeginEditable name="alert" -->

        <!-- InstanceEndEditable -->

        <div id="gotop"></div>

        <!-- Navbar -->
        <header class="">

            <? include("top_menu.php"); ?>

        </header>

        <? include("right_button.php"); ?>


        <!--內容-->
        <div id="wrapper" style="">

            <!-- InstanceBeginEditable name="titleImg" -->
            <div class="titleImg">
                <? include("pageTitleImg.php"); ?>
            </div>  

            <!-- InstanceEndEditable -->
            <nav class="cd-navtb">
                <ul class="page-pad">
                    <!-- InstanceBeginEditable name="breadcrumb" -->
                    <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <?=$all_page_name_array["news"]?></li>
                    <!-- InstanceEndEditable -->
                </ul>
            </nav>

            <!-- InstanceBeginEditable name="main" -->
            <main class="cd-main-content page clearfix">
                <ul class="news">

                    <?
                    $list_no = 0 ;
                    $query = "select * , DATE_FORMAT(EDIT_TIME,'%Y-%m-%d') AS deit_FORMAT_time
                                from news where HIDE_ID = 0
                                order by EDIT_TIME DESC limit 15" ;

                    $result = mysql_query( $query ) or die( mysql_error() ) ;
                    $num_total = mysql_num_rows( $result ) ;

                    while ( $record = mysql_fetch_array( $result ) )
                    {
                        $list_no ++ ;
                        $id = $record["ID"] ;
                        $title = mb_substr( strip_tags( $record["TITLE"] ),0,30,'utf8') ;
                        $edit_time = $record["deit_FORMAT_time"] ;
                    ?>

                        <li class="row">
                            <span class="col-lg-2 col-md-2 col-xs-12"><?=$edit_time?>
                                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right"></i></span>
                            </span>
                            <span class="col-lg-10 col-md-10 col-xs-12">
                                <a href="newsDetailed.php?ID=<?=$id?>"><?=$title?></a>
                            </span>
                        </li>

                    <?
                    }
                    ?>

                </ul>
            </main>

            <div class="clear"></div>
            <!-- InstanceEndEditable -->
        </div><!--wrap結束-->
        <!--內容結束-->


        <? include("footer.php"); ?>

        <!--totop-->
        <div class="top">
            <a href="#" id="goTop"><span></span></a>
        </div>


        <!-- InstanceEndEditable -->


    </body>

    <!-- InstanceEnd -->

</html>

<? include("common_js.php"); ?>

    <script>

    var is_check_response = true ; //確認抓取最新消息的資料是否回應
    var start_index = 15; //第幾筆開始
    var each_count = 10 ; //每次抓取的數量
    var is_end = false ; //是否已經最後一筆了

    function getMessageInfo()
    {
        is_check_response = false ;

        $.ajax({
            url: "./api/api_news_list.php",
            data: {
                start_index:start_index ,
                count:each_count
            },
            dataType: "json",
            error: function(xhr)
            {
                alert('Ajax request 發生錯誤');
            },
            success: function(responseObject)
            {
                //console.log("response="+response.message_list) ;

                for (var content_key in responseObject["contents"]) {

                    var object = responseObject["contents"][content_key] ;

                    console.log("id="+object["id"]+" title="+object["title"]+" edit_time="+object["edit_time"]) ;

                    news_template(object) ;

                }

                start_index = responseObject['count'] + start_index  ;

                if( responseObject['count'] < each_count  ) is_end = true ; //如果取得的數量小於要娶的數量代表已經最後一次了

                console.log("共："+ responseObject["contents"].length + "筆") ;
                is_check_response = true ;

            }

        }) ;
    }


    function news_template(news_object){

        var news_template_html = '<li class="row">'+
                                     '<span class="col-lg-2 col-md-2 col-xs-12">'+news_object["edit_time"]+
                                         '<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right"></i></span>'+
                                     '</span>'+
                                     '<span class="col-lg-10 col-md-10 col-xs-12">'+
                                         '<a href="newsDetailed.php?ID='+news_object["id"]+'">'+news_object["title"]+'</a>'+
                                     '</span>'+
                                 '</li>';
        $('.news').append(news_template_html);
    }

    $( document ).ready(function() {

        //當網頁滑動到底下時 要再去抓資料
        $(window).scroll(function () {

            console.log('window.scrollTop='+$(window).scrollTop()+' document.height='+$(document).height()+' window.height='+$(window).height() );

            if ($(window).scrollTop() >= ( $(document).height() - $(window).height() - 100 ) )
            {
                console.log('滑到最下面囉');

                if( !is_check_response ) return ;

                if( !is_end )
                {
                    getMessageInfo() ;
                }
            }

        });

    });


    </script>

