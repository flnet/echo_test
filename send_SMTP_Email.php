<?
//驗證eamil資訊
function verificationSMTPinfo($par_send_mail,$par_recipient_name,$par_sender_name,$par_from_mail,$par_subject,$par_msg,$par_smtp_EmailUsername,$par_smtp_EmailPassword)
{
    if( !filter_var($par_send_mail, FILTER_VALIDATE_EMAIL) )
    {
        return false;
    }

    if( !filter_var($par_smtp_EmailUsername, FILTER_VALIDATE_EMAIL) )
    {
        return false;
    }

    if( !filter_var($par_from_mail, FILTER_VALIDATE_EMAIL) )
    {
        return false;
    }

    if( $par_recipient_name == "" || $par_sender_name == "" || $par_subject == "" || $par_msg == "" || $par_smtp_EmailPassword == "" )
    {
        return false;
    }

    return true;
}

//寄送email
function sendEmailMsg($par_send_mail,$par_recipient_name,$par_sender_name,$par_from_mail,$par_subject,$par_msg,$par_smtp_EmailUsername,$par_smtp_EmailPassword) {

    $mail = new PHPMailer(); // 產生 Mailer 實體
    $mail->IsSMTP();// 設定為 SMTP 方式寄信

    // SMTP 伺服器的設定，以及驗證資訊
    $mail->SMTPAuth = true;
    $mail->Host = "mail.qu106.com"; //此處請填寫您的郵件伺服器位置,通常是mail.網址。如果您MX指到外地，那這邊填入www.XXX.com 即可
    $mail->Port = 25; //ServerZoo主機的郵件伺服器port為 25

    $mail->CharSet = "utf-8";// 信件內容的編碼方式
    $mail->Encoding = "base64";// 信件處理的編碼方式

    // SMTP 驗證的使用者資訊
    $mail->Username = $par_smtp_EmailUsername;  // 此處為驗証電子郵件帳號,就是您在ServerZoo主機上新增的電子郵件帳號，＠後面請務必一定要打。
    $mail->Password = $par_smtp_EmailPassword;  //此處為上方電子郵件帳號的密碼 (一定要正確不然會無法寄出)

    // 信件內容設定
    $mail->From = $par_from_mail ; //此處為寄出後收件者顯示寄件者的電子郵件 (請設成與上方驗証電子郵件一樣的位址)
    $mail->FromName = $par_sender_name; //此處為寄出後收件者顯示寄件者的名稱
    $mail->Subject = $par_subject ; //此處為寄出後收件者顯示寄件者的電子郵件標題
    $mail->Body = $par_msg;   //信件內容
    $mail->IsHTML(true);

    // 收件人
    $mail->AddAddress($par_send_mail, $par_recipient_name); //此處為收件者的電子信箱及顯示名稱
    $mail->Send() ;
}

?>