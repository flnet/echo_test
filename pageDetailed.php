<? include("head.php"); ?>

<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 16"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.大標(C)
$newsDetailed_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$newsDetailed_stylePar_name1 = 'color' ;
$newsDetailed_stylePar_name2 = 'border-top' ;
$newsDetailed_stylePar_name3 = 'border-right' ;
$newsDetailed_stylePar_name4 = 'border-bottom' ;
$newsDetailed_stylePar_name5 = 'border-left' ;
$newsDetailed_contentTitle_color = $newsDetailed_style1->$newsDetailed_stylePar_name1 ;
$newsDetailed_contentTitle_border_top = $newsDetailed_style1->$newsDetailed_stylePar_name2 ;
$newsDetailed_contentTitle_border_right = $newsDetailed_style1->$newsDetailed_stylePar_name3 ;
$newsDetailed_contentTitle_border_bottom = $newsDetailed_style1->$newsDetailed_stylePar_name4 ;
$newsDetailed_contentTitle_border_left = $newsDetailed_style1->$newsDetailed_stylePar_name5 ;
//2.back-btn(PH) 150x50
$newsDetailed_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$newsDetailed_newsback_a = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;
//3.back-btn(PH) 150x50 HOVER
$newsDetailed_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$newsDetailed_newsback_a_hover = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE3"] ;

?>

<style>
    .contentTitle {color: <?=$newsDetailed_contentTitle_color?>;border-top:<?=$newsDetailed_contentTitle_border_top?>;border-right:<?=$newsDetailed_contentTitle_border_right?>;border-bottom:<?=$newsDetailed_contentTitle_border_bottom?>;border-left:<?=$newsDetailed_contentTitle_border_left?>;}/*1.抬頭文字、線條(C)*/
    .newsback a{background-image: url(<?=$newsDetailed_newsback_a?>);}/*2.back-btn(PH) 150x50*/
    .newsback a:hover{background-image: url(<?=$newsDetailed_newsback_a_hover?>);}/*3.back-btn(PH) 150x50 HOVER*/
</style>

<body style="">


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
<!--輪播-->
<div class="slideWa">
    <ul class="slidedown-btn mainnav floating">
        <li class="scroll-1"><a href="#p1"><!--<i class="fa fa-angle-down"></i>--></a></li>
    </ul>

    <!--<div class="slideWa-txt animations-1">
        <h1 class="animations-1">響應式<br>讓您的網站像水一樣</h1>
        <h2 class="">一次解決各種螢幕解析相容問題<br>
            網站　做一個就好
        </h2>
        <h3 class="animations-2"><a href="#">了解更多</a></h3>
    </div>-->
    <picture id="main-slide-photpshow" class="owl-carousel_B">

        <? //上方圖片輪播
        $db_table_name = 'in_page_cycle';
        if($_GET['page1_ID']==23) $db_table_name = 'in_bespoke_cycle';
        if($_GET['page1_ID']==33) $db_table_name = 'in_rent_cycle';


        $query_index_cycle  = "select * from ".$db_table_name." where HIDE_ID =0  order by LEVEL ASC" ;
        $result_index_cycle = mysql_query($query_index_cycle)or die(mysql_error());
        while( $record_index_cycle = mysql_fetch_array($result_index_cycle) )
        {
            $index_img_link[] = $record_index_cycle["LINK"] ;
            $index_img_image[] = $record_index_cycle["IMAGE"] ;
            $index_img_image_m[] = $record_index_cycle["IMAGE_M"] ;
            $index_img_image_s[] = $record_index_cycle["IMAGE_S"] ;

            if( $record_index_cycle["IMAGE_M"] == '' ) $index_img_image_m[] = $record_index_cycle["IMAGE"] ;
            if( $record_index_cycle["IMAGE_S"] == '' ) $index_img_image_s[] = $record_index_cycle["IMAGE"] ;

        }


        foreach( $index_img_image as $v1_no => $v2_image )
        {
        ?>
            <div class="item">
                <a href="<?=$index_img_link[$v1_no]?>" target="_blank" >
                    <picture>

                        <!--[if IE 9]><img srcset="<?=FILE_PATH?>/<?=$db_table_name?>/<?=$v2_image?>" alt=""><![endif]-->

                        <source srcset="<?=FILE_PATH?>/<?=$db_table_name?>/<?=$v2_image?>" media="(min-width: 768px)">
                        <source srcset="<?=FILE_PATH?>/<?=$db_table_name?>/<?=$index_img_image_m[$v1_no]?>" media="(min-width: 568px)">
                        <img srcset="<?=FILE_PATH?>/<?=$db_table_name?>/<?=$index_img_image_s[$v1_no]?>" alt="" class="ie9-no">

                    </picture>
                </a>
            </div>
            <?
        }
        ?>

    </picture>
</div>

    <?

    $pageDetailed_par_page1_ID = $_GET["page1_ID"] ;
    $pageDetailed_par_page1_type = $_GET["page1_type"] ;
    $pageDetailed_par_page2_ID = $_GET["page2_ID"] ;

    $back_url = '' ;

    if( $pageDetailed_par_page1_type == 3 )
    {
        $query_page1  = "select * from page1 where HIDE_ID = 0 and ID=".$pageDetailed_par_page1_ID ;
        $result_page1 = mysql_query($query_page1)or die(mysql_error());
        while( $record_page1 = mysql_fetch_array($result_page1) )
        {
            $page_name = $record_page1["NAME"] ;
            $page_name_english = $record_page1["NAME_ENGLISH"] ;
            $page_content = $record_page1["CONTENT"] ;
        }
        $back_url = 'index.php' ;
    }

    if( $pageDetailed_par_page2_ID >= 1 && $pageDetailed_par_page1_type == 2 )
    {
        $query_page2  = "select * from page2 where HIDE_ID = 0 and ID = ".$pageDetailed_par_page2_ID ;
        $result_page2 = mysql_query($query_page2)or die(mysql_error());
        while( $record_page2 = mysql_fetch_array($result_page2) )
        {
            $page_name = $record_page2["NAME"] ;
            $page_content = $record_page2["CONTENT"] ;
        }
        $back_url = 'page.php?page1_ID='.$pageDetailed_par_page1_ID.'&page1_type='.$pageDetailed_par_page1_type ;
    }

    ?>


    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> /

            <?
            if( $pageDetailed_par_page1_type == 2 )
            {
            ?>
                <a href="<?=$back_url?>"><?=$together_page_name[$_GET["page1_ID"]]?></a> / <?=$page_name?></li>
            <?
            }
            ?>

            <?
            if( $pageDetailed_par_page1_type == 3 )
            {
            ?>
                <?=$page_name?></li>
            <?
            }
            ?>


            <!-- InstanceEndEditable -->
        </ul>
    </nav>
    <!-- InstanceBeginEditable name="main" -->
    <main class="cd-main-content page clearfix">
        <div class="contentTitle"><?=$page_name?></div>
        <div class="newsContent">
            <div class="content">
                <?=$page_content?>
            </div>

        </div>

        <div class="newsback">

            <a href="<?=$back_url?>"></a>
        </div>

    </main>
    <!-- InstanceEndEditable -->
</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>


<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>

</body>

<!-- InstanceEnd --></html>

<? include("common_js.php"); ?>

<!--<script src="index2/js/jquery.min.js"></script> jquery啟動 -->
<!--<script src="index2/js/picturefill.min.js"></script> 依解析度不同載入不同圖片 -->
<!-- <script src="index/js/jquery.script.js"></script>浮動icon -->
<!--<script type="text/javascript" src="index2/js/totop.js"></script>totop-->
<!--<script src="index2/js/menu-new.js"></script> menu主 js -->
<!-- <script src="js/jquery_lazyload/jquery.lazyload.js"></script> -->
<!--焦點大圖輪播-->
<!-- <script src="index2/js/owl.carousel_B.js"></script> -->
<!-- <script> 
    //Owl Carousel control
    //$(document).ready(function() {
        //$("#main-slide-photpshow").owlCarousel({
            //autoPlay: true,
            //navigation: true,
            //slideSpeed: 300,
            //paginationSpeed: 400,
            //singleItem: true
        //});
        //$("#sub-slide").owlCarousel({
            //autoPlay: false,
            //navigation: true,
            //slideSpeed: 300,
            //paginationSpeed: 400,
            //singleItem: false,
            //items: 3,
            //itemsDesktop: [1024, 3],
            //itemsDesktopSmall: [980, 5],
            //itemsTablet: [768, 5],
            //itemsMobile: [600, 3]
        //});
    //});
//</script>-->