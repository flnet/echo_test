<? include("head.php"); ?>

<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 16"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.大標(C)
$newsDetailed_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$newsDetailed_stylePar_name1 = 'color' ;
$newsDetailed_stylePar_name2 = 'border-top' ;
$newsDetailed_stylePar_name3 = 'border-right' ;
$newsDetailed_stylePar_name4 = 'border-bottom' ;
$newsDetailed_stylePar_name5 = 'border-left' ;
$newsDetailed_contentTitle_color = $newsDetailed_style1->$newsDetailed_stylePar_name1 ;
$newsDetailed_contentTitle_border_top = $newsDetailed_style1->$newsDetailed_stylePar_name2 ;
$newsDetailed_contentTitle_border_right = $newsDetailed_style1->$newsDetailed_stylePar_name3 ;
$newsDetailed_contentTitle_border_bottom = $newsDetailed_style1->$newsDetailed_stylePar_name4 ;
$newsDetailed_contentTitle_border_left = $newsDetailed_style1->$newsDetailed_stylePar_name5 ;
//2.back-btn(PH) 150x50
$newsDetailed_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$newsDetailed_newsback_a = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;
//3.back-btn(PH) 150x50 HOVER
$newsDetailed_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$newsDetailed_newsback_a_hover = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE3"] ;

?>

<style>
    .contentTitle {color: <?=$newsDetailed_contentTitle_color?>;border-top:<?=$newsDetailed_contentTitle_border_top?>;border-right:<?=$newsDetailed_contentTitle_border_right?>;border-bottom:<?=$newsDetailed_contentTitle_border_bottom?>;border-left:<?=$newsDetailed_contentTitle_border_left?>;}/*1.抬頭文字、線條(C)*/
    .newsback a{background-image: url(<?=$newsDetailed_newsback_a?>);}/*2.back-btn(PH) 150x50*/
    .newsback a:hover{background-image: url(<?=$newsDetailed_newsback_a_hover?>);}/*3.back-btn(PH) 150x50 HOVER*/
</style>
<body style="">


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>

<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / 最新消息</li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>

    <?
        $news_item = $_GET["ID"] ;
        $query = "select * from news where HIDE_ID = 0 and ID = " . $news_item ;
        $result = mysql_query( $query ) or die( mysql_error() ) ;
        $record = mysql_fetch_array( $result ) ;
        $title = $record["TITLE"] ;
        if( $record["CONTENT"] == "" ) $content = "暫無發表內容"."<br/><br/><br/><br/><br/><br/>" ;
        else $content = $record["CONTENT"] ;
    ?>



    <!-- InstanceBeginEditable name="main" -->
    <main class="cd-main-content page clearfix">
        <div class="contentTitle"><?=$title?></div>
        <div class="newsContent">
            <div class="content">
                <!--<img src="images/2.jpg" width="540" height="540"></b>-->
                <?=$content?>
            </div>

        </div>
        <div class="newsback">
        <a href="news.php"></a>
            <!--<a href="news.php"><i class="fa fa-mail-reply"></i>&nbsp;&nbsp;&nbsp;back</a>-->
        </div>
    </main>
    <!-- InstanceEndEditable -->
</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>

</body>

<!-- InstanceEnd --></html>

<? include("common_js.php"); ?>
