<? include("head.php"); ?>
<? include("table.php"); ?>

<?

/*-----------------------
修改會員資料
-------------------------*/
if( $_POST["member_edit"] == 1 )
{
    if( $_POST['name'] != "" ) $data['NAME'] = "'".$_POST['name']."'"  ;
    if( $_POST['cell'] != "" ) $data['PHONE'] = "'".$_POST['cell']."'"  ;
    if( $_POST['city'] != "" && $_POST['city'] != '0'  ) $data['CITY'] = "'".$_POST['city']."'"  ;
    if( $_POST['area'] != "" && $_POST['area'] != '0' ) $data['AREA'] = "'".$_POST['area']."'"  ;
    if( $_POST['road'] != "" ) $data['ROAD'] = "'".$_POST['road']."'"  ;

    foreach( $data as $k => $v )
    {
        $header .= "," . $k . " = " . $v ;
    }
    $query = "update member set " . substr($header,1) . " where ID = '" . $_SESSION['member_id'] . "'";
    $record = mysql_query( $query ) or die( mysql_error() ) ;

    if( $record )
    {
        if( $_POST['name'] != "" ) $_SESSION['member_name']=$_POST['name'];
        if( $_POST['cell'] != "" ) $_SESSION['member_phone']=$_POST["cell"];
        if( $_POST['city'] != "" && $_POST['city'] != '0' ) $_SESSION['member_city']=$_POST["city"];
        if( $_POST['area'] != "" && $_POST['area'] != '0' ) $_SESSION['member_area']=$_POST["area"];
        if( $_POST['road'] != "" ) $_SESSION['member_road']=$_POST["road"];

        header("Location:profile.php?edit_success=1");
        exit;
    }
    else
    {
        header("Location:profile.php?edit_error=1");
        exit;
    }
}


if( $_POST["password_edit"] == 1 )
{
    //比對密碼是否正確
    if( $_SESSION['member_password'] != $_POST['old_password'] )
    {
        header("Location:profile.php?password_edit_error=1");
        exit;
    }

    $data['PASSWORD'] = "'".$_POST['new_password']."'"  ;

    foreach( $data as $k => $v )
    {
        $header .= "," . $k . " = " . $v ;
    }
    $query = "update member set " . substr($header,1) . " where ID = '" . $_SESSION['member_id'] . "'";
    $record = mysql_query( $query ) or die( mysql_error() ) ;

    if( $record )
    {
        $_SESSION['member_password'] = $_POST['new_password'] ;
        header("Location:profile.php?password_edit_success=1");
        exit;
    }
    else
    {
        header("Location:profile.php?password_edit_error=2");
        exit;
    }
}




?>



<body style="">

<!-- 產品獨立的css -->
<link rel="stylesheet" href="css/mainKingLeft.css"><!-- 左列菜單 -->
<link rel="stylesheet" href="css/bootstrap.min.css"><!-- 左列菜單 -->

<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->
<div class="alertbox-wa-2">
    <div class="alertbox-ok">
        <div class="alertbox-txt">
            <i class="fa fa-check"></i>
            <p>修改成功</p>
        </div>
        <a href="#" class="alertbox-s-2 check">確定</a>

        <!--<div class="alertbox-txt">
            <i class="fa fa-times"></i>
            <p>操作失敗，麻煩重新操作一次</p>
        </div>
        <a href="#" class="alertbox-s-2 times">確定</a>-->
    </div>
</div>
<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <a href="profile.php"><?=$all_page_name_array["member_center"]?></a> / 修改會員資料</li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>

    <main class="row page">


        <? include("member_menu.php"); ?>

        <main id="proList" class="col-md-9 col-sm-8 col-xs-12">
            <!-- InstanceBeginEditable name="mainKingRight" -->
            <main class="main-cont">

                <div>

                    <table border="0" class="table">

                        <form class="cd-form" id="profile_fm" name="profile_fm" method="post" onsubmit="return check_member_edit();" action="./profile.php">
                        <tr valign="middle" class="" colspan="1">
                            <td colspan="2" class="">※ 電子信箱：<?=$_SESSION['member_account']?></td>
                        </tr>
                        <tr valign="middle" class="">
                            <td colspan="2" class="">※ 姓名：(必填)

                                <label for="textfield2"></label>
                                <input name="name" id="name" type="text" class=" text" value="<?=$_SESSION['member_name']?>" size="3" style="width:100%;" placeholder="">
                                <p>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td height="10" colspan="2"></td>
                        </tr>
                        <tr class="table">
                            <td colspan="2" class="table">※ 手機號碼(必填)：

                                <label for="textfield3"></label>
                                <input name="cell" id="cell" value="<?=$_SESSION['member_phone']?>" type="text" class="td1 text" size="3" style="width:100%;" placeholder="請輸入手機號碼" >
                                <p>
                                </p>
                            </td>
                        </tr>
                        <tr><td height="10" colspan="2"></td></tr>
                        <tr class="table">
                            <td colspan="3" class="table row">
                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                    ※ 聯絡地址：
                                </div>
                                <div class="col-lg-6 col-sm-6 col-xs-6">
                                    <select id="縣市1" name="city" class="td1 text" style="width:99%;">
                                    </select>
                                </div>
                                <div class="col-lg-6 col-sm-6 col-xs-6">
                                    <select id="鄉鎮市區1" name="area" class="td1 text" style="width:100%;"></select>
                                </div>
                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                    <input name="road" type="text" class="td1 text credit_card3" placeholder="請輸入路名" value="<?=$_SESSION['member_road']?>" />
                                </div>
                            </td>
                        </tr>
                        <tr><td height="10" colspan="2"></td></tr>

                        <tr align="center" class="table">
                            <td height="100" colspan="2" align="center" valign="middle" class="">
                                <!--<input class="td1 btn01 alertbox-btn-2" type="submit" name="button2" id="button2" value="確認修改">-->
                                <input class="td1 btn01" type="submit" name="button2" id="button2" value="會員資料修改">
                            </td>
                        </tr>

                        <input type="hidden" name="member_edit" value="1" />
                        </form>

                        <form class="cd-form" id="password_fm" name="password_fm" method="post" onsubmit="return check_password_edit();" action="./profile.php">
                        <tr class="table">
                            <td colspan="3" class="table row">
                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                    ※ 修改密碼：(以下三項須全部輸入，才會做密碼更換)
                                    <input name="old_password" id="" value="" type="password" class="text" size="3" placeholder="請輸入您的舊密碼" style="width:100%;">
                                    <input name="new_password" id="" value="" type="password" class="text" size="3" placeholder="請輸入您的新密碼" style="width:100%;">
                                    <input name="again_password" id="" value="" type="password" class="text" size="3" placeholder="確認密碼(請再次輸入您的新密碼)" style="width:100%;">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td height="0"></td>
                        </tr>
                        <tr align="center" class="table">
                            <td height="100" colspan="2" align="center" valign="middle" class="">
                                <!--<input class="td1 btn01 alertbox-btn-2" type="submit" name="button2" id="button2" value="確認修改">-->
                                <input class="td1 btn01" type="submit" name="button2" id="button2" value="密碼修改">
                            </td>
                        </tr>

                        <input type="hidden" name="password_edit" value="1" />
                        </form>


                    </table>



                </div>
                <div class="clear"></div>
            </main>

            <!-- InstanceEndEditable -->
</div>
<div class="clear"></div>
</main>


</div><!--wrap結束-->
<!--內容結束-->


<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>


</body>

<!-- InstanceEnd --></html>



<? include("common_js.php"); ?>

<!--左列菜單-->
<? include("left_menu_js.php"); ?>

<!-- InstanceBeginEditable name="layout2 script" -->
<script>
    !function(){
        laydate.skin('default');//切换皮肤，请查看skins下面皮肤库
        laydate({elem: '#demo'});//绑定元素
    }();
    //日期范围限制
    var start = {
        elem: '#start',
        format: 'YYYY-MM-DD',
        min: laydate.now(), //设定最小日期为当前日期
        max: '2099-06-16', //最大日期
        istime: true,
        istoday: false,
        choose: function(datas){
            end.min = datas; //开始日选好后，重置结束日的最小日期
            end.start = datas //将结束日的初始值设定为开始日
        }
    };
    var end = {
        elem: '#end',
        format: 'YYYY-MM-DD',
        min: laydate.now(),
        max: '2099-06-16',
        istime: true,
        istoday: false,
        choose: function(datas){
            start.max = datas; //结束日选好后，充值开始日的最大日期
        }
    };
    laydate(start);
    laydate(end);
    //自定义日期格式
    laydate({
        elem: '#test1',
        format: 'YYYY年MM月DD日',
        festival: true, //显示节日
        choose: function(datas){ //选择日期完毕的回调
            alert('得到：'+datas);
        }
    });
    //日期范围限定在昨天到明天
    laydate({
        elem: '#hello3',
        min: laydate.now(+2), //-1代表昨天，-2代表前天，以此类推
        max: laydate.now(+30) //+1代表明天，+2代表后天，以此类推
    });
</script>

<!--台灣縣市下拉選單-->
<script src="js/AddressSelectList.min.js"></script>
<script type="text/javascript">
    window.onload = function () {
        //當頁面載完之後，用AddressSeleclList.Initialize()，
        //傳入要綁定的縣市下拉選單ID及鄉鎮市區下拉選單ID
        //AddressSeleclList.Initialize('縣市1', '鄉鎮市區1');
        AddressSeleclList.Initialize('縣市1', '鄉鎮市區1','<?=$_SESSION['member_city']?>', '0', '<?=$_SESSION['member_area']?>', '0');

        //後面四個參數分別是兩個下拉選單的預設文字跟值
        //AddressSeleclList.Initialize('縣市2', '鄉鎮市區2', 'Please Select City', '0', 'Please Select Area', '0');
    }
    function show() {
        //取出指定縣市及鄉鎮市區的下拉選單的值
        //alert(AddressSeleclList.ReturnSelectAddress('縣市1', '鄉鎮市區1'));
    }
</script>


<script>

    $( document ).ready(function() {


        //$(".sp-btn").trigger("click");


    });

    function check_member_edit()
    {
//
//        alert(document.profile_fm.city.value) ;
//        alert(document.profile_fm.area.value) ;
//
//
//        return false ;


        if( document.profile_fm.name.value == "" )
        {
            alert("姓名未輸入!") ;
            return false ;
        }

        if( document.profile_fm.cell.value == "" )
        {
            alert("手機號碼未輸入!") ;
            return false ;
        }

        if ( !(/^09[0-9]{8}$/.test(document.profile_fm.cell.value)) )
        {
            alert("手機號碼不是09XX開頭！，請輸入正確格式，共10位數字");
            document.profile_fm.cell.focus();
            return false;
        }

        document.profile_fm.submit() ;

    }

    function check_password_edit()
    {
        if( document.password_fm.old_password.value == "" )
        {
            alert("舊密碼尚未填寫");
            return false;
        }

        if( document.password_fm.new_password.value == "" )
        {
            alert("新密碼尚未填寫");
            return false;
        }

        if( document.password_fm.again_password.value == "" )
        {
            alert("再次輸入密碼尚未填寫");
            return false;
        }

        //密碼更換
        if( document.password_fm.old_password.value != "" && document.password_fm.new_password.value != "" && document.password_fm.again_password.value != "" )
        {
            if ( document.password_fm.new_password.value != document.password_fm.again_password.value  )
            {
                alert("您輸入的新密碼不一樣，請重新輸入");
                return false;
            }

        }

        document.password_fm.submit() ;
    }





    function check_logout()
    {
        window.location.href = "./index.php?logout=1" ;
    }

    function search_sent_check()
    {
        if( $("#keywords").val() == '請輸入關鍵字' || $("#keywords").val() == '' )
        {
            alert("您尚未輸入關鍵字");
            return ;
        }

        window.location.href = 'search.php?keyWord=' + $("#keywords").val()  ;
    }

    //訊息通知
    <?

    if( $_GET['logout'] == 2 )
    {
        echo 'alert( "您已登出!");' ;
        echo 'window.location.href = "signIn.php"';
    }

    if( $_GET['login_error'] == 1 )
    {
        echo 'alert( "登入失敗 帳號密碼錯誤 請重新輸入帳號密碼");' ;
        echo 'window.location.href = "signIn.php"';
    }

    if( $_GET['login_success'] == 1 )
    {
        echo 'alert( "您已成功登入");' ;
        echo 'window.location.href = "index.php"';
    }

    if( $_GET["BuyMode"] == 'ok')
    {
        echo 'alert( "您已下單成功");' ;
        echo 'window.location.href = "index.php"';
    }

    if( $_GET['error_register'] == 1 )
    {
        echo 'alert("註冊失敗");' ;
    }
    else if( $_GET['error_register'] == 2 )
    {
        echo 'alert("此EMAIL已被註冊過了!");' ;
    }

    ?>


jQuery(document).ready(function($) {

    /*當左邊或右邊選單隱藏、上面選單出現時，商品內容滿版*/
    if($('.main-top-wa').css("display") == "block"){
        $("#proList").attr("class","col-md-12 col-sm-12 col-xs-12");
        $("#proList #imgB").attr("style","width: 100%;");
    }
});

</script>

