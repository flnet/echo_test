<? include("head.php"); ?>

<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 19"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.背景(C)
$photo_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$photo_bg = $photo_style1->color ;

//2.文字(CH)
$photo_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$photo_txt = $photo_style2->color ;

//2.文字(CH)
$photo_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$photo_txt_hover = $photo_style3->color ;

?>

<style>
    /*相簿選單*/
    #header-left:hover #header-left-icon,#header-left:hover #header-left-text,#header-left-icon,#header-right:hover #header-right-icon,#header-right:hover #header-right-text,.gallery-header-center-right-links:hover,.gallery-header-center-right-links-current,.gallery-header-center-right-links-current:hover{background-color: <?=$photo_bg?>;}/*背景(C)*/
    .gallery-header-center-right-links{color: <?=$photo_txt?>;}/*文字(CH)*/
    .gallery-header-center-right-links-current,.gallery-header-center-right-links:hover {color: <?=$photo_txt_hover?>;} /*文字(CH) HOVER*/   
</style>

<?

$query_company_info  = "select company_info1_table.NAME AS info1_NAME , company_info1_table.LEVEL AS info1_LEVEL ,
                               company_info2_table.NAME AS info2_NAME , company_info2_table.IMAGE AS info2_IMAGE ,company_info2_table.LEVEL AS info2_LEVEL
                                 from company_info1 AS company_info1_table LEFT JOIN company_info2 AS company_info2_table
                                   ON company_info1_table.ID = company_info2_table.ON_LEVEL_ID
                                where company_info1_table.HIDE_ID = 0 and company_info2_table.HIDE_ID = 0
                                ORDER BY company_info1_table.LEVEL ASC ,company_info2_table.LEVEL ASC" ;

$result_company_info = mysql_query($query_company_info)or die(mysql_error());
while( $record_company_info = mysql_fetch_array($result_company_info) )
{
    $company_info2_image[$record_company_info["info1_NAME"]][] = $record_company_info["info2_IMAGE"]  ;
    $company_info2_name[$record_company_info["info1_NAME"]][] = $record_company_info["info2_NAME"]  ;
}

?>


<body style="">

<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">

    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <?=$all_page_name_array["photo"]?></li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>
    <!-- InstanceBeginEditable name="main" -->
    <main class="cd-main-content page clearfix">
        <div id="gallery">
            <div id="gallery-header">
                <div id="">
                    <div id="gallery-header-center-left">
                        <div class="gallery-header-center-right-links"  id="filter-all">All</div>

                        <?
                        $album_item = 0 ;
                        foreach( $company_info2_image as $info1_name => $info2_image_array )
                        {
                            $album_item = $album_item + 1 ;
                            echo '<div class="gallery-header-center-right-links album_class"  id="filter-f'.$album_item.'" item="'.$album_item.'" >'.$info1_name.'</div>' ;
                        }
                        ?>

                    </div>
                </div>
            </div>
            <div id="gallery-content">
                <div id="gallery-content-center" class="gallery-ul">
                    <ul class=""> 

                        <?
                        $photo_class_item = 0 ;
                        foreach( $company_info2_image as $info1_name => $info2_image_array )
                        {
                            $photo_class_item = $photo_class_item + 1 ;
                            $photo_class_all = 'all' ;
                            $photo_class_f = 'f'.($photo_class_item) ;

                            foreach( $info2_image_array as $info2_item => $info2_image )
                            {
                                echo '<li class="b-box">' ;

                                echo '<a href="'.FILE_PATH.'/company_info2/'.$info2_image.'" class="titan-lb imh_zoom" rel="fancybox-thumb '.$photo_class_all.' '.$photo_class_f.'" title="'.$company_info2_name[$info1_name][$info2_item].'">' ;

                                echo '<img src="'.FILE_PATH.'/company_info2/'.$info2_image.'" class="'.$photo_class_all.' '.$photo_class_f.'"/>' ;

                                echo '</a>' ;

                                echo '</li>' ;
                            }
                        }
                        ?>

                    </ul>
                </div>
            </div>
        </div>
    </main>

    <!-- InstanceEndEditable -->
</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>

<!-- InstanceEndEditable -->


</body>

<!-- InstanceEnd --></html>



<? include("common_js.php"); ?>

<!--fancyBox 產品點小圖變大圖輪播-->
<script>
    var isMobile = false;
    var isTablet =false;
    var isAndroidOS =false;
    var isiOS =false;
</script>
<script type="text/javascript" src="fancybox/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="fancybox/js/jquery.fancybox-thumbs.js"></script>
<!-- <script type="text/javascript" src="http://www.genplus.com.tw/product/../Scripts/fancybox/helpers/jquery.fancybox-media.js"></script>-->
<!--photoSwipe-->
<script type="text/javascript">
    $(function() {
        if (isMobile) {
            if ($('.titan-lb').length > 0) {
                $('.titan-lb').photoSwipe();
            }
        } else {
            $('.titan-lb').fancybox({
                padding: 0,//原10
                helpers: {
                    title: {
                        type: 'inside'
                    },
                    thumbs: {
                        width: 50,
                        height: 50
                    },
                    media: {}
                }
            });
        }

    });
</script>
<!--fancyBox 產品點小圖變大圖輪播 end-->

<!--瀑布流-->
<script type="text/javascript" src="waterfall-photo/js/jquery.isotope.min.js"></script>

<script type="text/javascript">
$( document ).ready(function() {
    $(window).scroll(function () {

        var size = 1;
        var button = 1;
        var button_class = "gallery-header-center-right-links-current";
        var normal_size_class = "gallery-content-center-normal";
        var full_size_class = "gallery-content-center-full";
        var $container = $('#gallery-content-center');
    if ($(window).scrollTop() >= ( $(document).height() - $(window).height() - 100 ) || $(window).scrollTop() >= ( $(window).height() -50 ))
           //if ($(window).scrollTop() >= ( $(window).height() -50 ) )
        {
        $container.isotope({itemSelector : 'img'});

        var photo_count = <?=$album_item?> ; //相簿的數目


        function check_button(button_number,target_id){

            $('.gallery-header-center-right-links').removeClass(button_class);

            if(button==1){
                $("#filter-all").addClass(button_class);
                $("#gallery-header-center-left-title").html('All Galleries');
            }

            if( button != 1 )
            {
                $("#"+target_id).addClass(button_class);
                $("#gallery-header-center-left-title").html('f'+(button_number-1)+' Gallery');
            }

        }

        function check_size(){
            $("#gallery-content-center").removeClass(normal_size_class).removeClass(full_size_class);
            if(size==0){
                $("#gallery-content-center").addClass(normal_size_class);
                $("#gallery-header-center-left-icon").html('<span class="iconb" data-icon="&#xe23a;"></span>');
            }
            if(size==1){
                $("#gallery-content-center").addClass(full_size_class);
                $("#gallery-header-center-left-icon").html('<span class="iconb" data-icon="&#xe23b;"></span>');
            }
            $container.isotope({itemSelector : 'img'});
        }

        $("#filter-all").click(function() { $container.isotope({ filter: '.all' }); button = 1; check_button(button,"filter-all"); });

        $(document).on('click touchstart', '.album_class', function(event) {

            $container.isotope({ filter:'.f'+parseInt($(this).attr("item")) }); button = parseInt($(this).attr("item"))+1 ; check_button(button,"filter-f"+parseInt($(this).attr("item")));

        });

        $("#gallery-header-center-left-icon").click(function() { if(size==0){size=1;}else if(size==1){size=0;} check_size(); });


        check_button(button,"filter-all");
        check_size();
    }


        // if($('.gallery-header-center-right-links').hasClass('gallery-header-center-right-links-current')){
        //     $('#gallery-content ul li a').attr('rel','fancybox-thumb all');
        // }
        // $('#filter-all').click(function(){
        //     $('#gallery-content ul li a').attr('rel','fancybox-thumb all');
        // })
        // $('.album_class').click(function(){
        //     $('#gallery-content ul li a').attr('rel','fancybox-thumb all ff');
        // })

    });
 });


    


// $('#filter-all').click(function(){
// $('#gallery-content ul li a').attr('rel','fancybox-thumb all');
// })


    
</script>

