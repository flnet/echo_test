
<? include("head.php"); ?>
<? include("king-color.php"); ?>
<style>
.my-table td a:hover{color: <?=$king_color?> <?=$king_color_important?>}/*2.文字HOVER*/
</style>

<?

if( $_SESSION['member_id'] == "" )
{
    header("Location:./");
    exit() ;
}

/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 17"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;
//1.整體文字(C)
$orderForm_style1 = json_decode($record_design_style2["STYLE1"]) ;
$orderForm_orderForm_page_my_table_td_a_color = $orderForm_style1->color ;
//2.文字HOVER
$orderForm_style2 = json_decode($record_design_style2["STYLE2"]) ;
$orderForm_stylePar_name2_1 = 'color' ;
$orderForm_stylePar_name2_1_1 = 'color-important' ;
$orderForm_my_table_td_a_hover = $orderForm_style2->$orderForm_stylePar_name2_1 ;
$orderForm_my_table_td_a_hover_important = $orderForm_style2->$orderForm_stylePar_name2_1_1 ;
//3.線條(C)
$orderForm_style3 = json_decode($record_design_style2["STYLE3"]) ;
$orderForm_stylePar_name1 = 'border-top' ;
$orderForm_stylePar_name2 = 'border-right' ;
$orderForm_stylePar_name3 = 'border-bottom' ;
$orderForm_stylePar_name4 = 'border-left' ;
$orderForm_orderForm_page_my_table_tr_orf_td_border_top = $orderForm_style3->$orderForm_stylePar_name1 ;
$orderForm_orderForm_page_my_table_tr_orf_td_border_last_child_right = $orderForm_style3->$orderForm_stylePar_name2 ;
$orderForm_orderForm_page_my_table_tr_orf_td_border_bottom = $orderForm_style3->$orderForm_stylePar_name3 ;
$orderForm_orderForm_page_my_table_tr_orf_td_last_child_border_left = $orderForm_style3->$orderForm_stylePar_name4 ;
//4.背景(C)
$orderForm_style4 = json_decode($record_design_style2["STYLE4"]) ;
$orderForm_stylePar_name5 = 'color-for' ;
$orderForm_stylePar_name6 = 'color-for-p' ;
$orderForm_stylePar_name7 = 'color-to' ;
$orderForm_stylePar_name8 = 'color-to-p' ;
$orderForm_orderForm_page_my_table_tr_orf_color_for = $orderForm_style4->$orderForm_stylePar_name5 ;
$orderForm_orderForm_page_my_table_tr_orf_color_for_p = $orderForm_style4->$orderForm_stylePar_name6 ;
$orderForm_orderForm_page_my_table_tr_orf_color_to = $orderForm_style4->$orderForm_stylePar_name7 ;
$orderForm_orderForm_page_my_table_tr_orf_color_to_p = $orderForm_style4->$orderForm_stylePar_name8 ;

$orderForm_stylePar_name9 = 'color-for-hover' ;
$orderForm_stylePar_name10 = 'color-for-p-hover' ;
$orderForm_stylePar_name11 = 'color-to-hover' ;
$orderForm_stylePar_name12 = 'color-to-p-hover' ;
$orderForm_orderForm_page_my_table_tr_orf_color_for_hover = $orderForm_style4->$orderForm_stylePar_name9 ;
$orderForm_orderForm_page_my_table_tr_orf_color_for_p_hover = $orderForm_style4->$orderForm_stylePar_name10 ;
$orderForm_orderForm_page_my_table_tr_orf_color_to_hover = $orderForm_style4->$orderForm_stylePar_name11 ;
$orderForm_orderForm_page_my_table_tr_orf_color_to_p_hover = $orderForm_style4->$orderForm_stylePar_name12 ;
?>
<style>
    /*文字(C)*/
    .orderForm-page,.my-table td a {color: <?=$orderForm_orderForm_page_my_table_td_a_color?>;}/*1.整體文字(C)*/
    .my-table td a:hover{color: <?=$orderForm_my_table_td_a_hover?> <?=$orderForm_my_table_td_a_hover_important?>}/*2.文字HOVER*/
    /*3.線條(C)*/
    .orderForm-page .my-table tr.orf td:first-child {border-left: <?=$orderForm_orderForm_page_my_table_tr_orf_td_last_child_border_left?>;}
    .orderForm-page .my-table tr.orf td {border-top: <?=$orderForm_orderForm_page_my_table_tr_orf_td_border_top?>;border-bottom: <?=$orderForm_orderForm_page_my_table_tr_orf_td_border_bottom?>;}
    .orderForm-page .my-table tr.orf td:last-child {border-right: <?=$orderForm_orderForm_page_my_table_tr_orf_td_border_last_child_right?>;}
    /*4.背景(C)*/
    .orderForm-page .my-table tr.orf {
        /*background: #ffffff; */
        background: -moz-linear-gradient(top, <?=$orderForm_orderForm_page_my_table_tr_orf_color_for?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_p?>%,<?=$orderForm_orderForm_page_my_table_tr_orf_color_to?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_to_p?>%);
        background: -webkit-linear-gradient(top, <?=$orderForm_orderForm_page_my_table_tr_orf_color_for?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_p?>%,<?=$orderForm_orderForm_page_my_table_tr_orf_color_to?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_to_p?>%);
        background: linear-gradient(to bottom, <?=$orderForm_orderForm_page_my_table_tr_orf_color_for?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_p?>%,<?=$orderForm_orderForm_page_my_table_tr_orf_color_to?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_to_p?>%);
    }
    .orderForm-page .my-table tr.orf:hover{
        /*background: #d8d8d8; */
        background: -moz-linear-gradient(top, <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_hover?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_p_hover?>%,<?=$orderForm_orderForm_page_my_table_tr_orf_color_to_hover?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_to_p_hover?>%);
        background: -webkit-linear-gradient(top, <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_hover?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_p_hover?>%,<?=$orderForm_orderForm_page_my_table_tr_orf_color_to_hover?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_to_p_hover?>%);
        background: linear-gradient(to bottom, <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_hover?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_for_p_hover?>%,<?=$orderForm_orderForm_page_my_table_tr_orf_color_to_hover?> <?=$orderForm_orderForm_page_my_table_tr_orf_color_to_p_hover?>%);
    }
</style>



<?
/*==== 取得商品樣式 手機板 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 18"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;
//1.整體文字(C)
$orderForm_mob_style1 = json_decode($record_design_style2["STYLE1"]) ;
$orderForm_mob_orderForm_page_my_table_td_a = $orderForm_mob_style1->color ;
//2.底線(C)
$orderForm_mob_style2 = json_decode($record_design_style2["STYLE2"]) ;
$orderForm_mob_stylePar_name1 = 'border-bottom' ;
$orderForm_orderForm_page_my_table_tr_orf = $orderForm_mob_style2->$orderForm_mob_stylePar_name1 ;
//3.背景(C)
$orderForm_mob_style3 = json_decode($record_design_style2["STYLE3"]) ;
$orderForm_mob_stylePar_name2 = 'color' ;
$orderForm_orderForm_page_my_table_tr_orf_orderForm_page_my_table_tr_orf_hover = $orderForm_mob_style3->$orderForm_mob_stylePar_name2 ;
?>

<style>
    /*手機板*/
    @media screen and (max-width: 767px){
        .orderForm-page, .my-table td a{color: <?=$orderForm_mob_orderForm_page_my_table_td_a?>;}/*1.整體文字(C)*/
        .orderForm-page .my-table tr.orf, .orderForm-page .my-table tr{border-bottom: <?=$orderForm_orderForm_page_my_table_tr_orf?>;}/*2.底線(C)*/
        .orderForm-page .my-table tr.orf,.orderForm-page .my-table tr.orf:hover{background: <?=$orderForm_orderForm_page_my_table_tr_orf_orderForm_page_my_table_tr_orf_hover?>;}/*3.背景*/
        .orderForm-page .my-table tr.orf td:first-child,.orderForm-page .my-table tr.orf td,.orderForm-page .my-table tr.orf td:last-child{border:0px;}
    }/*這行不管*/
</style>

<body style="">

<!-- 產品獨立的css -->
<link rel="stylesheet" href="css/mainKingLeft.css"><!-- 左列菜單 -->
<link rel="stylesheet" href="css/bootstrap.min.css"><!-- 左列菜單 -->


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->



<div class="alertbox-lastNumber">
    <div class="alertbox">
        <div class="alertbox-txt">
            <p class="pIbput">請輸入轉帳後五碼：<br>
                <input id="lastNumberText" type="text" maxlength="5" onkeyup="value=value.replace(/[^\d]/g,'') ">
            </p>

        </div>
        <a href="#" class="alertbox-lastNumber-s">確定</a>
        <a href="#" class="alertbox-lastNumber-cancel">取消</a>
    </div>
</div>

<div class="alertbox-wa-delete">
    <div class="alertbox-delete">
        <div class="alertbox-txt">
            <i class="fa fa-question"></i>
            <p>確定取消訂單嗎？</p>
        </div>
        <a href="#" class="alertbox-determine">確定</a>
        <a href="#" class="alertbox-cancel">取消</a>
    </div>
</div>

<div class="alertbox-wa-onLine">
    <div class="alertbox-onLine">
        <div class="alertbox-txt">
            <i class="fa fa-question"></i>
            <p>確定前往刷卡嗎？</p>
        </div>
        <a href="#" class="alertbox-onLine-determine">確定</a>
        <a href="#" class="alertbox-onLine-cancel">取消</a>
    </div>
</div>

<div class="alertbox-wa-3">
    <div class="alertbox-ok error_register_2">
        <div class="alertbox-txt">
            <i class="fa fa-check"></i>
            <p id="alert_text" >購物車更新成功!!</p>
        </div>
        <a href="#" class="alertbox-s-3 check">確定</a>
    </div>
</div>




<div id="gotop"></div>
<!-- Navbar -->
<header class="">
    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <a href="profile.html"><?=$all_page_name_array["member_center"]?></a> / 訂單查詢</li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>

    <main class="row page">


        <? include("member_menu.php"); ?>


        <main id="proList" class="col-md-9 col-sm-8 col-xs-12">
            <!-- InstanceBeginEditable name="mainKingRight" -->
            <main class="main-cont orderForm-page">

                <table class="my-table" border="0" cellspacing="0">
                    <tr class="">
                        <th>日　　期</th>
                        <th>訂單編號</th>
                        <th>處理進度</th>
                        <th>配送方式</th>
                        <th>付款方式</th>
                        <th>末五碼＆刷卡</th>
                        <th>應付金額</th>
                        <th>訂單狀態</th>
                    </tr>

                    <?
                    $item_count = 0 ;
                    $query = "select * , DATE_FORMAT(CHECK_ENTER_TIME,'%Y-%m-%d') AS CHECK_ENTER_DATE  from product_order1 where HIDE_ID = 0 and STATUS >= 2 and MEMBER_ID = ".$_SESSION['member_id']." order by ID DESC";
                    $result = mysql_query( $query ) or die( mysql_error() ) ;
                    while ( $record = mysql_fetch_array( $result ) )
                    {
                        $item_count++ ;

                        $id = $record["ID"];
                        $order_number  = str_pad($id,5,"0",STR_PAD_LEFT);

                        //抓取訂單處理狀態
                        if ($record["STATUS"] == 2) $record_column_data_4 = "待處理"; //原本是 客戶已下訂
                        if ($record["STATUS"] == 2) $record_column_data_4_color = "red";
                        if ($record["STATUS"] == 5) $record_column_data_4 = "處理中";
                        if ($record["STATUS"] == 5) $record_column_data_4_color = "red";
                        if ($record["STATUS"] == 3) $record_column_data_4 = "已出貨";
                        if ($record["STATUS"] == 3) $record_column_data_4_color = "mediumblue";
                        if ($record["STATUS"] == 4) $record_column_data_4 = "已取消";




                        //抓取配送方式
                        $parm_set[1] = "宅配";
                        $parm_set[2] = "貨到付款";
                        $parm_set[99] = "自行取貨";
                        $record_column_data_3 = $parm_set[$record["SHIPPING"]];

                        $bank = $record["BANK"] ;
                        $parm_back_set[200] = "線上刷卡";
                        $parm_back_set[201] = "轉帳付款";
                        $parm_back_set[202] = "貨到付款";
                        $parm_back_set[203] = "取貨付款";

                        //訂購日期
                        $record_column_data_2 = $record["CHECK_ENTER_DATE"] ;

                        //取貨時間
                        $pickup_date = $record["PICKUP_DATE"] ;

                        //商品金額
                        $total_money = $record["TOTAL_MONEY"] ;

                        //運費
                        $shipfee = $record["SHIPFEE"] ;

                        //總金額
                        $all_money = $total_money + $shipfee ;

                        //帳號後五碼
                        $last_five = $record["LAST_FIVE"];

                        //付款狀態
                        $pay_status = $record["PAY_STATUS"] ;

                        $email = $record["EMAIL"] ;

                        //刷卡分期（all:一次付清 03:三期 06:六期 12:十二期以此類推）
                        $credit_card_staging = $record["CREDIT_CARD_STAGING"] ;
                        $credit_card_staging_str = '' ;
                        if($credit_card_staging!=""){
                            if($credit_card_staging=="all"){
                                $credit_card_staging_str = '付清';
                            } else {
                                $credit_card_staging_str = $credit_card_staging ;
                            }
                        }

                        //當前設定的刷卡利率
                        $current_interest_rate = $record["CURRENT_INTEREST_RATE"] ;
                        if( $current_interest_rate != '' ){

                            $interest_rate_tmp = round(($current_interest_rate*$all_money)/100)  ;

                            $total_money_tmp_add_interest_rate = $interest_rate_tmp + $all_money ;
                        }


                        //如果是從結帳頁面過來刷卡

                        if( $_GET["onlineCard"] == 1 && $_GET["product_order1_id"] == $id )
                        {
                            $from_shopping_card_product_order1_id = $id ;
                            $from_shopping_card_money = $all_money ;
                            $from_shopping_card_email = $record["EMAIL"] ;

                            $from_shopping_card_credit_card_staging = $record["CREDIT_CARD_STAGING"] ;
                            $from_shopping_card_current_interest_rate = $record["CURRENT_INTEREST_RATE"] ;
                        }

                        ?>
                        
                        <tr class="orf">
                            <td data-th="日　　期" class="td1"><?=$record_column_data_2?></td>
                            <td data-th="訂單編號" class="td2"><a href="ShoppingCartHistory.php?ID=<?=$id?>"><?=$order_number?></a></td>
                            <td data-th="處理進度" class="td3"><?=$record_column_data_4?></td>
                            <td data-th="配送方式" class="td5"><?=$record_column_data_3?></td>
                            <td data-th="付款方式" class="td4"><?=$parm_back_set[$bank]?></td>

                            <td data-th="轉帳末五碼" class="td4">
                                <? if( $bank == 201 && $record["STATUS"] != 4 && $record["STATUS"] != 3 ) { ?>
                                    <a href="#" product_order1_id="<?=$id?>" class="alertbox-lastNumber-btn">
                                        <span href="#" class="alertbox-lastNumber-txt"><?=($last_five!=""?$last_five:"點我輸入")?></span>
                                    </a>
                                <? } ?>

                                <? if( $bank == 201 && $record["STATUS"] == 3 ) { ?>

                                    <span href="#" class="alertbox-lastNumber-txt"><?=($last_five!=""?$last_five:"尚未輸入")?></span>

                                <? } ?>

                                <? if( $bank == 200 && $pay_status == 2 ) { ?>

                                    <span href="#" class="alertbox-lastNumber-txt">刷卡完成</span>

                                <? } ?>

                                <? if( $bank == 200 && $pay_status == 1 ) { ?>

                                    <? if( $record["STATUS"] != 4 ) { ?>

                                        <a href="#" email="<?=$email?>" credit_card_staging="<?=$credit_card_staging?>" current_interest_rate="<?=$current_interest_rate?>" product_order1_id="<?=$id?>" money="<?=$total_money_tmp_add_interest_rate?>" class="alertbox-btn-onLine" >點我刷卡(<?=$credit_card_staging_str?>期)</a>

                                    <? } ?>



                                <? } ?>

                            </td>

                            <? if( $bank == 200 ) { ?>

                                <td data-th="應付金額" class="td4"><?=$total_money_tmp_add_interest_rate?></td>

                            <? } else { ?>

                                <td data-th="應付金額" class="td4"><?=$all_money?></td>

                            <? } ?>



                            <? if( $record["STATUS"] == 4 ) { ?>

                                <td data-th="訂單狀態" class="td5">已取消</td>

                            <? } else if( $record["STATUS"] == 2 && $pay_status == 1 ) { ?>

                                <td data-th="訂單狀態" class="td5"><a href="#" product_order1_id="<?=$id?>" class="alertbox-btn-delete">取消</a></td>

                            <? } else if( $record["STATUS"] == 3 || $record["STATUS"] == 5 || $pay_status == 2 ) { ?>

                                <td data-th="訂單狀態" class="td5">已成立</td>

                            <? } else { ?>

                                <td data-th="訂單狀態" class="td5"><a href="#" product_order1_id="<?=$id?>" class="alertbox-btn-delete">取消</a></td>

                            <? }  ?>


                        </tr>
                        <?
                    }
                    ?>

                    <tr class="my-td1">
                        <td colspan="7">共&nbsp;<?=$item_count?>&nbsp;筆訂單</td>
                    </tr>
                </table>

            </main>

            <!--            <div class="tab page">-->
            <!--                <div class="tabw">-->
            <!--                    <ul class="tabNumber">-->
            <!--                        <li class="first btnShare"><a onclick=".c" href="#" class="fa fa-angle-double-left"></a></li>-->
            <!--                        <li class="pre btnShare"><a href="#" class="fa fa-caret-left"></a></li>-->
            <!--                        <li class="tb"><a href="#">1</a></li>-->
            <!--                        <li class="tb"><a href="#">2</a></li>-->
            <!--                        <li class="tb"><a href="#">3</a></li>-->
            <!--                        <li class="next btnShare"><a href="#" class="fa fa-caret-right"></a></li>-->
            <!--                        <li class="last btnShare"><a href="#" class="fa fa-angle-double-right"></a></li>-->
            <!--                    </ul>-->
            <!--                </div>-->
            <!--                <div class="tabGo">-->
            <!--                    <div class="mobile">頁次：2/8</div>-->
            <!--                    <button>Go</button>-->
            <!--                    <input type="text" name="text" id="" placeholder="">-->
            <!--                </div>-->
            <!---->
            <!--            </div>-->
            <!-- InstanceEndEditable -->
</div>

<button type="" class="alertbox-btn-3" hidden ></button>


<form id="pay2go_form" action="" method="post" style="display: none;">
    <input type="hidden" name="MerchantID" value=""/>
    <input type="hidden" name="TradeInfo" value=""/>
    <input type="hidden" name="TradeSha" value=""/>
    <input type="hidden" name="Version" value=""/>
    <input type="hidden" name="RespondType" value=""/>
    <input type="hidden" name="TimeStamp" value=""/>
    <input type="hidden" name="MerchantOrderNo" value=""/>
    <input type="hidden" name="Amt" value=""/>
    <input type="hidden" name="ItemDesc" value=""/>
    <input type="hidden" name="Email" value=""/>
    <input type="hidden" name="EmailModify" value="0"/>
    <input type="hidden" name="ExpireDate" value=""/>
    <input type="hidden" name="LoginType" value=""/>
    <input type="hidden" name="NotifyURL" value=""/>
    <input type="hidden" name="ReturnURL" value="https://www.178tour.com.tw"/>
    <input type="hidden" name="ClientBackURL" value=""/>
    <input type="hidden" name="CustomerURL" value=""/>
    <!--    <input type="hidden" name="InstFlag" value="3"/>-->
</form>

<div class="clear"></div>
</main>


</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>



</body>

<!-- InstanceEnd --></html>


<? include("common_js.php"); ?>

<!--左列菜單-->
<? include("left_menu_js.php"); ?>




<script>
    jQuery(document).ready(function ($) {

        //處理取消的參數
        var delete_product_order1_id = 0 ;
        var sendCondition = new Object(); //傳遞search參數
        var current_delete_this ;

        //處理輸入後五碼的參數
        var lastFive_product_order1_id = 0 ;
        var sendLastFiveCondition = new Object(); //傳遞search參數
        var current_lastFive_this ;

        //後五碼彈出框
        $('.alertbox-lastNumber-btn').click(function (event) {
            event.preventDefault();

            current_lastFive_this = $(this) ;
            lastFive_product_order1_id = $(this).attr("product_order1_id") ;
            console.log("lastFive_product_order1_id="+lastFive_product_order1_id);

            var thisLN = $(this).find('span');
            thisLN.addClass('active');
            $(this).parents().siblings().find('span').removeClass('active');

            var thisLNtxt = thisLN.text();
            var alertboxInp = $('.alertbox').find('input').val(thisLNtxt);
            thisLNtxt == alertboxInp
            $('.alertbox').fadeIn();
            $('.alertbox-lastNumber').fadeIn();

            disable_scroll();



            $('.alertbox-lastNumber-cancel').click(function (event) {
                event.preventDefault();
                $('.alertbox-lastNumber').fadeOut().hide;
                //$('body').removeClass('body-overflow');
                enable_scroll();


            });

            //確定
            $('.alertbox-lastNumber-s').click(function (event) {
                event.preventDefault();
                var str = $(':text').val();
                $('.alertbox-lastNumber').fadeOut().hide;
                //$('body').removeClass('body-overflow');


                sendLastFiveCondition.product_order1_id = lastFive_product_order1_id;
                sendLastFiveCondition.lastFive = str;

                $.ajax(
                    {
                        url: './api/api_input_bank_five.php',
                        data: sendLastFiveCondition,
                        type: 'POST',
                        context: this,
                        error: function(xhr)
                        {
                            alert('Ajax request 發生錯誤');
                        },
                        success: function(responseObject)
                        {

                            if( responseObject["status"] == 1 )
                            {
                                console.log("訂單id="+responseObject["product_order1_id"]) ;
                                console.log("後五碼="+responseObject["lastFive"]) ;

                                $('.active').text(str);
                                enable_scroll();

                                $("#alert_text").html("修改成功!");
                                $(".alertbox-btn-3").trigger("click");
                            }
                            else
                            {
                                console.log(responseObject["status"]) ;
                                console.log(responseObject["reason"]) ;

                                $("#alert_text").html(responseObject["reason"]);
                                $(".alertbox-btn-3").trigger("click");

                                enable_scroll();
                            }

                            //商品追蹤處理
                            //goods_track($(this)) ;

                        }
                    }) ;




            });

            /*$('.alertbox-lastNumber-s').keydown(function (event) {
             if (event.keyCode == 13) {
             alert('點點點');
             return false;
             }
             });*/
        });






        //取消訂單彈出框
        $('.alertbox-btn-delete').click(function(event) {
            event.preventDefault();
            current_delete_this = $(this) ;
            delete_product_order1_id = $(this).attr("product_order1_id") ;
            console.log("delete_product_order1_id="+delete_product_order1_id);
            $('.alertbox-delete').fadeIn();
            $('.alertbox-wa-delete').fadeIn();
            //$('body').addClass('body-overflow');
            //$('html, body').scrollTop(0);
            disable_scroll();
        });

        //確定
        $('.alertbox-determine').click(function(event) {
            event.preventDefault();
            $('.alertbox-delete').fadeOut().hide;
            $('.alertbox-wa-delete').fadeOut().hide;
            $(this).parents('.take-li').fadeOut();
            //$('body').removeClass('body-overflow');


            console.log(current_delete_this.attr("product_order1_id"));

            console.log("確定");

            sendCondition.product_order1_id = delete_product_order1_id;


            $.ajax(
                {
                    url: './api/api_delete_order.php',
                    data: sendCondition,
                    type: 'POST',
                    context: this,
                    error: function(xhr)
                    {
                        alert('Ajax request 發生錯誤');
                    },
                    success: function(responseObject)
                    {

                        if( responseObject["status"] == 1 )
                        {
                            console.log(responseObject["product_order1_id"]) ;

                            current_delete_this.parent().text("已取消");
                        }
                        else
                        {
                            console.log(responseObject["status"]) ;
                            console.log(responseObject["reason"]) ;
                        }

                        enable_scroll();

                        //商品追蹤處理
                        //goods_track($(this)) ;

                    }
                }) ;

        });

        //取消
        $('.alertbox-cancel').click(function(event) {
            event.preventDefault();
            $('.alertbox-wa-delete').fadeOut().hide;
            //$('body').removeClass('body-overflow');
            console.log("取消");
            enable_scroll();
        });



        $('.alertbox-btn-3').click(function(event) {
            event.preventDefault();
            $('.error_register_2').fadeIn();
            $('.alertbox-wa-3').fadeIn();
            $('.alertbox').hide();
            //$('body').addClass('body-overflow');
            //$('html, body').scrollTop(0);
            disable_scroll();
        });

        //確定
        $('.alertbox-s-3').click(function(event) {
            event.preventDefault();
            //$('body').removeClass('body-overflow');
            $('.alertbox-wa-3').fadeOut().hide;
            enable_scroll();
        });


        //是否前往刷卡
        $('.alertbox-btn-onLine').click(function(event) {
            event.preventDefault();
            current_order1_this = $(this) ;
            //delete_product_order1_id = $(this).attr("product_order1_id") ;
            //console.log("delete_product_order1_id="+delete_product_order1_id);
            $('.alertbox-onLine').fadeIn();
            $('.alertbox-wa-onLine').fadeIn();
            //$('body').addClass('body-overflow');
            //$('html, body').scrollTop(0);
            disable_scroll();
        });

        //取消前往刷卡
        $('.alertbox-onLine-cancel').click(function(event) {
            event.preventDefault();
            $('.alertbox-wa-onLine').fadeOut().hide;
            //$('body').removeClass('body-overflow');
            //alert("取消刷卡");
            enable_scroll();
        });


        var pay_product_order1_id = 0 ;
        var sendPayCondition = new Object(); //傳遞search參數
        var current_order1_this ;

        //確定刷卡
        $('.alertbox-onLine-determine').click(function(event) {
            event.preventDefault();
            $('.alertbox-onLine').fadeOut().hide;
            $('.alertbox-wa-onLine').fadeOut().hide;
            $(this).parents('.take-li').fadeOut();
            //$('body').removeClass('body-overflow');
            //alert("前往刷卡");


            pay_product_order1_id = current_order1_this.attr("product_order1_id") ; ;
            sendPayCondition.product_order1_id = pay_product_order1_id;
            sendPayCondition.money = current_order1_this.attr("money");
            sendPayCondition.email = current_order1_this.attr("email");
            sendPayCondition.credit_card_staging = current_order1_this.attr("credit_card_staging"); //刷卡分期（all:一次付清 03:三期 06:六期 12:十二期以此類推）
            sendPayCondition.current_interest_rate = current_order1_this.attr("current_interest_rate"); //當前設定的刷卡利率


            //alert("email="+sendPayCondition.email+" product_order1_id="+sendPayCondition.product_order1_id+" money="+sendPayCondition.money);

            $.ajax(
                {
                    url: './api/api_paySpgateway.php',
                    data: sendPayCondition,
                    type: 'POST',
                    context: this,
                    error: function(xhr)
                    {
                        alert('Ajax request 發生錯誤');
                    },
                    success: function(responseObject)
                    {

                        console.log(JSON.stringify(responseObject)) ;

                        if( responseObject.status == 1 ){

                            var form_id = "#pay2go_form";

                            $(form_id).attr("action", responseObject.Pay2goUrl);
                            $("input[name='MerchantID']").val(responseObject.MerchantID);
                            $("input[name='RespondType']").val(responseObject.RespondType);
                            $("input[name='TradeInfo']").val(responseObject.TradeInfo);
                            $("input[name='TradeSha']").val(responseObject.TradeSha);
                            $("input[name='TimeStamp']").val(responseObject.TimeStamp);
                            $("input[name='Version']").val(responseObject.Version);
                            $("input[name='MerchantOrderNo']").val(responseObject.MerchantOrderNo);
                            $("input[name='Amt']").val(responseObject.Amt);
                            $("input[name='ItemDesc']").val(responseObject.ItemDesc);
                            $("input[name='Email']").val(responseObject.Email);
                            $("input[name='ExpireDate']").val(responseObject.ExpireDate);
                            $("input[name='LoginType']").val(responseObject.LoginType);
                            //$("input[name='InstFlag']").val(responseObject.InstFlag);
//                        $("input[name='NotifyURL']").val(responseObject.NotifyURL);
//                        $("input[name='ReturnURL']").val(responseObject.ReturnURL);
//                        $("input[name='CustomerURL']").val(responseObject.CustomerURL);
                            $(form_id).submit();

                        } else {

                            window.location.href = "orderForm.php" ;
                        }


                    }
                }) ;

        });




        <? if( $_GET["onlineCard"] == 1 && $_GET["product_order1_id"] != ''  ) { ?>

        var from_shopping_card_product_order1_id = <?=$from_shopping_card_product_order1_id?> ;
        var from_shopping_card_money = <?=$from_shopping_card_money?> ;
        var from_shopping_card_email = '<?=$from_shopping_card_email?>' ;
        var from_shopping_card_credit_card_staging = '<?=$from_shopping_card_credit_card_staging?>' ;
        var from_shopping_card_current_interest_rate = '<?=$from_shopping_card_current_interest_rate?>' ;


        sendPayCondition.product_order1_id = from_shopping_card_product_order1_id;
        sendPayCondition.money = from_shopping_card_money ;
        sendPayCondition.email = from_shopping_card_email;
        sendPayCondition.credit_card_staging = from_shopping_card_credit_card_staging ; //刷卡分期（all:一次付清 03:三期 06:六期 12二期以此類推）
        sendPayCondition.current_interest_rate = from_shopping_card_current_interest_rate; //當前設定的刷卡利率

        //alert("from_shopping_card_product_order1_id="+from_shopping_card_product_order1_id+" from_shopping_card_money="+from_shopping_card_money+" from_shopping_card_email="+from_shopping_card_email) ;

        $.ajax(
            {
                url: './api/api_paySpgateway.php',
                data: sendPayCondition,
                type: 'POST',
                context: this,
                error: function(xhr)
                {
                    alert('Ajax request 發生錯誤');
                },
                success: function(responseObject)
                {

                    console.log(JSON.stringify(responseObject)) ;

                    if( responseObject.status == 1 ){

                        var form_id = "#pay2go_form";

                        $(form_id).attr("action", responseObject.Pay2goUrl);
                        $("input[name='MerchantID']").val(responseObject.MerchantID);
                        $("input[name='RespondType']").val(responseObject.RespondType);
                        $("input[name='TradeInfo']").val(responseObject.TradeInfo);
                        $("input[name='TradeSha']").val(responseObject.TradeSha);
                        $("input[name='TimeStamp']").val(responseObject.TimeStamp);
                        $("input[name='Version']").val(responseObject.Version);
                        $("input[name='MerchantOrderNo']").val(responseObject.MerchantOrderNo);
                        $("input[name='Amt']").val(responseObject.Amt);
                        $("input[name='ItemDesc']").val(responseObject.ItemDesc);
                        $("input[name='Email']").val(responseObject.Email);
                        $("input[name='ExpireDate']").val(responseObject.ExpireDate);
                        $("input[name='LoginType']").val(responseObject.LoginType);
                        //$("input[name='InstFlag']").val(responseObject.InstFlag);
//                        $("input[name='NotifyURL']").val(responseObject.NotifyURL);
//                        $("input[name='ReturnURL']").val(responseObject.ReturnURL);
//                        $("input[name='CustomerURL']").val(responseObject.CustomerURL);
                        $(form_id).submit();

                    }  else {

                        window.location.href = "orderForm.php" ;
                    }




                }
            }) ;

        <? } ?>

        

    });


    jQuery(document).ready(function($) {

        /*當左邊或右邊選單隱藏、上面選單出現時，商品內容滿版*/
        if($('.main-top-wa').css("display") == "block"){
            $("#proList").attr("class","col-md-12 col-sm-12 col-xs-12");
        }
    });
</script>


<!--<script>
    !function(){
        laydate.skin('default');//切换皮肤，请查看skins下面皮肤库
        laydate({elem: '#demo'});//绑定元素
    }();
    //日期范围限制
    var start = {
        elem: '#start',
        format: 'YYYY-MM-DD',
        min: laydate.now(), //设定最小日期为当前日期
        max: '2099-06-16', //最大日期
        istime: true,
        istoday: false,
        choose: function(datas){
            end.min = datas; //开始日选好后，重置结束日的最小日期
            end.start = datas //将结束日的初始值设定为开始日
        }
    };
    var end = {
        elem: '#end',
        format: 'YYYY-MM-DD',
        min: laydate.now(),
        max: '2099-06-16',
        istime: true,
        istoday: false,
        choose: function(datas){
            start.max = datas; //结束日选好后，充值开始日的最大日期
        }
    };
    laydate(start);
    laydate(end);
    //自定义日期格式
    laydate({
        elem: '#test1',
        format: 'YYYY年MM月DD日',
        festival: true, //显示节日
        choose: function(datas){ //选择日期完毕的回调
            alert('得到：'+datas);
        }
    });
    //日期范围限定在昨天到明天
    laydate({
        elem: '#hello3',
        min: laydate.now(+2), //-1代表昨天，-2代表前天，以此类推
        max: laydate.now(+30) //+1代表明天，+2代表后天，以此类推
    });
</script>-->

