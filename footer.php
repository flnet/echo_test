<?
$query="select * from introduction where ID = 1 ";
$result = mysql_query($query) or die(mysql_error()) ;
while( $record=mysql_fetch_array($result) )
{
    $introduction_summary = $record["SUMMARY"] ; //公司簡介
    $introduction_address = $record["ADDRESS"] ; //公司地址
    $introduction_cell = $record["CELL"] ; //公司電話
    $introduction_email = $record["EMAIL"] ; //公司信箱
    $introduction_fax = $record["FAX"] ; //公司傳真
    $introduction_data_facebookUrl = $record["FACEBOOK_URL"] ;
    $introduction_data_facebookName = $record["FACEBOOK_NAME"] ;
    $introduction_data_facebookOpen = $record["FACEBOOK_OPEN"] ;
    $introduction_translation = $record["GOOD_TRANSLATION"] ; //google翻譯網站原始語言
    $introduction_translation_off = $record["GOOD_OFF"] ; //google翻譯關閉
    $introduction_ga = $record["GOOD_GA"] ; //googleGA
    $introduction_goodMap = $record["GOOD_MAP"] ; //google地圖
    $introduction_info = $record["INFO1"] ; //版權宣告
    $introduction_customized = $record["CUSTOMIZED"] ; //自訂區
}
?>

<?
    /*==== 取得網頁小工具-聯絡資訊小圖示樣式 Start====*/
    $query_web_tool2 = "select * from web_tool2 where HIDE_ID = 0 and ID = 1"  ;
    $result_web_tool2 = mysql_query( $query_web_tool2 ) or die( mysql_error() ) ;
    $record_web_tool2 = mysql_fetch_array( $result_web_tool2 ) ;

    //1.整體
    $contaction_style1 = json_decode($record_web_tool2["STYLE1"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'align' ;
    $contaction_display = $contaction_style1->$contaction_stylePar_name1 ;
    $contaction_align = $contaction_style1->$contaction_stylePar_name2 ;

    //2.第1個圖示 phone電話 固定
    $contaction_style2 = json_decode($record_web_tool2["STYLE2"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE2"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_stylePar_name5 = 'color' ;
    $contaction_stylePar_name6 = 'bg-color' ;
    $contaction_li_cont_icon_1_display = $contaction_style2->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_1_link = $contaction_style2->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_1_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_1_order = $contaction_style2->$contaction_stylePar_name4 ;
    $contaction_li_cont_icon_1_color = $contaction_style2->$contaction_stylePar_name5 ;
    $contaction_li_cont_icon_1_bg_color = $contaction_style2->$contaction_stylePar_name6 ;

    //3.第2個圖示
    $contaction_style3 = json_decode($record_web_tool2["STYLE3"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE3"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_2_display = $contaction_style3->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_2_link = $contaction_style3->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_2_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_2_order = $contaction_style3->$contaction_stylePar_name4 ;

    //4.第3個圖示
    $contaction_style4 = json_decode($record_web_tool2["STYLE4"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE4"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_3_display = $contaction_style4->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_3_link = $contaction_style4->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_3_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_3_order = $contaction_style4->$contaction_stylePar_name4 ;

    //5.第4個圖示
    $contaction_style5 = json_decode($record_web_tool2["STYLE5"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE5"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_4_display = $contaction_style5->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_4_link = $contaction_style5->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_4_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_4_order = $contaction_style5->$contaction_stylePar_name4 ;

    //6.第5個圖示
    $contaction_style6 = json_decode($record_web_tool2["STYLE6"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE6"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_5_display = $contaction_style6->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_5_link = $contaction_style6->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_5_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_5_order = $contaction_style6->$contaction_stylePar_name4 ;

    //7.第6個圖示
    $contaction_style7 = json_decode($record_web_tool2["STYLE7"]) ; 
    $contaction_stylePar_name1 = 'display' ;
    $contaction_stylePar_name2 = 'link' ;
    $contaction_stylePar_name3 = FILE_PATH."/web_tool2/".$record_web_tool2["MODIFY_IMAGE7"] ;
    $contaction_stylePar_name4 = 'order' ;
    $contaction_li_cont_icon_6_display = $contaction_style7->$contaction_stylePar_name1 ;
    $contaction_li_cont_icon_6_link = $contaction_style7->$contaction_stylePar_name2 ;
    $contaction_li_cont_icon_6_image = $contaction_stylePar_name3 ;
    $contaction_li_cont_icon_6_order = $contaction_style7->$contaction_stylePar_name4 ;

?>
<style>
    /* 1.整體 */
    ul.contaction{display:<?=$contaction_display?>}
    /* 2.第1個圖示 phone電話 固定 */
    ul.contaction li.cont-icon-1{display:<?=$contaction_li_cont_icon_1_display?>;order:<?=$contaction_li_cont_icon_1_order?>}/*開關、排序*/
    ul.contaction li a#phone p{color:<?=$contaction_li_cont_icon_1_color?>;background-color:<?=$contaction_li_cont_icon_1_bg_color?>}/*滑入的電話顏色*/
    /* 3.第2個圖示*/
    ul.contaction li.cont-icon-2{display:<?=$contaction_li_cont_icon_2_display?>;order:<?=$contaction_li_cont_icon_2_order?>}/*開關、排序*/ 
    /* 4.第2個圖示*/
    ul.contaction li.cont-icon-3{display:<?=$contaction_li_cont_icon_3_display?>;order:<?=$contaction_li_cont_icon_3_order?>}/*開關、排序*/ 
    /* 5.第2個圖示*/
    ul.contaction li.cont-icon-4{display:<?=$contaction_li_cont_icon_4_display?>;order:<?=$contaction_li_cont_icon_4_order?>}/*開關、排序*/ 
    /* 6.第2個圖示*/
    ul.contaction li.cont-icon-5{display:<?=$contaction_li_cont_icon_5_display?>;order:<?=$contaction_li_cont_icon_5_order?>}/*開關、排序*/ 
    /* 7.第2個圖示*/
    ul.contaction li.cont-icon-6{display:<?=$contaction_li_cont_icon_6_display?>;order:<?=$contaction_li_cont_icon_6_order?>}/*開關、排序*/ 
</style>

<?
    /*==== 取得網頁小工具-是否禁用右鍵 Start====*/
    $query_web_tool2 = "select * from web_tool2 where HIDE_ID = 0 and ID = 2"  ;
    $result_web_tool2 = mysql_query( $query_web_tool2 ) or die( mysql_error() ) ;
    $record_web_tool2 = mysql_fetch_array( $result_web_tool2 ) ;

    //1.鎖右鍵
    $prohibited_style1 = json_decode($record_web_tool2["STYLE1"]) ; 
    $cprohibited_stylePar_name1 = 'prohibited' ;
    $prohibited = $prohibited_style1->$cprohibited_stylePar_name1 ;
?>

<?
    /*==== 取得網頁小工具-瀏覽人數計數器 Start====*/
    $query_web_tool2 = "select * from web_tool2 where HIDE_ID = 0 and ID = 3"  ;
    $result_web_tool2 = mysql_query( $query_web_tool2 ) or die( mysql_error() ) ;
    $record_web_tool2 = mysql_fetch_array( $result_web_tool2 ) ;

    //1.瀏覽人數計數器
    $visits_people_style1 = json_decode($record_web_tool2["STYLE1"]) ; 
    $visits_people_stylePar_name1 = 'display' ;
    $visits_people_stylePar_name2 = 'visits_people' ;
    $visits_people_stylePar_name3 = 'visits_name' ;
    $visits_people_display = $visits_people_style1->$visits_people_stylePar_name1 ;
    $visits_people = $visits_people_style1->$visits_people_stylePar_name2 ;
    $visits_name = $visits_people_style1->$visits_people_stylePar_name3 ;
?>


<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 42"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;


//1.下半部
$footer_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$footer_stylePar_name1 = 'color' ;
$footer_stylePar_name2 = 'background-color' ;
$footer_stylePar_name3 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;
$footer_stylePar_name4 = 'border-top' ;
$footer_stylePar_name5 = 'border-right' ;
$footer_stylePar_name6 = 'border-bottom' ;
$footer_stylePar_name7 = 'border-left' ;
$footer_foote_color = $footer_style1->$footer_stylePar_name1 ;
$footer_foote_background_color = $footer_style1->$footer_stylePar_name2 ;
$footer_foote_background_image = $footer_stylePar_name3 ;
$footer_foote_border_top = $footer_style1->$footer_stylePar_name4 ;
$footer_foote_border_right = $footer_style1->$footer_stylePar_name5 ;
$footer_foote_border_bottom = $footer_style1->$footer_stylePar_name6 ;
$footer_foote_border_left = $footer_style1->$footer_stylePar_name7 ;

//2.上半部
$footer_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$footer_stylePar_name1 = 'color' ;
$footer_stylePar_name2 = 'color-hover' ;
$footer_stylePar_name3 = 'background-color' ;
$footer_stylePar_name4 = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;
$footer_stylePar_name5 = 'border-top' ;
$footer_stylePar_name6 = 'border-right' ;
$footer_stylePar_name7 = 'border-bottom' ;
$footer_stylePar_name8 = 'border-left' ;
$footer_foote_footer_nav_color = $footer_style2->$footer_stylePar_name1 ;
$footer_foote_footer_nav_color_hover = $footer_style2->$footer_stylePar_name2 ;
$footer_foote_footer_nav_background_color = $footer_style2->$footer_stylePar_name3 ;
$footer_foote_footer_nav_background_image = $footer_stylePar_name4 ;
$footer_foote_footer_nav_border_top = $footer_style2->$footer_stylePar_name5 ;
$footer_foote_footer_nav_border_right = $footer_style2->$footer_stylePar_name6 ;
$footer_foote_footer_nav_border_bottom = $footer_style2->$footer_stylePar_name7 ;
$footer_foote_footer_nav_border_left = $footer_style2->$footer_stylePar_name8 ;
?>

<style>
    /*整體FOOTER文字(C)、背景(P)*/
    footer, .footer-conter a{color: <?=$footer_foote_color?>;background-color: <?=$footer_foote_background_color?>; background-image: url(<?=$footer_foote_background_image?>);border-top: <?=$footer_foote_border_top?>;border-right: <?=$footer_foote_border_right?>;border-bottom: <?=$footer_foote_border_bottom?>;border-left: <?=$footer_foote_border_left?>;}/*1.下半部-體FOOTER文字(C)、背景(P)*/
    .footer-conter a:hover{opacity: 0.5;}
    footer .footer-nav {background-color: <?=$footer_foote_footer_nav_background_color?>;background-image: url(<?=$footer_foote_footer_nav_background_image?>);border-top: <?=$footer_foote_footer_nav_border_top?>;border-right: <?=$footer_foote_footer_nav_border_right?>;border-bottom: <?=$footer_foote_footer_nav_border_bottom?>;border-left: <?=$footer_foote_footer_nav_border_left?>;}/*2.上半部-背景(P)、線條(C)*/
    .footer-nav li,.footer-nav li a{color: <?=$footer_foote_footer_nav_color?>;} /*3.文字(CH)*/
    .footer-nav li a:hover{color: <?=$footer_foote_footer_nav_color_hover?>;} /*2.上半部-文字HOVER*/

    /*最底部FOOTER 小圖示(PH)，這裡就是50X50的圖跟HOVER圖+連結*/
    .footer-icon a.fi-1{background-image: url(images/icon-01-1.png);}
    .footer-icon a.fi-1:hover{background-image: url(images/icon-02-1.png);}
    .footer-icon a.fi-2{background-image: url(images/icon-01-2.png);}
    .footer-icon a.fi-2:hover{background-image: url(images/icon-02-2.png);}
    .footer-icon a.fi-3{background-image: url(images/icon-01-3.png);}
    .footer-icon a.fi-3:hover{background-image: url(images/icon-02-3.png);}
    .footer-icon a.fi-4{background-image: url(images/icon-01-4.png);}
    .footer-icon a.fi-4:hover{background-image: url(images/icon-02-4.png);}
    .footer-icon a.fi-5{background-image: url(images/icon-01-5.png);}
    .footer-icon a.fi-5:hover{background-image: url(images/icon-02-5.png);}
    .footer-icon a.fi-6{background-image: url(images/icon-01-6.png);}
    .footer-icon a.fi-6:hover{background-image: url(images/icon-02-6.png);}
</style>
<!--<footer style="background-image: url(images/footer-bg.png);">背景圖1(P)-->
<footer>
<!--<div class="footer-nav" style="background-image: url(images/footer-nav.png);">背景圖2(P)-->
<div class="footer-nav">
    <div class="page">
        <ul>
            <?
            foreach( $page1_array as $page1_id => $page1_id_array )
            {
                foreach( $page1_id_array as $page1_name => $page1_type )
                {
            ?>
                    <?
                    if( $page1_id == 1 )
                    {
                        ?>
                        <!--首頁-->
                        <li><a href="index.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id == 2 )
                    {
                    ?>
                        <!--線上購物-->
                        <?
                        if( $web_style == SHOPPING_CART )
                        {
                            echo '<li><a href="products.php?page1_ID=2">'.$page1_name.'</a><span>/</span></li>' ;
                        }
                        else
                        {
                            echo '<li><a href="products-images.php?page1_ID=2">'.$page1_name.'</a><span>/</span></li>' ;
                        }
                        ?>

                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 3 )
                    {
                        ?>
                        <!--最新消息-->
                        <li><a href="news.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id == 4 )
                    {
                        ?>
                        <!--聯絡我們-->
                        <li><a href="connection.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id == 5 )
                    {
                        ?>
                        <!--相簿-->
                        <li><a href="photo.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id == 6 )
                    {
                    ?>


                        <?
                        if( $_SESSION['member_id'] == "") {
                            ?>
                            <!--產品問與答-->
                            <li><a href="#" class="alertbox-btn-noLogin"><?=$page1_name?></a><span>/</span></li>
                            <?
                        }
                        else
                        {
                            ?>
                            <li><a href="qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                            <?
                        }
                        ?>


                        <?
                    }
                    ?>

                    <?
                    if( $page1_id == 7 )
                    {
                    ?>
                    <?
                    if( $_SESSION['member_id'] == "") {
                        ?>
                        <!--會員中心-->
                        <li><a href="#" class="alertbox-btn-noLogin"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    else
                    {
                        ?>
                        <li><a href="profile.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>
                    <?
                    }
                    ?>

                    <?
                    if( $page1_id == 8 )
                    {
                        ?>
                        <!--關於我們-->
                        <li><a href="about.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id == 9 )
                    {
                        ?>
                        <!--常見Ｑ＆Ａ-->
                        <li><a href="common-qa.php?page1_ID=<?=$page1_id?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id > 9 && $page1_type == 2 )
                    {
                        ?>
                        <!--共用頁面-->
                        <li><a href="page.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id > 9 && $page1_type == 3 )
                    {
                        ?>
                        <li><a href="pageDetailed.php?page1_ID=<?=$page1_id?>&page1_type=<?=$page1_type?>"><?=$page1_name?></a><span>/</span></li>
                        <?
                    }
                    ?>

                    <?
                    if( $page1_id > 9 && $page1_type == 4 )
                    {
                        $all_page_name_array["pageDetailed_".$page1_id] = $page1_name ;
                    ?>
                        <li><!--自訂頁面 連結頁面-->
                            <a href="<?=$page1_url_array[$page1_id]?>"><?=$page1_name?></a> <span>/</span>
                        </li>
                    <?
                    }
                    ?>


            <?
                }
            }
            ?>
        </ul>
    </div>
</div>
<div class="footer-conter">
    <div class="page"><?=$introduction_info?></br>
        <?=$introduction_address?>｜服務電話：<?=$introduction_cell?>｜傳真:<?=$introduction_fax?>｜Email:<?=$introduction_email?>
        <!-- ｜系統提供 by <a href="https://www.qu106.com.tw/">皇后架站</a> -->
    </div>
</div>
<!--<div class="footer-icon">
    <div class="page">
        <ul>
            <li ><a href="" class="fa fi-1" target="new"></a></li>
            <li ><a href="" class="fa fi-2"></a></li>
            <li ><a href="" class="fa fi-3" target="new"></a></li>
            <li ><a href="" class="fa fi-4"></a></li>
            <li ><a href="" class="fa fi-5"></a></li>
            <li ><a href="" class="fa fi-6"></a></li>
        </ul>
    </div>
</div>-->

<!--google語言-->
<div class="customized">
    <?=$introduction_customized?>
</div>

 <!--googe翻譯-->
 <!-- <div id="google_translate_element" style="display:<?=$introduction_translation_off?>"></div>
    <script type="text/javascript">
    function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: '<?=$introduction_translation?>', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>  -->

<div id="google_translate_element" style="display:<?=$introduction_translation_off?>">
</div>
    <script type="text/javascript">
    function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: '<?=$introduction_translation?>'}, 'google_translate_element');
    }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"> </script>


<!--嵌入facebook fb-->
<!-- <div class="embedded-fb" style="display:<?=$introduction_data_facebookOpen?>">
    <div class="fb-page" data-href="<?=$introduction_data_facebookUrl?>" data-tabs="timeline" width="500" data-height="70" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
        <blockquote cite="<?=$introduction_data_facebookUrl?>" class="fb-xfbml-parse-ignore">
            <a href="<?=$introduction_data_facebookUrl?>"><?=$introduction_data_facebookName?></a>
        </blockquote>
    </div>
    <div id="fb-root"></div>
</div> -->

    <ul class="contaction">
        <li class="cont-icon-6"><a href="<?=$contaction_li_cont_icon_6_link?>" id=""><img src="<?=$contaction_li_cont_icon_6_image?>"></a></li>
        <li class="cont-icon-5"><a href="<?=$contaction_li_cont_icon_5_link?>" id=""><img src="<?=$contaction_li_cont_icon_5_image?>"></a></li>
        <li class="cont-icon-4"><a href="<?=$contaction_li_cont_icon_4_link?>" id="map"><img src="<?=$contaction_li_cont_icon_4_image?>"></a></li>
        <li class="cont-icon-3"><a href="<?=$contaction_li_cont_icon_3_link?>" id="fb"><img src="<?=$contaction_li_cont_icon_3_image?>"></a></li>
        <li class="cont-icon-2"><a href="<?=$contaction_li_cont_icon_2_link?>" id="line"><img src="<?=$contaction_li_cont_icon_2_image?>"></a></li>
        <li class="cont-icon-1"><a href="" id="phone"><img src="<?=$contaction_li_cont_icon_1_image?>"><p><?=$contaction_li_cont_icon_1_link?></p></a></li>
    </ul >
    <div class="align" style="display:none"><?=$contaction_align?></div>
    <div class="visits_people" style="display:<?=$visits_people_display?>;padding-bottom:10px;">您是第&nbsp;&nbsp;
        <span>
            <a href="http://www.cutercounter.com/" target="_blank"><img src="<?=$visits_people?>" border="0" alt="<?=$visits_name?>"></a>
        </span>&nbsp;&nbsp;位造訪本站用戶！
    </div>
</footer>

<script>
    //網頁小工具-聯絡資訊圖示
    var tel_str ="<?=$contaction_li_cont_icon_1_link?>";
    var tel_num = tel_str.replace(/[^0-9]/ig,"");
    var tel_phone = "tel:"+tel_num;
    $('#phone').attr('href',tel_phone);

    if($('.align').text() == "left"){
        $('ul.contaction').css({'left': '15px','bottom': '15px'})
    }
    if($('.align').text() == "right"){
        $('ul.contaction').css({'left': 'unset','right': '25px','bottom': '200px'});
        $('ul.contaction li a#phone p').css({'left': 'unset','right': '50px'});
    }

    //禁用右鍵
    var prohibited = <?=$prohibited?>;
    /*** 禁用右键菜单*/
    document.oncontextmenu = function () {event.returnValue = prohibited;};
    /***禁用选中功能*/
    document.onselectstart = function () {event.returnValue = prohibited;};
    /*** 禁用复制功能*/
    document.oncopy = function () {event.returnValue = prohibited;};
    /*** 禁用鼠标的左右键* @param {Object} e */
    document.onmousedown = function () {
        // if (event.which == 1) {//鼠标左键
        //     return prohibited;
        // }
        if (event.which == 3) {//鼠标右键
            return prohibited;
        }
    };
    /*** 获取键盘上的输入值*/
    document.onkeydown = function () {
        console.info(event.which);
        if (event.which == 13) {
            console.info("回车键");
        }
    };
</script>











