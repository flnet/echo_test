<? include("head.php"); ?>

<?
/*==== 取得商品樣式 Start====*/
$query_design_style2 = "select * from design_style2 where HIDE_ID = 0 and ID = 7"  ;
$result_design_style2 = mysql_query( $query_design_style2 ) or die( mysql_error() ) ;
$record_design_style2 = mysql_fetch_array( $result_design_style2 ) ;

//1.Q小圖 30X30(P)
$common_qa_style1 = json_decode($record_design_style2["STYLE1"]) ; 
$common_qa_common_qa_itle_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE1"] ;
//2.A小圖 30X30(P)
$common_qa_style2 = json_decode($record_design_style2["STYLE2"]) ; 
$common_qa_common_qa_cont_background_image = FILE_PATH."/design_style2/".$record_design_style2["MODIFY_IMAGE2"] ;
//3.展開-文字、線條、背景(C)
$common_qa_style3 = json_decode($record_design_style2["STYLE3"]) ; 
$common_qa_stylePar_name1 = 'color' ;
$common_qa_stylePar_name2 = 'border-top' ;
$common_qa_stylePar_name3 = 'border-right' ;
$common_qa_stylePar_name4 = 'border-bottom' ;
$common_qa_stylePar_name5 = 'border-left' ;
$common_qa_stylePar_name6 = 'background-color' ;
$common_qa_title_color = $common_qa_style3->$common_qa_stylePar_name1 ;
$common_qa_title_border_top = $common_qa_style3->$common_qa_stylePar_name2 ;
$common_qa_title_border_right = $common_qa_style3->$common_qa_stylePar_name3 ; 
$common_qa_title_border_bottom = $common_qa_style3->$common_qa_stylePar_name4 ;
$common_qa_title_border_left = $common_qa_style3->$common_qa_stylePar_name5 ; 
$common_qa_title_background_color = $common_qa_style3->$common_qa_stylePar_name6 ;
//4.展開小圖示(C)
$common_qa_style4 = json_decode($record_design_style2["STYLE4"]) ; 
$common_qa_common_qa_title_i_fa_plus = $common_qa_style4->color ;
//5.收合-文字、線條、背景(C)
$common_qa_style5 = json_decode($record_design_style2["STYLE5"]) ; 
$common_qa_stylePar_name7 = 'color' ;
$common_qa_stylePar_name8 = 'border-top' ;
$common_qa_stylePar_name9 = 'border-right' ;
$common_qa_stylePar_name10 = 'border-bottom' ;
$common_qa_stylePar_name11 = 'border-left' ;
$common_qa_stylePar_name12 = 'background-color' ;
$common_qa_common_qa_title_bbt_color = $common_qa_style5->$common_qa_stylePar_name7 ;
$common_qa_common_qa_title_bbt_border_top = $common_qa_style5->$common_qa_stylePar_name8 ;
$common_qa_common_qa_title_bbt_border_right = $common_qa_style5->$common_qa_stylePar_name9 ; 
$common_qa_common_qa_title_bbt_border_bottom = $common_qa_style5->$common_qa_stylePar_name10 ;
$common_qa_common_qa_title_bbt_border_left = $common_qa_style5->$common_qa_stylePar_name11 ; 
$common_qa_common_qa_title_bbt_background_color = $common_qa_style5->$common_qa_stylePar_name12 ;
//6.收合小圖示(C)
$common_qa_style6 = json_decode($record_design_style2["STYLE6"]) ; 
$common_qa_common_qa_title_i_fa_minus = $common_qa_style6->color ;
//7.內容(CP)
$common_qa_style7 = json_decode($record_design_style2["STYLE7"]) ; 
$common_qa_stylePar_name13 = 'color' ;
$common_qa_stylePar_name14 = 'border-top' ;
$common_qa_stylePar_name15 = 'border-right' ;
$common_qa_stylePar_name16 = 'border-bottom' ;
$common_qa_stylePar_name17 = 'border-left' ;
$common_qa_stylePar_name18 = 'background-color' ;
$common_qa_common_qa_cont_color = $common_qa_style7->$common_qa_stylePar_name13 ;
$common_qa_common_qa_cont_border_top = $common_qa_style7->$common_qa_stylePar_name14 ;
$common_qa_common_qa_cont_border_right = $common_qa_style7->$common_qa_stylePar_name15 ; 
$common_qa_common_qa_cont_border_bottom = $common_qa_style7->$common_qa_stylePar_name16 ;
$common_qa_common_qa_cont_border_left = $common_qa_style7->$common_qa_stylePar_name17 ; 
$common_qa_common_qa_cont_background_color = $common_qa_style7->$common_qa_stylePar_name18 ;
?>


<style>
    /*抬頭*/
    .common-qa-title {background-image: url(<?=$common_qa_common_qa_itle_background_image?>);}/*1.Q小圖 30X30(P)*/
    .common-qa-cont{background-image: url(<?=$common_qa_common_qa_cont_background_image?>);}/*2.A小圖 30X30(P)*/
    .common-qa-title {color: <?=$common_qa_common_qa_title_bbt_color?>;border-top: <?=$common_qa_common_qa_title_bbt_border_top?>;border-right: <?=$common_qa_common_qa_title_bbt_border_right?>;border-bottom: <?=$common_qa_common_qa_title_bbt_border_bottom?>;border-left: <?=$common_qa_common_qa_title_bbt_border_left?>;background-color: <?=$common_qa_common_qa_title_bbt_background_color?>; }/*3.展開-文字、線條、背景(C)*/
    .common-qa-title i.fa-minus{color: <?=$common_qa_common_qa_title_i_fa_minus?>;}/*4.展開小圖示(C)*/
    .common-qa-title-bbt {color: <?=$common_qa_title_color?>;border-top: <?=$common_qa_title_border_top?>;border-right: <?=$common_qa_title_border_right?>;border-bottom: <?=$common_qa_title_border_bottom?>;border-left: <?=$common_qa_title_border_left?>;background-color: <?=$common_qa_title_background_color?>;}/*5.收合-文字、線條、背景(C)*/
    .common-qa-title i.fa-plus{color: <?=$common_qa_common_qa_title_i_fa_plus?>;}/*6.收合小圖示(C)*/
    /*7.內容(CP)*/
    .common-qa-cont {
    color: <?=$common_qa_common_qa_cont_color?>;
    border-top: <?=$common_qa_common_qa_cont_border_top?>;
    border-right: <?=$common_qa_common_qa_cont_border_right?>;
    border-bottom: <?=$common_qa_common_qa_cont_border_bottom?>;
    border-left: <?=$common_qa_common_qa_cont_border_left?>;
    background-color: <?=$common_qa_common_qa_cont_background_color?>;}
</style>
<body style="">

<!-- 產品獨立的css -->
<link rel="stylesheet" href="css/mainKingLeft.css"><!-- 左列菜單 -->
<link rel="stylesheet" href="css/bootstrap.min.css"><!-- 左列菜單 -->


<div id="loading"><img src="<?=$loading_image?>" alt="" ></div>
<!-- InstanceBeginEditable name="alert" -->

<!-- InstanceEndEditable -->

<div id="gotop"></div>
<!-- Navbar -->
<header class="">
    <? include("top_menu.php"); ?>

</header>

<? include("right_button.php"); ?>


<!--內容-->
<div id="wrapper" style="">
    <!-- InstanceBeginEditable name="titleImg" -->
    <div class="titleImg">
        <? include("pageTitleImg.php"); ?>
    </div>
    <!-- InstanceEndEditable -->
    <nav class="cd-navtb">
        <ul class="page-pad">
            <!-- InstanceBeginEditable name="breadcrumb" -->
            <!--<li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <a href="profile.php"><?=$all_page_name_array["member_center"]?></a> / 常見Q & A</li>-->
            <li class="breadcrumb"><a href="index.php" class="fa fa-home"></a> / <?=$all_page_name_array["common_qa"]?></li>
            <!-- InstanceEndEditable -->
        </ul>
    </nav>

    <main class="row page">

        

        <main class="col-md-12 col-sm-12 col-xs-12">
            <!-- InstanceBeginEditable name="mainKingRight" -->
            <main class="main-cont">
                <div class="row cont common-qa-page">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <ul>
                            <?
                            $query = "select * , DATE_FORMAT(EDIT_TIME,'%Y-%m-%d') AS deit_FORMAT_time
                                from common_qa where HIDE_ID = 0
                                order by LEVEL ASC" ;
                            $result = mysql_query( $query ) or die( mysql_error() ) ;
                            $num_total = mysql_num_rows( $result ) ;
                            while ( $record = mysql_fetch_array( $result ) )
                            {
                                $list_no++;
                                $id = $record["ID"];
                                //$title = mb_substr( strip_tags( $record["TITLE"] ),0,30,'utf8') ;
                                $title = $record["TITLE"];
                                $content = $record["CONTENT"];
                                $edit_time = $record["deit_FORMAT_time"];
                            ?>

                                <li>
                                    <div class="common-qa-title"><span><?=$title?></span><i class="fa fa-plus"></i>
                                    </div>
                                    <div class="common-qa-cont">
                                    <span class="qa-cont">
                                        <?=$content?>
                                    </span>
                                    </div>
                                </li>
                            <?
                            }
                            ?>


                        </ul>

                    </div>

                </div>
            </main>

            <!-- InstanceEndEditable -->
</div>
<div class="clear"></div>
</main>


</div><!--wrap結束-->
<!--內容結束-->

<? include("footer.php"); ?>

<!--totop-->
<div class="top">
    <a href="#" id="goTop"><span></span></a>
</div>





</body>

<!-- InstanceEnd --></html>

<? include("common_js.php"); ?>



<!-- InstanceBeginEditable name="layout2 script" -->
<script src="js/common-qa.js" type="text/javascript"></script>
<!-- InstanceEndEditable -->